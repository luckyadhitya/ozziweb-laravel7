<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## How to Run
Ketikan perintah dibawah ini pada terminal didalam folder project

1. Copy file .env.example
2. rename menjadi .env
3. sesuaikan konfigurasi database mysql
4. buka terminal lalu ketikan sesuai urutan
    - composer install
    - php artisan storage:link
    - php artisan key:generate
    - npm install && npm run dev
    - php artisan migrate --seed
    - php arisan serve
