const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public')
     .styles([
        'public/storage/laraassets/css/aos.css',
        'public/storage/laraassets/css/slick.css',
        'public/storage/laraassets/css/hover-min.css',
        'public/storage/laraassets/css/main.css',
     ], 'public/storage/laraassets/css/all.css')
     .js([
        'public/storage/laraassets/js/scrolloverflow.min.js',
        'public/storage/laraassets/js/owl.carousel.min.js',
        'public/storage/laraassets/js/apps.js',
        'public/storage/laraassets/js/script.js'
     ], 'public/storage/laraassets/js/bundle.js')