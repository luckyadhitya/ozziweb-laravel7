@extends('master')

@section('section')

<div class="z-layout">
    <div class="ozg-blog">
        <article class="ozg-blog-article">
            <figure class="ozg-blog-cover">
                <img src="{{asset('storage/laraassets/images/home/background/ozzigeno-intro-1.png')}}" alt="">
            </figure>
            <div class="ozg-blog-section my-5">
                <div class="ozg-blog-back">
                    <a href="{{url('/contact')}}" class="ozg-blog-back-btn">
                        <i class="fa fa-arrow-left fa-2x"></i>
                    </a>
                </div>
                <div class="ozg-blog-details">
                    <h1 class="ozg-blog-details-title">Looking for a job</h1>
                    <!-- <span class="ozg-blog-date-posted">9/05/2019</span> -->
                    <div class="ozg-blog-details-wrapper" style="min-height: 60vh;">
                        <ul>
                            @forelse ( $jobs as $job )
                                <li>
                                    <a href="{{ url('/hire', [$job['slug']]) }}">
                                        <strong>{{ $job->position }} </strong>
                                        <i class="fa fa-arrow-right"></i>
                                    </a>
                                    {{-- <div>{!! Str::words($job->responsibilities, 15, '...') !!}</div> --}}
                                </li>
                            @empty
                                <li>
                                    <a href="">
                                        <strong>Senior Account Executive</strong>
                                    </a>
                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum, ducimus ea quisquam aperiam, laboriosam officia consectetur odit modi quia dicta perspiciatis. Hic eius aut ex numquam exercitationem ipsam autem quibusdam.</p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div class="ozg-blog-sharer">
                    <ul class="ozg-blog-sharer-position">
                        <li class="ozg-blog-share">
                            <a href="javascript:;" class="facebook" id="shareToFacebook" onclick=""></a>
                        </li>
                        <li class="ozg-blog-share">
                            <a href="javascript:;" class="twitter" id="shareToTwitter" onclick=""></a>
                        </li>
                        <li class="ozg-blog-share">
                            <a href="javascript:;" class="whatsapp" id="shareToWhatsApp" onclick=""></a>
                        </li>
                    </ul>
                </div>
            </div>
            <footer class="ozg-blog-footer">
            </footer>
        </article>
    </div>
</div>

@endsection