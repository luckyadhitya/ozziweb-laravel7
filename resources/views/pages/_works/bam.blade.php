@extends('pages.work-detail')

@section('workdetail')

    <div class="work-details" style="background-color: #FFFFFF;">
        <section class="banner" style="background-image: url({{url('/')}}/laraassets/images/work-details/bam/background/bam-banner.png); background-color: #383636;">
            <article class="work-details-content banner-content">
                <div class="work-details-desc banner-desc">
                    <div class="banner-logo">
                        <img src="{{url('/')}}/laraassets/images/work-details/bam/logo/bam-logo.png" alt="BAM Logo" srcset="">
                    </div>
                    <div class="work-details-desc-wrap banner-desc-wrap">
                        <h1 style="color: #FFFFFF;">Project Brief</h1>
                        <p style="color: #FFFFFF;">Create brand awareness of BAM. A Restaurant with Spanish food in Indonesia.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="site" style="background-color: #383636;">
            <article class="work-details-content site-content" style="background-image: url({{url('/')}}/laraassets/images/work-details/bam/background/bam-gallery.png);">
                <div class="site-info">
                    <h1 style="color: #FFFFFF;">
                        <span>Food</span>
                        <span>Photography</span>
                    </h1>
                    <p style="color: #FFFFFF;">Maintaince for Social media of BAM and 48 Dimsum.<br>Food photography in every post combine with digital imaging.</p>
                </div>
                <div class="site-preview">
                    <img src="{{url('/')}}/laraassets/images/work-details/bam/thumb/bam-instagram.png" alt="BAM Instagram Official" srcset="">
                </div>
            </article>
        </section>
        <section class="app">
            <article class="work-details-content app-features image-right">
                <div class="work-details-thumb center">
                    <img src="{{url('/')}}/laraassets/images/work-details/bam/thumb/bam-project-brief.png" alt="BAM Project Brief" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #000000;">
                            <span>Project Brief</span>
                        </h1>
                        <p style="color: #000000;">Create brand awareness of 48 Dimsum a Halal Dimsum in Indonesia.</p>
                    </div>
                </div>
            </article>
        </section>
    </div>

@include('layouts.work-nav', ['bgColorWork'=>$bgColorWork, 'page'=>$workpage])

@endsection