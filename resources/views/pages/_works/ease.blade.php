@extends('pages.work-detail')

@section('workdetail')

    <div class="work-details" style="background-color: #F8CACA;">
        <section class="banner" style="background-image: url({{url('/')}}/laraassets/images/work-details/ease/background/ease-banner.png);">
            <article class="work-details-content banner-content">
                <div class="work-details-desc banner-desc">
                    <div class="banner-logo">
                        <img src="{{url('/')}}/laraassets/images/work-details/ease/logo/ease-logo.png" alt="Ease Brand Logo" srcset="">
                    </div>
                    <div class="work-details-desc-wrap banner-desc-wrap">
                        <h1 style="color: #243638;">Project Brief</h1>
                        <p style="color: #243638;">An Ecommerce web that provides a pleasant shopping experience which is designed in a modern manner with a touch of soft colours that spoil the eyes of the consumers.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="site">
            <article class="work-details-content site-content">
                <div class="site-info">
                    <h1 style="color: #243638;">Website</h1>
                    <p style="color: #333333;">An Ecommerce web that provides a pleasant shopping experience which is designed in a modern manner with a touch of soft colours that spoil the eyes of the consumers.</p>
                    <a class="site-url" href="https://easebrand.com" target="_blank" style="background-color: #243638;">Go to website</a>
                </div>
                <div class="site-preview">
                    <img src="{{url('/')}}/laraassets/images/work-details/ease/thumb/ease-site.png" alt="Ease Brand Official Site" srcset="">
                </div>
            </article>
        </section>
        <section class="app">
            <article class="work-details-content app-features image-right">
                <div class="work-details-thumb center">
                    <img src="{{url('/')}}/laraassets/images/work-details/ease/thumb/ease-bag.png" alt="Ease Brand add to Bag" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #243638;">
                            <span>To Bag</span>
                        </h1>
                        <p style="color: #333333;">Using a simple layout filled with all the information needed, that makes it easy for consumers to purchase.</p>
                    </div>
                </div>
            </article>
            <article class="work-details-content app-features image-right" style="background-image: url({{url('/')}}/laraassets/images/work-details/ease/background/ease-cart.jpg);">
                <div class="work-details-thumb end">
                    <img src="{{url('/')}}/laraassets/images/work-details/ease/thumb/ease-cart.png" alt="Ease Brand add to Cart" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h2 style="color: #243638;">
                            <span>To Cart</span>
                        </h2>
                        <p style="color: #333333;">The items that you save or want to buy are stored in a shopping basket that is easy to process.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="result">
            <article class="work-details-content result-content image-right">
                <div class="work-details-thumb result-thumb end">
                    <img src="{{url('/')}}/laraassets/images/work-details/ease/thumb/ease-result.png" alt="Ease Brand Result" srcset="">
                </div>
                <div class="work-details-desc result-desc">
                    <div class="work-details-desc-wrap result-desc-wrap">
                        <h1 style="color: #243638;">Result</h1>
                        <p style="color: #333333;">Website adaptation to mobile devices is an essential and inseparable part of a modern website.</p>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="color: #333333;">2.514</h1>
                                        <p style="color: #5AC4BF;">User</p>
                                        <hr>
                                    </td>
                                    <td>
                                        <h1 style="color: #333333;">5.12</h1>
                                        <p style="color: #5AC4BF;">Page/Session</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="color: #333333;">21.641</h1>
                                        <p style="color: #5AC4BF;">Page view</p>
                                    </td>
                                    <td>
                                        <h1 style="color: #333333;">38,75%</h1>
                                        <p style="color: #5AC4BF;">Bounce Rate</p>
                                        <hr>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
        </section>
    </div>

    @include('layouts.work-nav', ['bgColorWork'=>$bgColorWork, 'page'=>$workpage])
@endsection