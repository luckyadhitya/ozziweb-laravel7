@extends('pages.work-detail')

@section('workdetail')

    <div class="work-details" style="background-color: #F5F5F7;">
        <section class="banner" style="background-image: url({{url('/')}}/laraassets/images/work-details/safi/background/safi-banner.png);">
            <article class="work-details-content banner-content">
                <div class="work-details-desc banner-desc">
                    <div class="banner-logo">
                        <img src="{{url('/')}}/laraassets/images/work-details/safi/logo/safi-logo.png" alt="Safi Indonesia Logo" srcset="">
                    </div>
                    <div class="work-details-desc-wrap banner-desc-wrap">
                        <h1 style="color: #001A66;">Project Brief</h1>
                        <p style="color: #333333;">Create BRAND AWARENESS of Safi as new skincare brand made from finest natural ingredients, halal, and tested (proven efficacy) with EMOTIONAL ATTACHMENT that Safi understand their needs and enables them become a better Muslimah every day in clear communication to DIFFERENTIATE Safi from others.</p>
                        <h1 style="color: #001A66;">The Challenge</h1>
                        <p style="color: #333333;">There are already some of Big Halal Skincare brands in Indonesia, then how to differentiate Safi with other brands? How to build awareness and trust thru digital?</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="additional">
            <article class="work-details-content additional-content image-left" style="background-image: url({{url('/')}}/laraassets/images/work-details/safi/background/hexagon-1.png);">
                <div class="work-details-thumb start aos-animate aos-init">
                    <img src="{{url('/')}}/laraassets/images/work-details/safi/thumb/safi-research-institute.png" alt="Safi Research Institute" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <span class="additional-logo">
                            <img src="{{url('/')}}/laraassets/images/work-details/safi/logo/safi-research-institute.png" alt="Safi Research Institute" srcset="">
                        </span>
                        <p style="color: #333333;">Safi is developed in Safi Research Institute, the World’s First Halal Research Facilities. Scientists in Safi Research Institute interact with Indonesian Muslim women to understand their needs of skincare products that are Halal, Natural, and Tested.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="site" style="background-color: #000040;">
            <article class="work-details-content site-content" style="background-image: url({{url('/')}}/laraassets/images/work-details/safi/background/hexagon-2.png);">
                <div class="site-info">
                    <h1 style="color: #FFFFFF;">Website</h1>
                    <p style="color: #FFFFFF;">One Stop Information Center for All Safi Enthusiasts. Designed to give costumers all the Information they need about Safi. User Friendly.</p>
                    <a class="site-url" href="https://safiindonesia.com" target="_blank" style="background-color: #1D1D4F;">Go to website</a>
                </div>
                <div class="site-preview">
                    <img src="{{url('/')}}/laraassets/images/work-details/safi/thumb/safi-site.png" alt="Safi Indonesia Official Site" srcset="">
                </div>
            </article>
        </section>
        <section class="app">
            <article class="work-details-content app-features image-right">
                <div class="work-details-thumb center">
                    <img src="{{url('/')}}/laraassets/images/work-details/safi/thumb/safi-social-media.png" alt="Safi Social Media" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #000040;">
                            <span>Social</span>
                            <span>Media</span>
                        </h1>
                        <p style="color: #000040;">The face of the brand: Bridging Safi and Costumers. Informative Content to build trust with customers while Highlighting the USP to differentiate Safi with the existing Brands.</p>
                    </div>
                </div>
            </article>
            <article class="work-details-content app-features image-right">
                <div class="work-details-thumb end">
                    <img src="{{url('/')}}/laraassets/images/work-details/safi/thumb/safi-application.gif" alt="Safi Skin Analyze Application" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h2 style="color: #000040;">
                            <span>Skin</span>
                            <span>Analyze</span>
                            <span>Application</span>
                        </h2>
                        <p style="color: #000040;">Highly Technological Apps to help customers to understand their skin in just 1 Selfie. Giving information related to skin problems and how to overcome it.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="kol" style="background-color: #000040;">
            <article class="work-details-content kol-content image-right" style="background-image: url({{url('/')}}/laraassets/images/work-details/safi/background/hexagon-3.png);">
                <div class="work-details-thumb end">
                    <img src="{{url('/')}}/laraassets/images/work-details/safi/thumb/safi-kol.png" alt="Safi KOL" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #FFFFFF;">KOL</h1>
                        <p style="color: #FFFFFF;">The power of Words of Mouth. Focusing on the Efficacy of the products, collaborating with KOL in raising awareness about Safi Products.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="result">
            <article class="work-details-content result-content image-left" style="background-image: url({{url('/')}}/laraassets/images/work-details/safi/background/safi-result.jpg);">
                <div class="work-details-thumb result-thumb center">
                    <img src="{{url('/')}}/laraassets/images/work-details/safi/thumb/safi-social-media.png" alt="Safi Result" srcset="">
                </div>
                <div class="work-details-desc result-desc">
                    <div class="work-details-desc-wrap result-desc-wrap">
                        <h1 style="color: #FFFFFF">Result</h1>
                        <p style="color: #FFFFFF;">Successfully enter Indonesian Market in a year. Being one of the noisiest brands that are launched in 2018.</p>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="color: #26FFFF;">75K</h1>
                                        <p style="color: #AAAAAA;">/ 7 Month<br>Followers Instagram</p>
                                        <hr>
                                    </td>
                                    <td>
                                        <h1 style="color: #26FFFF;">40K</h1>
                                        <p style="color: #AAAAAA;">Organic Traffic Monthly</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="color: #26FFFF;">1.16M</h1>
                                        <p style="color: #AAAAAA;">/ 10 Month<br>Organic Impression</p>
                                    </td>
                                    <td>
                                        <h1 style="color: #26FFFF;">41%</h1>
                                        <p style="color: #AAAAAA;">Awareness<br>The penetration of the awareness of Safi in Indonesia</p>
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="color: #26FFFF;">0.4%</h1>  
                                        <p style="color: #AAAAAA;">Difference with main competitor in terms of Sales in GT<br><br>Market Share</p>          
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
        </section>
    </div>

    @include('layouts.work-nav', ['bgColorWork'=>$bgColorWork, 'page'=>$workpage])
    
@endsection