@extends('pages.work-detail')

@section('workdetail')

    <div class="work-details" style="background-color: #F5F5F7;">
        <section class="banner" style="background-image: url({{url('/')}}/laraassets/images/work-details/bioessence/background/bioessence-banner.png);">
            <article class="work-details-content banner-content">
                <div class="work-details-desc banner-desc">
                    <div class="banner-logo">
                        <img src="{{url('/')}}/laraassets/images/work-details/bioessence/logo/bioessence-logo.png" alt="Bioessence Logo" srcset="">
                    </div>
                    <div class="work-details-desc-wrap banner-desc-wrap">
                        <h1 style="color: #3693CF;">Project Brief</h1>
                        <p style="color: #243538;">Create brand awareness of Bio-essence in Indonesia and drive it to sales.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="site">
            <article class="work-details-content site-content">
                <div class="site-info">
                    <h1 style="color: #2188CA;">Website</h1>
                    <p style="color: #444444;">Bio-essence designed as a responsive website which the customers can browse it anywhere from the desktop to the mobile phone. Not only limited to that, Bio-essence designed as a user friendly website to give a full information for customers.</p>
                    <a class="site-url" href="https://bioessence.id" target="_blank" style="background-color: #2188CA;">Go to website</a>
                </div>
                <div class="site-preview">
                    <img src="{{url('/')}}/laraassets/images/work-details/bioessence/thumb/bioessence-site.png" alt="Bioessence Official Site" srcset="">
                </div>
            </article>
        </section>
        <section class="app">
            <article class="work-details-content app-features image-right">
                <div class="work-details-thumb center">
                    <img src="{{url('/')}}/laraassets/images/work-details/bioessence/thumb/product.png" alt="Bioessence Product" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h3 style="color: #1F86CA;">
                            <span>Showing Off</span>
                            <span>Product Detail</span>
                        </h3>
                        <p style="color: #444444;">As a skincare who understand the customers needs, here Bio-essence give a product detail and benefit to keep the customers understanding about the product.</p>
                    </div>
                </div>
            </article>
            <article class="work-details-content app-features image-right" style="background-color: #CDE1EF;">
                <div class="work-details-thumb end">
                    <img src="{{url('/')}}/laraassets/images/work-details/bioessence/thumb/store-locator.png" alt="Bioessence Store Locator" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #1F86CA;">
                            <span>Store</span>
                            <span>Locator</span>
                        </h1>
                        <p style="color: #444444;">There's still a lot of people out there who have no idea where to buy Bio-essence. This is where the customers can find an information of where is the nearest store to buy Bio-essence.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="kol">
            <article class="work-details-content kol-content image-right" style="background-image: url({{url('/')}}/laraassets/images/work-details/bioessence/background/bioessence-kol.png);">
                <div class="work-details-thumb center">
                    <img src="{{url('/')}}/laraassets/images/work-details/bioessence/thumb/bioessence-kol.png" alt="Bioessence KOL" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #2B8BCA;">KOL</h1>
                        <p style="color: #333333;">Use BIG KOL influencer of skin care, the product was sold out in everywhere.</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="result">
            <article class="work-details-content result-content image-right">
                <div class="work-details-thumb result-thumb end">
                    <img src="{{url('/')}}/laraassets/images/work-details/bioessence/thumb/campaign-result.png" alt="Bioessence Campaign Result" srcset="">
                </div>
                <div class="work-details-desc result-desc">
                    <div class="work-details-desc-wrap result-desc-wrap">
                        <h1 style="color: #2B8BCA">
                            <span>Campaign</span>
                            <span>&amp; Result</span>
                        </h1>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="color: #444444;">360</h1>
                                        <p style="color: #444444;">Intergrated digital for Bio-essence Royal Jelly +ATP campaign</p>
                                        <hr>
                                    </td>
                                    <td>
                                        <h1 style="color: #444444;">601</h1>
                                        <p style="color: #444444;">Actual Offline Registrants from 100 targeted Registrants in Microsite</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="color: #444444;">449</h1>
                                        <p style="color: #444444;">Online Submissions Videos</p>
                                    </td>
                                    <td>
                                        <h1 style="color: #444444;">50+</h1>
                                        <p style="color: #444444;">KOL helps to review trial kit products of Bio-essence Royal Jelly+ATP</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
        </section>
        <section class="infographic" style="background-color: #FFFFFF;">
            <article class="work-details-content infographic-content" style="background-image: url({{url('/')}}/laraassets/images/work-details/bioessence/background/result-infographic.png);">
                <div class="work-details-thumb infographic-thumb">
                    <img src="{{url('/')}}/laraassets/images/work-details/bioessence/thumb/infographic.png" alt="Bioessence Campaign Infographic" srcset="">
                </div>
            </article>
        </section>
    </div>

    @include('layouts.work-nav', ['bgColorWork'=>$bgColorWork, 'page'=>$workpage])
    
@endsection