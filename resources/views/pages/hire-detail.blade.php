@extends('master')

@section('section')

<div class="z-layout">
    <div class="ozg-blog">
        <article class="ozg-blog-article">
            <figure class="ozg-blog-cover">
                <img src="images/home/background/ozzigeno-intro-1.png" alt="">
            </figure>
            <div class="ozg-blog-section my-5">
                <div class="ozg-blog-back">
                    <a href="{{url('/hire')}}" class="ozg-blog-back-btn">
                        <i class="fa fa-arrow-left fa-2x"></i>
                    </a>
                </div>
                <div class="ozg-blog-details">
                    <h1 class="ozg-blog-details-title">{{ $job->position }}</h1>
                    <!-- <span class="ozg-blog-date-posted">9/05/2019</span> -->
                    <div class="ozg-blog-details-wrapper">
                        <strong>Responsibilities:</strong>
                        {!! $job->responsibilities !!}

                        <strong>Requirements:</strong>
                        {!! $job->requirement !!}
                    </div>
                </div>
                <div class="ozg-blog-sharer">
                    <ul class="ozg-blog-sharer-position">
                        <li class="ozg-blog-share">
                            <a href="javascript:;" class="facebook" id="shareToFacebook" onclick=""></a>
                        </li>
                        <li class="ozg-blog-share">
                            <a href="javascript:;" class="twitter" id="shareToTwitter" onclick=""></a>
                        </li>
                        <li class="ozg-blog-share">
                            <a href="javascript:;" class="whatsapp" id="shareToWhatsApp" onclick=""></a>
                        </li>
                    </ul>
                </div>
            </div>
            <footer class="ozg-blog-footer">
            </footer>
        </article>
    </div>
</div>

@endsection