@extends('master')

@section('section')

    <div class="z-layout">
        <div class="ozg-work-teaser">
            <h1 data-aos="fade-right" data-aos-delay="500">
                <span>Big or</span>
                <span>Small,</span>
                <span>doesnt</span>
                <span>matter</span>
            </h1>
            <p data-aos="fade-right" data-aos-delay="750">We believe in the immense power and potential of a well-designed web image. To exploit the potential of the digital environment, we develop memorable.</p>
        </div>
        <div class="ozg-work" id="workPage">
            <section class="section ozg-work-section">
                <div class="ozg-work-wrapper">
                    <div class="ozg-work-grid">
                        <div class="z-page-hanger">
                            <a href="" class="hvr-underline-from-center">
                                <span>We are ozzi</span>
                            </a>
                        </div>
                        <div class="ozg-work-grid-wrapper">
                            <div class="ozg-work-quote">
                                <p data-quote="&ldquo;" data-aos="fade-down" data-aos-delay="250">Maybe we are not the best, but we will make you become the best</p>
                            </div>
                            <div class="ozg-work-teaser forMobile">
                                <h1 data-aos="fade-right" data-aos-delay="500">
                                    <span>Big or</span>
                                    <span>Small,</span>
                                    <span>doesnt</span>
                                    <span>matter</span>
                                </h1>
                                <p data-aos="fade-right" data-aos-delay="750">We believe in the immense power and potential of a well-designed web image. To exploit the potential of the digital environment, we develop memorable.</p>
                            </div>
                            <div class="ozg-work-content" data-aos="fade-down" data-aos-delay="1000">
                                <div class="ozg-work-content-grid">
                                    <div class="ozg-work-list">
                                        <div class="grid">
                                            @forelse ($brands as $brand)
                                                <a href="{{ $brand['url'] }}" class="item" data-aos="fade-down">
                                                    <div class="item-banner">
                                                        <div class="item-image">
                                                            <img src="{{ asset('storage')}}/laraassets/images/brands/{{ $brand['cover'] }}" title="{{ $brand['name'] }}" alt="{{ $brand['name'] }}">
                                                        </div>
                                                        <div class="item-overlay"></div>
                                                        <div class="item-name">
                                                            <h2>{{ $brand['name'] }}</h2>
                                                            <h3>{{ $brand['project-service'] }}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="item-thumb">
                                                        <div class="item-thumb-wrapper">
                                                            <img src="{{ asset('storage')}}/laraassets/images/brands/{{ $brand['thumb'] }}" title="{{ $brand['name'] }}" alt="{{ $brand['name'] }}">
                                                        </div>
                                                    </div>
                                                </a>
                                            @empty
                                                <div class="item-banner">
                                                    <div class="item-name">
                                                        <h2>No brands founded</h2>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
                        
@endsection