@extends('master')

@section('section')

    <div class="z-layout">
        <div class="ozg-work" id="workPage">
            <section class="section ozg-work-section">
                <div class="ozg-work-back">
                    <a href="{{url('/works')}}" class="ozg-work-back-btn">
                        <i class="fa fa-arrow-left fa-2x" style="color: {{$brand->nav_color}};"></i>
                    </a>
                </div>
                <div class="ozg-work-wrapper">
                    @yield('workdetail')
                    <div class="work-details" style="background-color: {{$brand->color}};">
                        @foreach ($sections as $section)
                            {!! $section->content !!}
                        @endforeach
                    </div>
                    @if (!empty($next))
                        <div class="next-work">
                            <div class="next-work-content">
                                <div class="next-work-cover">
                                    <div class="next-work-cover-image" style="background-image: url({{asset('storage')}}/laraassets/images/brands/{{ $next->background }});"></div>
                                    <div class="next-work-cover-overlay"></div>
                                </div>
                                <div class="next-work-teaser">
                                    <div class="next-work-teaser-wrapper">
                                        <p class="intro">Next Work</p>
                                        <a href="{{url($next->url)}}" class="title">
                                            <span data-letters="{{ $next->name }}">{{ $next->name }}</span>
                                        </a>
                                    </div>
                                    <a href="{{url('/works')}}" class="our-works">Our Works</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="ozg-work-backtoTop">
        <a href="#" class="ozg-work-backtoTop-btn show" id="back-to-top"></a>
    </div>
@endsection