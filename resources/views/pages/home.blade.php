@extends('master')

@section('section')
    <div class="z-layout">
        <div class="ozg-home" id="fullpage">
            <div class="section ozg-home-wrapper">
                <div class="ozg-home-slides rslides">
                    <li class="ozg-home-slides-item">
                        <div class="ozg-home-slides-wrap" style="background-image: url({{asset('storage/laraassets/images/home/background/ozzigeno-intro-1.png')}});">
                            <div class="ozg-home-intro">
                                <h1 class="ozg-home-motto" data-aos="fade-right">
                                    <span>Breathe</span>
                                    <span>Creativity</span>
                                </h1>
                                <h2 class="ozg-home-teaser" data-aos="fade-left">Digital Creative Agency</h2>
                            </div>
                        </div>
                    </li>
                    <li class="ozg-home-slides-item">
                        <div class="ozg-home-slides-wrap" style="background-image: url({{asset('storage/laraassets/images/home/background/ozzigeno-intro-2.png')}});">
                            <div class="ozg-home-intro">
                                <h1 class="ozg-home-motto" data-aos="fade-right">
                                    <span>Brand-care</span>
                                    <span>Routine</span>
                                </h1>
                                <h2 class="ozg-home-teaser" data-aos="fade-left">For Your Digital Presence</h2>
                            </div>
                        </div>
                    </li>
                </div>
            </div>
            <div class="section ozg-home-content">
                <div class="ozg-home-content-wrap">
                    <div class="slideshow" data-aos="fade-left">
                        <div class="slideshow-grid">
                            @if (!$brands->isEmpty())
                                <div class="slideshow-item" id="homeSlideshow">
                                    <div class="pieces" style="background-image: url({{asset('storage')}}/laraassets/images/brands/{{ $brands[0]->item }})"></div>
                                </div>
                                <div class="slideshow-anchor">
                                    <a href="{{url('/works/safi-indonesia')}}" class="link">
                                        <span class="hvr-underline-from-center">Detail</span>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <nav class="menu" id="homeMenu" data-aos="fade-right">
                        @forelse ($brands as $i => $brand)
                            <a class="menu-item {{ $i == 0 ? 'menu-item--current' : '' }}" data-href="{{url('/works/safi-indonesia')}}" data-image="{{asset('storage')}}/laraassets/images/brands/{{ $brand->item }}">
                                {{ $brand->name }} <span>{{ $brand->service }}</span>
                            </a>
                        @empty
                            <a class="menu-item">
                                No brands founded
                            </a>
                        @endforelse
                    </nav>
                    {{-- <nav class="menu" id="homeMenu" data-aos="fade-right">
                        <a class="menu-item menu-item--current" data-href="{{url('/works/safi-indonesia')}}" data-image="{{asset('storage')}}/laraassets/images/home/background/item/safi.png">Safi <span>Annual Campaign</span>
                        </a>
                        <a class="menu-item" data-href="{{url('/works/ease')}}" data-image="{{asset('storage')}}/laraassets/images/home/background/item/ease.png">Ease Brand <span>E-commerce</span></a>
                        </a>
                        <a class="menu-item" data-href="{{url('/works/bio-essence')}}" data-image="{{asset('storage')}}/laraassets/images/home/background/item/bio-essence.png">BioEssence <span>Digital Strategy</span>
                        </a>
                        <a class="menu-item" data-href="{{url('/works/milkuat')}}" data-image="{{asset('storage')}}/laraassets/images/home/background/item/milkuat.png">Milkuat <span>Digital Campaign</span>
                        </a>
                        <a class="menu-item" data-href="{{url('/works/bam')}}" data-image="{{asset('storage')}}/laraassets/images/home/background/item/bam.png">BAM! <span>Annual Campaign</span></a>
                        <a class="menu-item" data-href="{{url('/works/vitalis')}}" data-image="{{asset('storage')}}/laraassets/images/home/background/item/vitalis.png">Vitalis <span>Annual Campaign</span></a>
                    </nav> --}}
                    <div class="watermark">
                        <h1 class="ozg-page-title" data-aos="fade-right" data-aos-delay="2000">Latest work</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection