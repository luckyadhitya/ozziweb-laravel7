@extends('pages.work-detail')

@section('workdetail')

<div class="work-details" style="background-color: #ff;">
        <section class="banner" style="background-image: url(/storage/laraassets/images/work-details/milkuat/background/milkuat-banner.jpg);">
            <article class="work-details-content banner-content">
                <div class="work-details-desc banner-desc">
                    <div class="banner-logo">
                        <img src="/storage/laraassets/images/work-details/milkuat/logo/milkuat-logo.png" alt="Milkuat Logo" srcset="">
                    </div>
                    <div class="work-details-desc-wrap banner-desc-wrap">
                        <h1 style="color: #444444;">Project Brief</h1>
                        <ul style="color: #AAAAAA;">
                            <li>Educate Moms to providing good 	nutrition & great taste to kids through right channel, right content & right time</li>
                            <li>Encourage Kids to engage with Milkuat brand through experiences (games, video, etc) that drives incremental brand choice before purchase.</li>
                        </ul>
                        <h1 style="color: #444444;">Challenge</h1>
                        <p style="color: #AAAAAA;">How to Milkuat become a product choice and known as a milk that have nutrition</p>
                    </div>
                </div>
            </article>
        </section>
        <section class="site" style="background-color: #ED1B25;">
            <article class="work-details-content site-content" style="background-image: url(/storage/laraassets/images/work-details/milkuat/background/milkuat-site.png);">
                <div class="site-info">
                    <h1 style="color: #FFFFFF;">Website</h1>
                    <p style="color: #FFFFFF;">All information about product of Milkuat and parenting tips article and user friendly.</p>
                    <a class="site-url" href="https://milkuat.co.id" target="_blank" style="background-color: #FFC311;">Go to website</a>
                </div>
                <div class="site-preview">
                    <img src="/storage/laraassets/images/work-details/milkuat/thumb/milkuat-site.png" alt="Milkuat Official Site" srcset="">
                </div>
            </article>
        </section>
        <section class="app" style="background-image: url(/storage/laraassets/images/work-details/milkuat/background/milkuat-app.png);">
            <article class="work-details-content app-features image-right">
                <div class="work-details-thumb end">
                    <img src="/storage/laraassets/images/work-details/milkuat/thumb/milkuat-app.png" alt="What We Do with Milkuat" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h1 style="color: #444444;">
                            <span>What</span>
                            <span>We Do</span>
                        </h1>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="/storage/laraassets/images/work-details/milkuat/thumb/milkuat-do-1.png" alt="Article in website">
                                        <p style="color: #AAAAAA;">Article in website for Moms</p>
                                    </td>
                                    <td>
                                        <img src="/storage/laraassets/images/work-details/milkuat/thumb/milkuat-do-2.png" alt="Article in website">
                                        <p style="color: #AAAAAA;">Games for kids</p>
                                    </td>
                                    <td>
                                        <img src="/storage/laraassets/images/work-details/milkuat/thumb/milkuat-do-3.png" alt="Article in website">
                                        <p style="color: #AAAAAA;">Mini campaign for kids</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
            <article class="work-details-content app-features image-right" style="background-image: url(/storage/laraassets/images/work-details/milkuat/background/milkuat-app-earth.png);">
                <div class="work-details-thumb end">
                    <img src="/storage/laraassets/images/work-details/milkuat/thumb/milkuat-campaign.png" alt="Milkuat Boboiboy Campaign" srcset="">
                </div>
                <div class="work-details-desc">
                    <div class="work-details-desc-wrap">
                        <h3 style="color: #444444;">
                            <span>In 2017 Collaborate</span>
                            <span>With Boboiboy</span>
                        </h3>
                        <ol style="color: #AAAAAA;">
                            <li>Learning from Milkuat BBB Promo in semester 2, 2017, the promo was drives  the incrasing sales of Milkuat Bottle by >45%</li>
                            <li>On the brand imagery, the Milkuat BBB communication drives imagery on “makes me strong and gives me energy”</li>
                        </ol>
                    </div>
                </div>
            </article>
        </section>
        <section class="result">
            <article class="work-details-content result-content-noImage" style="background-image: url(/storage/laraassets/images/work-details/milkuat/background/milkuat-result.jpg);">
                <div class="work-details-desc result-desc-noImage">
                    <div class="work-details-desc-wrap result-desc-noImage-wrap">
                        <h1 style="color: #444444;">Result</h1>
                        <div class="result-desc-noImage-list">
                            <div class="data">
                                <div class="data-counter">
                                    <div class="data-counter-wrapper">
                                        <span style="color: #ED1B25;">2.895</span>
                                    </div>
                                </div>
                                <div class="data-desc">
                                    <p>Total user participant in 3 months of games called Milkupolis</p>
                                </div>
                            </div>
                            <div class="data">
                                <div class="data-counter">
                                    <div class="data-counter-wrapper">
                                        <span style="color: #ED1B25;">1.026.980</span>
                                    </div>
                                </div>
                                <div class="data-desc">
                                    <p>Total fans in facebook since 2012</p>
                                </div>
                            </div>
                            <div class="data">
                                <div class="data-counter">
                                    <div class="data-counter-wrapper">
                                        <span style="color: #ED1B25;">3.817</span>
                                    </div>
                                </div>
                                <div class="data-desc">
                                    <p>Total fans in Instagram (without ads at Instagram) since 2017</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
    </div>
    <div class="next-work">
        <div class="next-work-content">
            <div class="next-work-cover">
                <div class="next-work-cover-image" style="background-image: url(/storage/laraassets/images/work/background/bam.jpg);"></div>
                <div class="next-work-cover-overlay"></div>
            </div>
            <div class="next-work-teaser">
                <div class="next-work-teaser-wrapper">
                    <p class="intro">Next Work</p>
                    <a href="{{url('/works/bam')}}" class="title">
                        <span data-letters="BAM!">BAM!</span>
                    </a>
                </div>
                <a href="{{url('/works')}}" class="our-works">Our Works</a>
            </div>
        </div>
    </div>

    {{-- @include('layouts.work-nav', ['bgColorWork'=>$bgColorWork, 'page'=>$workpage]) --}}

@endsection