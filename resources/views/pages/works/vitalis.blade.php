@extends('pages.work-detail')

@section('workdetail')

<div class="work-details" style="background-color: #FFFFFF;">
    <section class="banner" style="background-image: url(/storage/laraassets/images/work-details/vitalis/background/vitalis-banner.png);">
        <article class="work-details-content banner-content">
            <div class="work-details-desc banner-desc">
                <div class="banner-logo">
                    <img src="/storage/laraassets/images/work-details/vitalis/logo/vitalis-logo.png" alt="Vitalis Logo" srcset="">
                </div>
                <div class="work-details-desc-wrap banner-desc-wrap">
                    <h1 style="color: #333333;">Project Brief</h1>
                    <p style="color: #444444;">Create BRAND AWARENESS and STRENGTHENING message Vitalis as market leader in female fragrance. Vitalis want to attract audience to reach their dream and Vitalis will help them to feel more confident.</p>
                    <h1 style="color: #333333;">The Challenge</h1>
                    <p style="color: #444444;">There are already some of fragrance brands in Indonesia, then how to differentiate Vitalis with other brands? How to build awareness and trust thru digital?</p>
                </div>
            </div>
        </article>
    </section>
    <section class="site" style="background-color: #EEEEEE;">
        <article class="work-details-content site-content">
            <div class="site-info">
                <h1 style="color: #333333;">Website</h1>
                <p style="color: #444444;">To give information about brand and product knowledge.</p>
                <a class="site-url" href="https://pesonavitalis.com" target="_blank" style="background-color: #666666;">Go to Website</a>
            </div>
            <div class="site-preview">
                <img src="/storage/laraassets/images/work-details/vitalis/thumb/vitalis-site.png" alt="Vitalis Official Site" srcset="">
            </div>
        </article>
    </section>
    <section class="app" style="background-color: #EEEEEE;">
        <article class="work-details-content app-features image-right">
            <div class="work-details-thumb center">
                <img src="/storage/laraassets/images/work-details/vitalis/thumb/vitalis-social-media.png" alt="Vitalis Social Media" srcset="">
            </div>
            <div class="work-details-desc">
                <div class="work-details-desc-wrap">
                    <h1 style="color: #333333;">
                        <span>Social</span>
                        <span>Media</span>
                    </h1>
                    <p style="color: #444444;">As a place for customers to find information updated from Vitalis and also as a place to invite customers to interact with brands.</p>
                </div>
            </div>
        </article>
    </section>
    <section class="result" style="background-color: #EEEEEE;">
        <div class="result-header">
            <h1 style="color: #333333;">Vitalis Empression</h1>
            <p style="color: #444444;">Awareness and Virality about campaign #VitalisEmpression</p>
        </div>
        <article class="work-details-content result-content image-left">
            <div class="work-details-thumb result-thumb center">
                <img src="/storage/laraassets/images/work-details/vitalis/thumb/vitalis-empression.png" alt="Vitalis Empression Campaign Result">
            </div>
            <div class="work-details-desc result-desc">
                <div class="work-details-desc-wrap result-desc-wrap">
                    <h1 style="color: #333333;">Result</h1>
                    <p style="color: #444444;">Successfully enter Indonesian Market in a year. Being one of the noisiest brands that are launched in 2018.</p>
                    <table width="100%" cellpadding="" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <h1 style="color: #333333;">87.934.805</h1>
                                    <p style="color: #444444;">Impression</p>
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h1 style="color: #333333;">877.449</h1>
                                    <p style="color: #444444;">Engagement</p>
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h1 style="color: #333333;">1%</h1>
                                    <p style="color: #444444;">Engagement Rate</p>
                                </td>
                                <td>
                                    <h1 style="color: #333333;">610</h1>
                                    <p style="color: #444444;">Submissions</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </article>
        <article class="work-details-content result-content image-right">
            <div class="work-details-thumb result-thumb end">
                <img src="/storage/laraassets/images/work-details/vitalis/thumb/vitalis-my-exotic-vacation.png" alt="Vitalis My Exotic Vacation Campaign Result" srcset="">
            </div>
            <div class="work-details-desc result-desc">
                <div class="work-details-desc-wrap result-desc-wrap">
                    <h1 style="color: #333333;">My Exotic Vacation</h1>
                    <p style="color: #444444;">To increase New Vitalis EBS Awareness which start from traveler blogger and to link our EBS wit a vacation. At the end we want to get virality from bloggers.</p>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <h1 style="color: #545454;">343</h1>
                                    <p style="color: #868686;">Total Post</p>
                                    <hr>
                                </td>
                                <td>
                                    <h1 style="color: #545454;">2217</h1>
                                    <p style="color: #868686;">Total User</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h1 style="color: #545454;">7411</h1>
                                    <p style="color: #868686;">Total Share</p>
                                </td>
                                <td>
                                    <h1 style="color: #545454;">1765</h1>
                                    <p style="color: #868686;">Total Likes</p>
                                    <hr>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </article>
    </section>
</div>

<div class="next-work">
    <div class="next-work-content">
        <div class="next-work-cover">
            <div class="next-work-cover-image" style="background-image: url(/storage/laraassets/images/work/background/safi.jpg);"></div>
            <div class="next-work-cover-overlay"></div>
        </div>
        <div class="next-work-teaser">
            <div class="next-work-teaser-wrapper">
                <p class="intro">Next Work</p>
                <a href="{{url('/works/safi-indonesia')}}" class="title">
                    <span data-letters="Safi Indonesia">Safi Indonesia</span>
                </a>
            </div>
            <a href="{{url('/works')}}" class="our-works">Our Works</a>
        </div>
    </div>
</div>

@endsection