@extends('master')

@section('section')

    <div class="z-layout">
        <div class="ozg-contact">
            <div class="ozg-contact-hanger">
                <div class="z-page-hanger left">
                    <a href="{{url('/')}}/about" class="hvr-underline-from-center">
                        <span>We are ozzi</span>
                    </a>
                </div>
                <div class="z-page-hanger right">
                    <a href="{{url('/')}}/works" class="hvr-underline-from-center">
                        <span>Case Studies</span>
                    </a>
                </div>
            </div>
            <div class="ozg-contact-wrapper">
                <div class="ozg-contact-grid">
                    <address class="ozg-contact-address" data-aos="fade-right" data-aos-delay="250">
                        <h3>Indonesia</h3>
                        <p><span>Pluit Raya 200 no. 10A</span><span>North Jakarta</span>
                        </p>
                        <p><a href="https://goo.gl/maps/g23x65zK2i92" class="hvr-underline-from-center" target="_blank">View Map</a></p>
                        <br>
                        <br>
                        <p>
                            <a href="tel:+62 21 2266 4715" class="tel" target="_blank">
                                <img src="{{asset('storage/laraassets/images/general/icon/phone.png')}}" alt="Ozzigeno Phone" srcset="">
                                <span class="hvr-underline-from-center">+62 21 2266 4715</span>
                            </a>
                        </p>
                        <p>
                            <a href="https://wa.me/6285899990221" class="tel" target="_blank">
                                <img src="{{asset('storage/laraassets/images/general/icon/whatsapp.png')}}" alt="Ozzigeno WhatsApp Business" srcset="">
                                <span class="hvr-underline-from-center">+62 858 9999 0221</span>
                            </a>
                        </p>
                    </address>
                    <div class="ozg-contact-caption">
                        <p data-quote="&ldquo;" data-aos="fade-down" data-aos-delay="500">We would love to work with you, just drop a messages. Who knows something good will happen.</p>
                    </div>
                    <div class="ozg-contact-carousel">
                        <div class="ozg-contact-list" data-aos="fade-down" data-aos-delay="750">
                            <div class="ozg-contact-dragger slick-carousel">
                                <h1 class="ozg-contact-heading" data-href="mailto: jobs@ozzigeno.com?subject=General%20Inquiry">
                                    <div>General Inquiry <span>01</span></div>
                                </h1>
                                <h1 class="ozg-contact-heading" data-href="mailto: jobs@ozzigeno.com?subject=I%E2%80%99m%20a%20client">
                                    <div>I'm a client <span>02</span></div>
                                </h1>
                                <h1 class="ozg-contact-heading" data-href="mailto: jobs@ozzigeno.com?subject=Looking%20for%20a%20job">
                                    <div>Looking for a job <span>{{ $count }}</span></div>
                                </h1>
                            </div>
                            <a href="mailto: jobs@ozzigeno.com?subject=General%20Inquiry" class="ozg-contact-to slick-next slick-arrow"></a>
                        </div>
                        <div class="ozg-contact-items slick-nav">
                            <div class="item" data-aos="fade-right" data-aos-delay="1000">
                                <div class="item-link">
                                    <a href="javascript:;" class="item-link-inner hvr-underline-from-center" data-href="mailto: jobs@ozzigeno.com?subject=General%20Inquiry">General inquiry</a>
                                </div>
                            </div>
                            <div class="item" data-aos="fade-right" data-aos-delay="1250">
                                <div class="item-link">
                                    <a href="javascript:;" class="item-link-inner hvr-underline-from-center" data-href="mailto: jobs@ozzigeno.com?subject=I%E2%80%99m%20a%20client">I'm a client</a>
                                </div>
                            </div>
                            <div class="item" data-aos="fade-right" data-aos-delay="1500">
                                <div class="item-link">
                                    {{-- <a href="javascript:;" class="item-link-inner hvr-underline-from-center" data-href="mailto: jobs@ozzigeno.com?subject=Looking%20for%20a%20job">Looking for a job</a> --}}
                                    <a href="{{ url('/hire') }}" class="item-link-inner hvr-underline-from-center" data-href="mailto: jobs@ozzigeno.com?subject=Looking%20for%20a%20job">Looking for a job</a>
                                    <ul>
                                        @foreach ($jobs as $job)
                                            @if ($loop->iteration <= 5)
                                                <li><a href="{{ url('/hire', $job->slug) }}">{{ $job->position }}</a></li>
                                            @else
                                                <li><a href="{{ url('/hire')}}">Click for More Jobs</a></li>
                                            @endif
                                        @endforeach
                                        {{-- <li><a href="">Sr. Designer</a></li>
                                        <li><a href="">Account Director</a></li>
                                        <li><a href="">Google Analyst</a></li> --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="watermark">
                <h1 class="ozg-page-title">Contact</h1>
            </div>
        </div>
    </div>

@endsection