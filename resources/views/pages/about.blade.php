@extends('master')

@section('section')

    <div class="z-layout">
        <div class="ozg-team-teaser">
            <h1 data-aos="fade-right" data-aos-delay="250">
                <span>Our talented &amp;</span>
                <span>experienced</span>
                <span>team delivers</span>
                <span>amazing</span>
                <span>results.</span>
            </h1>
            <p data-aos="fade-right" data-aos-delay="500">We believe in the immense power and potential of a well-designed web image.</p>
        </div>
        <div class="ozg-about" id="aboutPage">
            <section class="section ozg-about-section fp-auto-height">
                <div class="ozg-about-wrapper">
                    <div class="ozg-about-grid">
                        <div class="z-page-hanger">
                            <a href="{{url('/')}}/works" class="hvr-underline-from-center">
                                <span>Case Studies</span>
                            </a>
                        </div>
                        <div class="ozg-about-grid-wrapper">
                            <div class="ozg-about-heading">
                                <h1 data-aos="fade-right" data-aos-delay="250">Breathe <span>Creativity</span></h1>
                            </div>
                            <div class="ozg-about-content">
                                <div class="ozg-about-content-grid">
                                    <div class="ozg-about-content-big">
                                        <p data-aos="fade-right" data-aos-delay="500">Our tagline represent what we do, for us, Creativity is everything. No matter it’s about design, concept, or strategy. Even in programming we have to be creative to get the best output.</p>
                                        <div class="ozg-about-thumb" data-aos="fade-right" data-aos-delay="750">
                                            <img src="{{asset('storage/laraassets/images/about/thumb/cover.png')}}" alt="About Ozzigeno">
                                        </div>
                                    </div>
                                    <div class="ozg-about-content-small">
                                        <p data-quote="&ldquo;" data-aos="fade-right" data-aos-delay="1000">We just one of the digital agency who still believe, process and creative have to work together.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="watermark">
                    <h1 class="ozg-page-title">
                        <span>About</span>
                    </h1>
                </div>
            </section>
            <section class="section ozg-service-section fp-auto-height">
                <div class="ozg-service-wrapper">
                    <div class="ozg-service-grid">
                        <div class="z-page-hanger">
                            <a href="{{url('/')}}/works" class="hvr-underline-from-center">
                                <span>Case Studies</span>
                            </a>
                        </div>
                        <div class="ozg-service-grid-wrapper">
                            <div class="ozg-service-quote">
                                <p data-quote="&ldquo;" data-aos="fade-down" data-aos-delay="250">Every single things we do, we always seek for the best.</p>
                            </div>
                            <div class="ozg-service-teaser">
                                <h1 data-aos="fade-right" data-aos-delay="500">
                                    <span>We do</span>
                                    <span>believe</span>
                                    <span>in our</span>
                                    <span>skill</span>
                                </h1>
                                <p data-aos="fade-right" data-aos-delay="750">We believe in the immense power and potential of a well-designed web image. To exploit the potential of the digital environment, we develop memorable websites and help brands with their visual image and strategy.</p>
                            </div>
                            <div class="ozg-service-details">
                                <div class="ozg-service-details-grid">
                                    <div class="ozg-service-details-items">
                                        <div class="item" data-aos="zoom-in" data-aos-delay="1000">
                                            <div class="item-thumb">
                                                <img src="{{asset('storage/laraassets/images/service/thumb/item-1.png')}}" alt="Service Item 1">
                                            </div>
                                            <div class="item-caption">360 Integrated Digital</div>
                                        </div>
                                        <div class="item" data-aos="zoom-in" data-aos-delay="1250">
                                            <div class="item-thumb">
                                                <img src="{{asset('storage/laraassets/images/service/thumb/item-2.png')}}" alt="Service Item 2">
                                            </div>
                                            <div class="item-caption">Strategy &amp; Planning</div>
                                        </div>
                                        <div class="item" data-aos="zoom-in" data-aos-delay="1500">
                                            <div class="item-thumb">
                                                <img src="{{asset('storage/laraassets/images/service/thumb/item-3.png')}}" alt="Service Item 3">
                                            </div>
                                            <div class="item-caption">Social Media Management</div>
                                        </div>
                                        <div class="item" data-aos="zoom-in" data-aos-delay="1750">
                                            <div class="item-thumb">
                                                <img src="{{asset('storage/laraassets/images/service/thumb/item-4.png')}}" alt="Service Item 4">
                                            </div>
                                            <div class="item-caption">Technology &amp; Creative</div>
                                        </div>
                                        <div class="item" data-aos="zoom-in" data-aos-delay="2000">
                                            <div class="item-thumb">
                                                <img src="{{asset('storage/laraassets/images/service/thumb/item-5.png')}}" alt="Service Item 5">
                                            </div>
                                            <div class="item-caption">Digital Media Marketing</div>
                                        </div>
                                        <div class="item" data-aos="zoom-in" data-aos-delay="2250">
                                            <div class="item-thumb">
                                                <img src="{{asset('storage/laraassets/images/service/thumb/item-6.png')}}" alt="Service Item 6">
                                            </div>
                                            <div class="item-caption">Data &amp; Insight Analyst</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="watermark">
                    <h1 class="ozg-page-title">
                        <span>Services</span>
                    </h1>
                </div>
            </section>
            <section class="section ozg-client-section fp-auto-height">
                <div class="ozg-client-wrapper">
                    <div class="ozg-client-grid">
                        <div class="z-page-hanger">
                            <a href="{{url('/')}}/contact" class="hvr-underline-from-center">
                                <span>Let's meet up</span>
                            </a>
                        </div>
                        <div class="ozg-client-grid-wrapper">
                            <div class="ozg-client-teaser">
                                <h1 data-aos="fade-right" data-aos-delay="250">
                                    <span>Your</span>
                                    <span>success is</span>
                                    <span>our success</span>
                                </h1>
                                <p data-aos="fade-right" data-aos-delay="500">Of all shapes and sizes, from all corners of the globe and remember Your success is our success.</p>
                            </div>
                            <div class="ozg-client-list">
                                @forelse ($brands as $i => $brand)
                                    @if (!empty($brand->logo))
                                    <div class="item" data-aos="zoom-in" data-aos-delay="{{ (($i + 1) * 100) + 550  }}">
                                        <div class="item-wrapper">
                                            <img src="{{asset('storage/laraassets/images/brands/' . $brand->logo)}}" alt="{{ $brand->name }}">
                                        </div>
                                    </div>
                                    @endif
                                @empty
                                    <div class="item">
                                        No brands founded
                                    </div>
                                @endforelse
                                {{-- <div class="item" data-aos="zoom-in" data-aos-delay="750">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/bahana.png')}}" alt="Bahana">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="850">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/milkuat.png')}}" alt="Milkuat">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="950">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/delfi.png')}}" alt="Delfi">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1050">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/vivere.png')}}" alt="Vivere">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1150">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/nutricia.png')}}" alt="Nutricia">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1250">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/clarishome.png')}}" alt="Clarishome">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1350">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/durex.png')}}" alt="Durex">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1450">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/sasainti.png')}}" alt="SasaInti">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1550">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/morin.png')}}" alt="Morin">
                                    </div>
                                </div>
                            </div> --}}
                            {{-- <div class="ozg-client-list">
                                <div class="item" data-aos="zoom-in" data-aos-delay="550">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/vitalis.png')}}" alt="Vitalis">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="650">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/bio-essence.png')}}" alt="BioEssence">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="750">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/bahana.png')}}" alt="Bahana">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="850">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/milkuat.png')}}" alt="Milkuat">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="950">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/delfi.png')}}" alt="Delfi">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1050">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/vivere.png')}}" alt="Vivere">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1150">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/nutricia.png')}}" alt="Nutricia">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1250">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/clarishome.png')}}" alt="Clarishome">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1350">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/durex.png')}}" alt="Durex">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1450">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/sasainti.png')}}" alt="SasaInti">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1550">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/morin.png')}}" alt="Morin">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1650">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/royal-safari-garden.png')}}" alt="Royal Safari Garden">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1750">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/danone.png')}}" alt="Danone">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1850">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/wwf.png')}}" alt="WWF">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="1950">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/wika-gedung.png')}}" alt="Wika Gedung">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="2050">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/total8plus.png')}}" alt="Total8+">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="2150">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/bebelac.png')}}" alt="Bebelac">
                                    </div>
                                </div>
                                <div class="item" data-aos="zoom-in" data-aos-delay="2250">
                                    <div class="item-wrapper">
                                        <img src="{{asset('storage/laraassets/images/client/logo/intiland.png')}}" alt="Intiland">
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="watermark">
                    <h1 class="ozg-page-title">
                        <span>Clients</span>
                    </h1>
                </div>
            </section>
            <section class="section ozg-team-section fp-auto-height">
                <div class="ozg-team-teaser forMobile">
                    <h1 data-aos="fade-right" data-aos-delay="250">
                        <span>Our talented &amp;</span>
                        <span>experienced</span>
                        <span>team delivers</span>
                        <span>amazing</span>
                        <span>results.</span>
                    </h1>
                    <p data-aos="fade-right" data-aos-delay="500">We believe in the immense power and potential of a well-designed web image.</p>
                </div>
                <div class="ozg-team-wrapper">
                    <div class="ozg-team-grid">
                        <div class="z-page-hanger">
                            <a href="{{url('/works')}}" class="hvr-underline-from-center">
                                <span>Our daily life</span>
                            </a>
                        </div>
                        <div class="ozg-team-slides">
                            <div class="ozg-team-list" id="teamList">
                                @foreach ($teams as $team)
                                    <div class="item" data-aos="fade-down">
                                        <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/' . $team->photo)}})" data-image-alt="{{asset('storage/laraassets/images/team/' . $team->photo)}}"></div>
                                        <div class="team-info">
                                            <h3 class="team-name">{{ $team->callName }}</h3>
                                            <h4 class="team-title">{{ $team->title }}</h4>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            {{-- <div class="ozg-team-list" id="teamList">
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Kriston.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Kriston.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Kriston</h3>
                                        <h4 class="team-title">Chief Elaborate Officer</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Ayu.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Ayu.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Ayu</h3>
                                        <h4 class="team-title">Chief Inspiration Officer</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Sylvia.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Sylvia.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Sylvia</h3>
                                        <h4 class="team-title">PRINCESS (Profesional Relation of Client Happiness)</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Chika.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Chika.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Chika</h3>
                                        <h4 class="team-title">Client-Heart Holder</h4>
                                    </div>
                                </div>
                                <div class="item d-none" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Dina.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Dina.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Dina</h3>
                                        <h4 class="team-title">AXE (Account Xuper Executive)</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Billy.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Billy.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Billy</h3>
                                        <h4 class="team-title">(M)Ad Men</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Yori.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Yori.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Yori</h3>
                                        <h4 class="team-title">Design Artmirer</h4>
                                    </div>
                                </div>
                                <div class="item d-none" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/fangga.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/fangga.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Fangga</h3>
                                        <h4 class="team-title">Knight of Design Norms</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Esterina.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Esterina.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Ester</h3>
                                        <h4 class="team-title">Bachelor of Visual Magician</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Giovanni.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Giovanni.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Gio</h3>
                                        <h4 class="team-title">Secret Visual Agent</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Febri.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Febri.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Febri</h3>
                                        <h4 class="team-title">Illustrious Visual Alchemist</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Fina.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Fina.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Fina</h3>
                                        <h4 class="team-title">Brand Internetologist</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Tisha.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Tisha.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Tisha</h3>
                                        <h4 class="team-title">Content Creator Warrior</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Candhy.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Candhy.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Candhy</h3>
                                        <h4 class="team-title">Furiosa (Further Ozzigeno Socmed Assistant)</h4>
                                    </div>
                                </div>
                                <div class="item d-none" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Faqih.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Faqih.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Faqih</h3>
                                        <h4 class="team-title">Master of Internet Language &amp; Forecasting</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Agung.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Agung.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Agung</h3>
                                        <h4 class="team-title">Functional &amp; Usable Code Keeper</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Wahyu.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Wahyu.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Wahyu</h3>
                                        <h4 class="team-title">Amazing WYSIWYG Maker</h4>
                                    </div>
                                </div>
                                <div class="item" data-aos="fade-down">
                                    <div class="pieces" style="background-image: url({{asset('storage/laraassets/images/team/Eko.jpg')}})" data-image-alt="{{asset('storage/laraassets/images/team/Eko.jpg')}}"></div>
                                    <div class="team-info">
                                        <h3 class="team-name">Eko</h3>
                                        <h4 class="team-title">Virtuous Code Artisan</h4>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection