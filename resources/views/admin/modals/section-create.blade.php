<div class="modal fade" id="newSection" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Create new section</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('section.store', ['slug' => 'client']) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('post')
                    <input type="hidden" name="id_brand" class="form-control" value="{{ $client->id }}">
                    <div class="form-group">
                        <label for="InputSectionName">Section Name</label>
                        <input type="text" name="section_name" class="form-control" id="InputSectionName" placeholder="Section Name">
                    </div>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>