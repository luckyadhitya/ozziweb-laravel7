<div class="modal fade" id="delete{{ $user->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus {{ $user->callName }} ?</h5>
                <form action="/admin/team/{{ $user->id }}" method="post">

                    @csrf
                    @method('DELETE')
                    <div class="text-right">
                        <button class="btn btn-sm btn-danger float-right">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>