<div class="modal fade" id="edit{{ $job->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Edit hire detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/admin/hire/{{ $job->id }}" method="post" autocomplete="off">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="inputStatus">Status :</label>
                        <select name="status" id="inputStatus" class="form-control w-25">
                            <option value="1" {!! $job->status == 1 ? 'selected' : '' !!}>Open</option>
                            <option value="0" {!! $job->status == 0 ? 'selected' : '' !!}>Closed</option>
                        </select>
                        {{-- <input type="checkbox" class="ozshowhide" name="flag" value="1" {!! $job->status == 1 ? 'checked' : '' !!} data-oz-switch data-off-color="default" data-on-color="success" data-token="{{ csrf_token() }}"> --}}
                    </div>
                    <div class="form-group">
                        <label for="InputPosition">Position :</label>
                        <input type="text" name="position" class="form-control" id="InputPosition" placeholder="Nama Posisi" value="{{ $job->position }}">
                    </div>
                    <div class="form-group">
                        <label for="InputResponsibilities">Responsibilities :</label>
                        <div class="form-group">
                            <textarea class="tinyMCE" name="responsibilities">{{ $job->responsibilities }}</textarea>
                        </div>
                        {{-- <textarea class="form-control resize-none" name="responsibilities" id="InputResponsibilities" cols="30" rows="10">{{ $job->responsibilities }}</textarea> --}}
                    </div>
                    <div class="form-group">
                        <label for="InputRequirement">Requirement :</label>
                        <div class="form-group">
                            <textarea class="tinyMCE" name="requirement">{{ $job->requirement }}</textarea>
                        </div>
                        {{-- <textarea class="form-control resize-none" name="requirement" id="InputRequirement" cols="30" rows="10">{{ $job->requirement }}</textarea> --}}
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>