<div class="modal fade" id="delete{{ $section->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus section {{ $section->section_name }} ?</h5>
                <form action="{{ route('section.destroy', ['slug' => 'client', 'section' => $section->id]) }}" method="post">

                    @csrf
                    @method('DELETE')

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>