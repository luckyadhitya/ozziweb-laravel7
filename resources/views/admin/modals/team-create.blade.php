<div class="modal fade" id="create" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('team.create') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('get')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="InputNamaLengkap">Nama Lengkap</label>
                                <input type="text" name="fullName" class="form-control" id="InputNamaLengkap" placeholder="Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <label for="InputNamaPanggilan">Nama Panggilan</label>
                                <input type="text" name="callName" class="form-control" id="InputNamaPanggilan" placeholder="Nama Panggilan">
                            </div>
                            <div class="form-group">
                                <label for="InputTitle">Title</label>
                                <input type="text" name="title" class="form-control" id="InputTitle" placeholder="Title">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" width="200">
                            <div class="form-group mb-5">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="FotoDiri" name="newPhoto">
                                    <label class="custom-file-label" for="FotoDiri">Choice File</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>