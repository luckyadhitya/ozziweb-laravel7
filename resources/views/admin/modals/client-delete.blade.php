<div class="modal fade" id="delete{{ $client->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus {{ $client->name }} ?</h5>
                <form action="{{ route('client.destroy', $client->id) }}" method="post">

                    @csrf
                    @method('DELETE')

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>