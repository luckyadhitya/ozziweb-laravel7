<div class="modal fade" id="deleteImage-logo" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus logo {{ $client->name }} ?</h5>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->logo) }}" alt="{{ $client->name }}">
                <form action="{{ route('client.imgdestroy', $client->id) }}" method="post">

                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="image" value="logo">

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteImage-background" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus background {{ $client->name }} ?</h5>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->background) }}" alt="{{ $client->name }}">
                <form action="{{ route('client.imgdestroy', $client->id) }}" method="post">

                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="image" value="background">

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteImage-cover" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus cover {{ $client->name }} ?</h5>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->cover) }}" alt="{{ $client->name }}">
                <form action="{{ route('client.imgdestroy', $client->id) }}" method="post">

                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="image" value="cover">

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteImage-thumbnail" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus thumbnail {{ $client->name }} ?</h5>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->thumb) }}" alt="{{ $client->name }}">
                <form action="{{ route('client.imgdestroy', $client->id) }}" method="post">

                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="image" value="thumbnail">

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteImage-item" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Ingin menghapus item {{ $client->name }} ?</h5>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->item) }}" alt="{{ $client->name }}">
                <form action="{{ route('client.imgdestroy', $client->id) }}" method="post">

                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="image" value="item">

                    <button class="btn btn-sm btn-danger float-right">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>