<div class="modal fade" id="create" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Open New Hire</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('hire.store') }}" method="post" autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label for="InputPosition">Position :</label>
                        <input type="text" name="position" class="form-control" id="InputPosition" placeholder="Nama Posisi">
                    </div>
                    <div class="form-group">
                        <label for="InputResponsibilities">Responsibilities :</label>
                        <div class="form-group">
                            <textarea class="tinyMCE" name="responsibilities"></textarea>
                        </div>
                        {{-- <textarea class="form-control resize-none" name="responsibilities" id="InputResponsibilities" cols="30" rows="10">{{ $job->responsibilities }}</textarea> --}}
                    </div>
                    <div class="form-group">
                        <label for="InputRequirement">Requirement :</label>
                        <div class="form-group">
                            <textarea class="tinyMCE" name="requirement"></textarea>
                        </div>
                        {{-- <textarea class="form-control resize-none" name="requirement" id="InputRequirement" cols="30" rows="10">{{ $job->requirement }}</textarea> --}}
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>