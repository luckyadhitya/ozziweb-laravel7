<div class="modal fade" id="create" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Add new client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('client.store') }}" method="post" autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label for="InputNamaClient">Nama Client</label>
                        <input type="text" name="brandName" class="form-control" id="InputNamaClient" placeholder="Nama Brand">
                    </div>
                    {{-- <div class="form-group">
                        <p class="text-gray">Slug : <span id="tagSlug">-</span></p>
                        <input type="hidden" name="brandSlug" class="form-control" id="InputSlug" placeholder="Slug">
                    </div> --}}
                    <div class="form-group">
                        <label for="InputService">Service</label>
                        <input type="text" name="brandService" class="form-control" id="InputService" placeholder="Service">
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>