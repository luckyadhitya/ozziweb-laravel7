<div class="modal fade" id="showImage-logo" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->logo) }}" alt="{{ $client->name }}">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="showImage-background" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->background) }}" alt="{{ $client->name }}">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="showImage-cover" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->cover) }}" alt="{{ $client->name }}">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="showImage-thumb" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->thumb) }}" alt="{{ $client->name }}">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="showImage-item" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <img class="img-thumbnail p-0 border-0 mx-auto d-block" src="{{ asset('storage/laraassets/images/brands/'. $client->item) }}" alt="{{ $client->name }}">
            </div>
        </div>
    </div>
</div>