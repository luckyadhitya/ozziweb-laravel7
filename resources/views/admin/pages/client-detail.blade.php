@extends('admin.app')

@section('content-dashboard')
    @include('admin.modals.client-image')
    @include('admin.modals.client-image-delete')
    <div class="card col-md-11 mx-auto">
        <div class="card-header">
            <h1 class="card-title">Customize client</h1>
        </div>
        <div class="card-body">
            <form action="{{ route('client.update', $client->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="InputNamaBrand">Nama Client</label>
                        <input type="text" name="brandName" class="form-control" id="InputNamaBrand" placeholder="Nama Brand" value="{{ $client->name }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="InputSlug">Slug</label>
                        <input type="text" name="brandSlug" class="form-control" id="InputSlug" placeholder="Slug" value="{{ $client->slug }}" readonly>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="InputService">Service</label>
                        <input type="text" name="brandService" class="form-control" id="InputService" placeholder="Service" value="{{ $client->service }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ( empty($client->logo) )
                                <span class="text-danger">
                                    <i class="fas fa-times"></i>
                                </span>
                                <label>Logo</label>
                            @else
                                <span class="text-success">
                                    <i class="fas fa-check"></i>
                                </span>
                                <input type="hidden" name="oldLogo" value="{{ $client->logo }}">
                                <label>Logo
                                    <button type="button" class="btn btn-sm btn-flat text-primary" data-toggle="modal" data-target="#showImage-logo"><i class="fas fa-eye"></i></button>|
                                    <button type="button" class="btn btn-sm btn-flat text-danger" data-toggle="modal" data-target="#deleteImage-logo"><i class="fas fa-trash"></i></button>
                                </label>
                            @endif
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="logo" name="newLogo">
                                <label class="custom-file-label" for="logo">Masukan Logo</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ( empty($client->cover) )
                                <span class="text-danger">
                                    <i class="fas fa-times"></i>
                                </span>
                                <label>Cover</label>
                            @else
                                <span class="text-success">
                                    <i class="fas fa-check"></i>
                                </span>
                                <input type="hidden" name="oldCover" value="{{ $client->cover }}">
                                <label>Cover
                                    <button type="button" class="btn btn-sm btn-flat text-primary" data-toggle="modal" data-target="#showImage-cover"><i class="fas fa-eye"></i></button>|
                                    <button type="button" class="btn btn-sm btn-flat text-danger" data-toggle="modal" data-target="#deleteImage-cover"><i class="fas fa-trash"></i></button>
                                </label>
                            @endif
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="cover" name="newCover">
                                <label class="custom-file-label" for="cover">Masukan Cover</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ( empty($client->thumb) )
                                <span class="text-danger">
                                    <i class="fas fa-times"></i>
                                </span>
                                <label>Thumb</label>
                            @else
                                <span class="text-success">
                                    <i class="fas fa-check"></i>
                                </span>
                                <input type="hidden" name="oldThumb" value="{{ $client->thumb }}">
                                <label>Thumb
                                    <button type="button" class="btn btn-sm btn-flat text-primary" data-toggle="modal" data-target="#showImage-thumb"><i class="fas fa-eye"></i></button>|
                                    <button type="button" class="btn btn-sm btn-flat text-danger" data-toggle="modal" data-target="#deleteImage-thumb"><i class="fas fa-trash"></i></button>
                                </label>
                            @endif
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="thumb" name="newThumb">
                                <label class="custom-file-label" for="thumb">Masukan Thumb</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ( empty($client->background) )
                                <span class="text-danger">
                                    <i class="fas fa-times"></i>
                                </span>
                                <label>Background</label>
                            @else
                                <span class="text-success">
                                    <i class="fas fa-check"></i>
                                </span>
                                <input type="hidden" name="oldBackground" value="{{ $client->background }}">
                                <label>Background
                                    <button type="button" class="btn btn-sm btn-flat text-primary" data-toggle="modal" data-target="#showImage-background"><i class="fas fa-eye"></i></button>|
                                    <button type="button" class="btn btn-sm btn-flat text-danger" data-toggle="modal" data-target="#deleteImage-background"><i class="fas fa-trash"></i></button>
                                </label>
                            @endif
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="background" name="newBackground">
                                <label class="custom-file-label" for="background">Masukan Background</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ( empty($client->item) )
                                <span class="text-danger">
                                    <i class="fas fa-times"></i>
                                </span>
                                <label>Item</label>
                            @else
                                <span class="text-success">
                                    <i class="fas fa-check"></i>
                                </span>
                                <input type="hidden" name="oldItem" value="{{ $client->item }}">
                                <label>Item
                                    <button type="button" class="btn btn-sm btn-flat text-primary" data-toggle="modal" data-target="#showImage-item"><i class="fas fa-eye"></i></button>|
                                    <button type="button" class="btn btn-sm btn-flat text-danger" data-toggle="modal" data-target="#deleteImage-item"><i class="fas fa-trash"></i></button>
                                </label>
                            @endif
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="item" name="newItem">
                                <label class="custom-file-label" for="item">Masukan Item</label>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-4">
                        @if ( empty($client->logo) )
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" width="100">
                        @else
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/brands/'. $client->logo) }}" alt="{{ $client->name }}" width="100">
                            <input type="hidden" name="oldLogo" value="{{ $client->logo }}">
                        @endif
                        <div class="form-group mb-5">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="logo" name="newLogo">
                                <label class="custom-file-label" for="logo">Masukan Logo</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if ( empty($client->cover) )
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" height="50px">
                        @else
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/brands/'. $client->cover) }}" alt="{{ $client->name }}" height="50px">
                            <input type="hidden" name="oldCover" value="{{ $client->cover }}">
                        @endif
                        <div class="form-group mb-5">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="cover" name="newCover">
                                <label class="custom-file-label" for="cover">Masukan Cover</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if ( empty($client->thumb) )
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" width="100">
                        @else
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/brands/'. $client->thumb) }}" alt="{{ $client->name }}" width="100">
                            <input type="hidden" name="oldThumb" value="{{ $client->thumb }}">
                        @endif
                        <div class="form-group mb-5">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="thumb" name="newThumb">
                                <label class="custom-file-label" for="thumb">Masukan Thumbnail</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if ( empty($client->background) )
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" width="100">
                        @else
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/brands/'. $client->background) }}" alt="{{ $client->name }}" width="100">
                            <input type="hidden" name="oldBackground" value="{{ $client->background }}">
                        @endif
                        <div class="form-group mb-5">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="background" name="newBackground">
                                <label class="custom-file-label" for="background">Masukan Background</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if ( empty($client->item) )
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" width="100">
                        @else
                            <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/brands/'. $client->item) }}" alt="{{ $client->name }}" width="100">
                            <input type="hidden" name="oldItem" value="{{ $client->item }}">
                        @endif
                        <div class="form-group mb-5">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="item" name="newItem">
                                <label class="custom-file-label" for="item">Masukan Item</label>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">
                        Simpan
                        {{-- <i class="fas fa-save"></i> --}}
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <div class="card-title">Manage Client <span class="badge badge-info">{{ $client->name }}</span></div>
            <div class="card-tools">
                <button class="btn btn-primary" data-toggle="modal" data-target="#newSection">Add Section</button>
            </div>
            @include('admin.modals.section-create')
        </div>
        <div class="card-body">
            @foreach ($sections as $section)
                <div class="card card-lightblue collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Section {{ $section->section_name }}</h3>
            
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('section.update', ['slug' => 'client', 'section' => $section->id]) }}" method="post">

                            @csrf
                            @method('put')

                            {{-- <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <textarea class="tinyMCE" name="content">{{ $section->content }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <img class="img-thumbnail p-0 border-0 mx-auto d-block mb-3" src="{{ asset('storage/laraassets/images/general/add-image.png') }}" alt="Upload Foto" width="200">
                                    <div class="form-group mb-5">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="FotoDiri" name="newPhoto">
                                            <label class="custom-file-label" for="FotoDiri">Choice File</label>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <textarea class="tinyMCE" name="content">{{ $section->content }}</textarea>
                            </div>

                            <div class="text-right">
                                <button class="btn btn-sm btn-primary">
                                    Simpan
                                    {{-- <i class="fas fa-save"></i> --}}
                                </button>
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{ $section->id }}">
                                    Delete
                                    {{-- <i class="fas fa-trash"></i> --}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                @include('admin.modals.section-delete')
            @endforeach
        </div>
    </div>
@endsection