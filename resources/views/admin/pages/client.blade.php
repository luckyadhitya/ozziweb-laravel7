@extends('admin.app')

@section('content-dashboard')
@include('admin.modals.client-create')
    <div class="mb-3 text-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Add new client</button>
    </div>
    <div class="card card-info">
        <div class="card-header">
            <h1 class="card-title">Client list</h1>
        </div>
        <div class="card-body">
            <table class="table table-striped table-responsive-md sortable-client">
                <thead class="thead-light">
                    <th class="text-center align-middle">No</th>
                    <th class="text-center align-middle">Client Name</th>
                    {{-- <th class="text-center align-middle">Slug</th> --}}
                    <th class="text-center align-middle">Service</th>
                    <th class="text-center align-middle" style="max-width: 50px;">Page</th>
                    <th class="text-center align-middle" style="max-width: 50px;">Logo</th>
                    <th class="text-center align-middle" style="max-width: 50px;">Cover</th>
                    <th class="text-center align-middle" style="max-width: 50px;">Item</th>
                    <th class="text-center align-middle" style="max-width: 75px;">Thumbnail</th>
                    <th class="text-center align-middle">***</th>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($data as $client)
                        <tr data-id="{{ $client->id }}">
                            <td class="text-center align-middle">{{ $client->sort }}</td>
                            <td class="align-middle">{{ $client->name }}</td>
                            {{-- <td class="align-middle">{{ $client->slug }}</td> --}}
                            <td class="align-middle">{{ $client->service }}</td>
                            <td class="text-center align-middle">
                                @if (trim($client->url))
                                    <span class="text-success">
                                        <i class="fas fa-check"></i>
                                    </span>
                                @else
                                    <span class="text-danger">
                                        <i class="fas fa-times"></i>
                                    </span>
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                @if (trim($client->logo))
                                    <span class="text-success">
                                        <i class="fas fa-check"></i>
                                    </span>
                                @else
                                    <span class="text-danger">
                                        <i class="fas fa-times"></i>
                                    </span>
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                @if (trim($client->cover))
                                    <span class="text-success">
                                        <i class="fas fa-check"></i>
                                    </span>
                                @else
                                    <span class="text-danger">
                                        <i class="fas fa-times"></i>
                                    </span>
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                @if (trim($client->item))
                                    <span class="text-success">
                                        <i class="fas fa-check"></i>
                                    </span>
                                @else
                                    <span class="text-danger">
                                        <i class="fas fa-times"></i>
                                    </span>
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                @if (trim($client->thumb))
                                    <span class="text-success">
                                        <i class="fas fa-check"></i>
                                    </span>
                                @else
                                    <span class="text-danger">
                                        <i class="fas fa-times"></i>
                                    </span>
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                {{-- <button class="btn btn-sm btn-primary">Manage</button> --}}
                                {{-- <a href="{{ route('work.update', $client->id) }}" class="btn btn-sm {!! $client->flag == 1 ? 'btn-success' : 'btn-danger' !!}">
                                    {!! $client->flag == 1 ? '<i class="fas fa-eye"></i>' : '<i class="fas fa-eye-slash"></i>' !!}
                                </a> --}}
                                {{-- <button class="btn btn-sm {!! $client->flag == 1 ? 'btn-success' : 'btn-danger' !!} oz-btn-showhide" data-token="{{ csrf_token() }}" data-url="{{ route('work.toggle', $client->id) }}">
                                    {!! $client->flag == 1 ? '<i class="fas fa-eye"></i>' : '<i class="fas fa-eye-slash"></i>' !!}
                                </button> --}}
                                    {{-- <input type="checkbox" name="flag" {!! $client->flag == 1 ? 'checked' : '' !!} data-oz-switch> --}}
                                <input type="checkbox" class="ozshowhide" name="flag" {!! $client->flag == 1 ? 'checked' : '' !!} data-oz-switch data-off-color="default" data-on-color="success" data-token="{{ csrf_token() }}" data-url="{{ route('client.toggle', $client->id) }}">
                                <a href="{{ route('section.index', ['slug' => 'client','id' => $client->id]) }}" class="btn btn-sm btn-outline-primary">
                                    <i class="fas fa-edit"></i>
                                </a>
                                
                                <button class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete{{ $client->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                                @include('admin.modals.client-delete')
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection