@extends('admin.app')

@section('content-dashboard')
    <div class="card">
        <div class="card-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <th>No</th>
                    <th>Brand Name</th>
                    <th>Slug</th>
                    <th>Logo</th>
                    <th>Cover</th>
                    <th>Thumbnail</th>
                    <th>***</th>
                </thead>
            </table>
        </div>
    </div>
@endsection