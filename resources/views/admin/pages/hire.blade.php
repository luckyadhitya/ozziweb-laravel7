@extends('admin.app')

@section('content-dashboard')
@include('admin.modals.hire-create')
    <div class="mb-3 text-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Open New Hire</button>
    </div>
    <div class="card card-info">
        <div class="card-header">
            <h1 class="card-title">Position List</h1>
        </div>
        <div class="card-body">
            <table class="table table-striped table-responsive-md sortable-client">
                <thead class="thead-light">
                    <th class="text-center align-middle" style="width: 50px">No</th>
                    <th class="text-center align-middle">Position</th>
                    <th class="text-center align-middle">Status</th>
                    <th class="text-center align-middle">***</th>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($data as $job)
                        <tr data-id="{{ $job->id }}">
                            <td class="text-center align-middle">{{ $loop->iteration }}</td>
                            <td class="text-center align-middle">{{ $job->position }}</td>
                            <td class="text-center align-middle">
                                @if (trim($job->status))
                                    <span class="text-success">
                                        Open
                                    </span>
                                @else
                                    <span class="text-danger">
                                        Closed
                                    </span>
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#edit{{ $job->id }}">
                                    <i class="fas fa-edit"></i>
                                </button>
                                
                                <button class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete{{ $job->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @include('admin.modals.hire-edit')
                        @include('admin.modals.hire-delete')
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection