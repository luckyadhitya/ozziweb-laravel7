@extends('admin.app')

@section('content-dashboard')
    <div class="col-md-12 mb-2 text-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Add New Team</button>
        {{-- @if ($errors->any())
            @foreach ($errors->all() as $err)
                {{ $err }}
            @endforeach
        @endif --}}
    </div>
    @include('admin.modals.team-create')
    <div class="card">
        <div class="card-body">
            <table class="table table-striped table-bordered sortable-team">
                <thead class="thead-dark text-center">
                    <th width="10">No</th>
                    <th class="w-25">Nama Lengkap</th>
                    <th>Nama Panggilan</th>
                    <th>Title</th>
                    <th width="50">Foto</th>
                    <th>***</th>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    {{-- <tr>
                        <td class="text-center">0</td>
                        <td>Lucky</td>
                        <td>Lucky</td>
                        <td>New Programmer</td>
                        <td class="text-center">
                            <img src="{{ asset('storage/laraassets/images/team/Lucky.jpeg') }}" alt="Lucky Adhitya" width="50">
                            Lucky.jpeg
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-sm btn-primary">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-danger">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr> --}}
                    @foreach ($data as $i => $user )
                        <tr data-id="{{ $user->id }}">
                            <td class="text-center align-middle">{{ $i + 1 }}</td>
                            <td class="align-middle">{{ $user->fullName }}</td>
                            <td class="align-middle">{{ $user->callName }}</td>
                            <td class="align-middle">{{ $user->title }}</td>
                            <td class="text-center align-middle">
                                <img src="{{ asset('storage/laraassets/images/team/'.$user->photo) }}" alt="{{ $user->name }}" width="50">
                                {{-- {{ $user->photo }} --}}
                            </td>
                            <td class="text-center align-middle">
                                <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#edit{{ $user->id }}">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-outline-danger"  data-toggle="modal" data-target="#delete{{ $user->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @include('admin.modals.team-delete')
                        @include('admin.modals.team-edit')
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection