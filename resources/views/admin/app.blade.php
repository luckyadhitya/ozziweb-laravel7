<!DOCTYPE html>
<html lang="en">
  @include('admin.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  {{-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class=" animation__wobble" src="{{asset('storage/laraassets/images/general/ozzigeno-logo.png')}}" alt="{{ config('app.name', 'Laravel') }}" width="200">
  </div> --}}

  <!-- Navbar -->
  @include('admin.navbar')

  <!-- Main Sidebar Container -->
  @include('admin.sidebar')

  <!-- Content Wrapper. Contains page content -->
  @include('admin.content')

  @include('admin.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('admin.script')
</body>
</html>
