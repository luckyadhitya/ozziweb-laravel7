<!-- jQuery -->
<script src="{{ asset('storage/adminlte') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('storage/adminlte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
{{-- <script src="{{ asset('storage/adminlte') }}/plugins/sweetalert2/sweetalert2.min.js"></script> --}}
<!-- Toastr -->
<script src="{{ asset('storage/adminlte') }}/plugins/toastr/toastr.min.js"></script>
<!-- bs-custom-file-input -->
<script src="{{ asset('storage/adminlte') }}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- JQVMap -->
<script src="{{ asset('storage/adminlte') }}/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="{{ asset('storage/adminlte') }}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('storage/adminlte') }}/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('storage/adminlte') }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script>
  $(function () {
    bsCustomFileInput.init();
  });
</script>

{{-- Notification Controll --}}
@if ( session('message') )
  @if (session('message')['status'])
    <script>
      toastr.success("{{ session('message')['message'] }}")
    </script>
  @else
    <script>
      toastr.error("{{ session('message')['message'] }}")
    </script>
  @endif
@endif
@if ($errors->any())
    @foreach ($errors->all() as $err)
      <script>
        toastr.error("{{ $err }}")
      </script>
    @endforeach
@endif

<!-- Bootstrap Switch -->
<script src="{{ asset('storage/adminlte') }}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

<!-- AdminLTE App -->
<script src="{{ asset('storage/adminlte') }}/dist/js/adminlte.js"></script>

{{-- TinyMCE --}}
{{-- <script src="{{ asset('storage/js') }}/tinymce/tinymce.min.js" referrerpolicy="origin"></script> --}}
{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}
<script src="https://cdn.tiny.cloud/1/ryhja4ukez1ha3no4cry5pehequ184e8oyji2rsc8a4hx4g7/tinymce/5/tinymce.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.15.0/Sortable.min.js"></script>

<script src="{{ asset('storage') }}/laraassets/js/mainscript-dist.js"></script>

<script>
    // tinymce.init({
    //     selector: '#tinyMCE',
    //     menubar: false,
    //     plugins: "link image code",
    //     toolbar: 'undo redo | styleselect | forecolor | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
    //     toolbar_location: 'bottom',
    // });
    tinymce.init({
      selector: '.tinyMCE',
      menubar: false,
      plugins: 'link image code lists',
      toolbar: 'undo redo | styleselect | forecolor | bullist | bold underline italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });

    $(function() {
      $("input[data-oz-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      })
    })
</script>