<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">        
        <meta name="author" content="Ozzigeno Studio">
        <meta name="description" content="404 Page Not Found | Ozzigeno Studio Digital Agency">
        
        <!-- Mobile Stuff -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msapplication-tap-highlight" content="no">
        
        <!-- Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="404 Page Not Found | Ozzigeno Studio Digital Agency">
        <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon.png">
        
        <!-- Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="404 Page Not Found | Ozzigeno Studio Digital Agency">
        <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">
        
        <!-- Windows 8 -->
        <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">        
        
        <!-- General Tags -->
        <meta name="theme-color" content="#000000">        
        <link rel="shortcut icon" href="images/favicon.png">        
        <title>404 Page Not Found | Ozzigeno Studio Digital Agency</title>
        <link rel="stylesheet" href="{{asset('storage/laraassets/css/fullpage.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('storage/laraassets/css/all.css')}}" type="text/css">
    </head>
    <body data-aos-easing="ease" data-aos-duration="1000" data-aos-delay="0">
        <div class="oz-wrapper">
            <div class="oz-container">
                <div class="z-wrapper">
                    <div class="z-page-not-found">
                        <div class="z-page-not-found-wrapper">
                            <div class="z-page-not-found-thumb">
                                <img src="{{asset('storage/laraassets/images/general/404-page-not-found.png')}}" alt="404 Page Not Found" srcset="">
                            </div>
                            <div class="z-page-not-found-details">
                                <div class="text">
                                    <span>Page</span>
                                    <span>Not</span>
                                    <span>Found !</span>
                                </div>
                                <div class="button">
                                    <a href="{{url()->previous()}}" class="button-to">Go Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Scripts -->
        {{-- <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
        <script src="js/AnimatedImagePieces/imagesloaded.pkgd.min.js"></script>
        <script src="js/AnimatedImagePieces/anime.min.js"></script>
        <script src="js/AnimatedImagePieces/main.js"></script>
        <script src="js/AnimatedMenuIcon/segment.min.js"></script>
        <script src="js/AnimatedMenuIcon/ease.min.js"></script>
        <script src="js/scrolloverflow.min.js"></script>
        <script src="js/fullpage.min.js"></script>
        <script src="js/aos.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js"></script>
        <script src="js/apps.js"></script>
        <script src="js/script.js"></script> --}}
        <!-- <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-XXXXXXXX-X', 'auto');
            ga('send', 'pageview');
        </script> -->
    </body>
</html>