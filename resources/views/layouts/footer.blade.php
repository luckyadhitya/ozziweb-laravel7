        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js"></script>
        <script src="{{url('storage/laraassets/js/AnimatedImagePieces/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{url('storage/laraassets/js/AnimatedImagePieces/anime.min.js')}}"></script>
        <script src="{{url('storage/laraassets/js/AnimatedImagePieces/main.js')}}"></script>
        <script src="{{url('storage/laraassets/js/AnimatedMenuIcon/segment.min.js')}}"></script>
        <script src="{{url('storage/laraassets/js/AnimatedMenuIcon/ease.min.js')}}"></script>
        <script src="{{url('storage/laraassets/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{url('storage/laraassets/js/aos.js')}}"></script>
        <script src="{{url('storage/laraassets/js/responsiveslides.min.js')}}"></script>

        @if ($page == '' || $page == 'home' || $page == 'about' || $page == 'contact')
            <script src="{{url('storage/laraassets/js/scrolloverflow.min.js')}}"></script>
            @endif
        <script src="{{url('storage/laraassets/js/fullpage.min.js')}}"></script>

        <script src="{{url('storage/laraassets/js/slick.min.js')}}"></script>
        <script src="{{url('storage/laraassets/js/bundle.js')}}"></script>
        <script>
            @if ($page == 'home' || $page == '')
                try {
                    const menuItems = Array.from(document.querySelectorAll('#homeMenu > .menu-item'));
                    
                    const piecesObj = new Pieces(document.querySelector('#homeSlideshow > .pieces'), {
                        pieces: {rows: 12, columns: 9},
                        delay: [0,75],
                        hasTilt: true,
                        tilt: {
                            maxRotationX: -0.1,
                            maxRotationY: -0.1, 
                            maxTranslationX: -0.5, 
                            maxTranslationY: -0.2
                        }
                    });
                    
                    let isAnimating = false;
                    let current = 0;

                    const openImage = (ev, item, pos) => {
                        ev.preventDefault();
                        if ( isAnimating || current === pos ) {
                            return false;
                        }
                        isAnimating = true;
                        menuItems[current].classList.remove('menu-item--current');
                        current = pos;
                        menuItems[current].classList.add('menu-item--current');
                        const imgsrc = item.dataset.image;

                        piecesObj.animate({
                            duration: 200,
                            easing: 'easeOutQuad',
                            delay: (t,i,l) => {
                                return parseInt(t.dataset.row) * parseInt(t.dataset.delay);
                            },
                            translateX: (t,i) => {
                                return anime.random(-50,50)+'px';
                            },
                            translateY: (t,i) => { 
                                return anime.random(-800,-200)+'px';
                            },
                            opacity: 0,
                            complete: () => {
                                piecesObj.setImage(imgsrc);

                                piecesObj.animate({
                                    duration: 500,
                                    easing: [0.3,1,0.3,1],
                                    delay: (t,i,l) => {
                                        return parseInt(t.dataset.row) * parseInt(t.dataset.delay);
                                    },
                                    translateX: 0,
                                    translateY: () => {
                                        return [anime.random(200,800)+'px','0px'];
                                    },
                                    opacity: {
                                        value: 1,
                                        duration: 500,
                                        easing: 'linear'
                                    },
                                    complete: () => {
                                        isAnimating = false;
                                    }
                                });
                            }
                        });
                    };

                    menuItems.forEach((item,pos) => item.addEventListener('click', (ev) => {
                        openImage(ev,item,pos);
                        var linknya = item.getAttribute('data-href');
                        document.getElementsByClassName('link')[0].setAttribute('href', linknya);
                    }));
                } catch (error) {   
                    console.log('home');
                }
            @elseif ($page == 'about')
                try {
                    if($(window).width() > 800) {
                        Array.from(document.querySelectorAll('#teamList .pieces')).forEach((el,pos) => {
                            const piecesObj = new Pieces(el, { 
                                pieces: {rows: 5, columns: 4},
                                delay: [0,25],
                                bgimage: el.dataset.imageAlt
                            });
                            el.addEventListener('mouseenter', () => animateOut(piecesObj, pos));
                            el.addEventListener('touchstart', () => animateOut(piecesObj, pos));
                            el.addEventListener('mouseleave', () => animateIn(piecesObj, pos));
                            el.addEventListener('touchend', () => animateOut(piecesObj, pos));
                        });
                        
                        const animateOut = (instance,pos) => instance.animate({
                            delay: (t,i,l) => {
                                return parseInt(t.dataset.column)*parseInt(t.dataset.delay);
                            },
                            translateX: [
                                {
                                    value: pos % 2 === 1 ? (t,i) => {
                                        return anime.random(75,150)+'px';
                                    } : (t,i) => {
                                        return anime.random(-150,-75)+'px';
                                    },
                                    duration: 500,
                                    easing: 'easeOutQuad'
                                },
                                {
                                    value: pos % 2 === 1 ? (t,i) => {
                                        return anime.random(-1000,-400)+'px';
                                    } : (t,i) => {
                                        return anime.random(400,1000)+'px';
                                    },
                                    duration: 400,
                                    easing: 'easeOutExpo'
                                }
                            ],
                            translateY: [
                                {
                                    value: (t,i) => {
                                        return anime.random(-125,-75)+'px';
                                    },
                                    duration: 500,
                                    easing: 'easeOutQuad'
                                },
                                {
                                    value: (t,i) => {
                                        return t.dataset.row < instance.getTotalRows()/2 ? anime.random(100,200)+'px' : anime.random(-200,-100)+'px';
                                    },
                                    duration: 400,
                                    easing: 'easeOutExpo'
                                }
                            ],
                            opacity: {
                                value: 0,
                                delay: 500,
                                duration: 400,
                                easing: 'easeOutExpo'
                            }
                        });
                        
                        const animateIn = (instance,pos) => instance.animate({
                            duration: 500,
                            easing: [0.8,1,0.3,1],
                            delay: (t,i) => {
                                return pos % 2 === 1 ? 
                                    (instance.getTotalColumns() - parseInt(t.dataset.column)) * parseInt(t.dataset.delay) :
                                    parseInt(t.dataset.column) * parseInt(t.dataset.delay);
                            },
                            translateX: '0px',
                            translateY: '0px',
                            opacity: {
                                value: 1,
                                duration: 500,
                                easing: 'linear'
                            }
                        });
                    }
                } catch (error) {   
                    console.log('about');
                }
            @endif
        </script>
		<script src="https://apis.google.com/js/platform.js" async defer></script>