                        <div class="z-overlay"></div>
                        <div class="z-background">
                            <div class="z-background-wrapper">
                                <div class="z-background-column"></div>
                                <div class="z-background-column"></div>
                                <div class="z-background-column"></div>
                                <div class="z-background-column"></div>
                                <div class="z-background-column"></div>
                            </div>
                        </div>
                        <div class="z-menu js-z-menu">
                            <div class="z-menu-wrapper js-z-menu-wrapper">
                                <svg class="z-menu-mask-group">
                                    <defs>
                                        <mask id="reveal-mask" width="2000" height="2000">
                                            <rect width="2000" height="2000" id="reveal-mask-inner" class="z-menu-mask-inner" fill="#FFF"></rect>
                                            <circle cx="100%" cy="50%"></circle>
                                        </mask>
                                    </defs>
                                    <rect id="reveal-mask-outer" width="2000" height="2000" mask="url(#reveal-mask)" fill="#A3D900"></rect>
                                </svg>
                                <nav class="z-menu-nav">
                                    <ul class="z-menu-list">
                                        <li class="{{ ($page == '' || $page == 'home') ? 'active' : '' }}">
                                            <a href="{{ url('/') }}">
                                                <span data-letters="Home">Home</span>
                                            </a>
                                        </li>
                                        <li class="{{ ($page == 'about') ? 'active' : '' }}">
                                            <a href="{{ url('/about') }}">
                                                <span data-letters="About">About</span>
                                            </a>
                                        </li>
                                        <li class="{{ ($page == 'work') ? 'active' : '' }}">
                                            <a href="{{ url('/works') }}">
                                                <span data-letters="Works">Works</span>
                                            </a>
                                        </li>
                                        <li class="{{ ($page == 'contact') ? 'active' : '' }}">
                                            <a href="{{ url('/contact') }}">
                                                <span data-letters="Contact">Contact</span>
                                            </a>
                                        </li>
                                        {{-- <li class="{{ ($page == 'blog') ? 'active' : '' }}">
                                            <a href="{{ url('/blog') }}">
                                                <span data-letters="Blog">Blog</span>
                                            </a>
                                        </li> --}}
                                    </ul>
                                    <div class="z-menu-footer">
                                        <div class="z-menu-footer-text">
                                            <p>&copy; {{ date('Y') }} All Rights Reserved.</p>
                                            <ul class="z-menu-footer-anchors">
                                                <li><a href="">Privacy Policy</a></li>
                                            </ul>
                                        </div>
                                        <ul class="z-menu-footer-assets">
                                            <li><a href="mailto:jobs@ozzigeno.com" class="mail"></a></li>
                                            <li><a href="https://www.facebook.com/ozzigenostudio" target="_blank" class="facebook"></a></li>
                                            <li><a href="https://www.instagram.com/ozzigenostudio" target="_blank" class="instagram"></a></li>            
                                        </ul>
                                    </div>
                                </nav>
                                <div class="z-menu-logo">
                                    <img src="{{asset('storage/laraassets/images/general/ozzigeno-logo.png')}}" alt="" srcset="">
                                </div>
                            </div>
                        </div>