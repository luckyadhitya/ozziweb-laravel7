    
    <div class="work-list" style="background-color: {{$bgColorWork}};">
        <section class="work-list-content">
            @foreach($brands as $brand)
                <div class="work-list-row">
                    <div class="work-list-wrapper">
                        <a href="{{url('/'.$brand['url'])}}" class="work-list-url {{($page == $brand['slug']) ? 'active' : ''}}">
                            <div class="brand">
                                <div class="brand-image">
                                    <img src="{{ $brand['cover'] }}" alt="{{ $brand['name'] }}" title="{{ $brand['name'] }}" srcset="">
                                </div>
                                <div class="brand-overlay"></div>
                                <div class="brand-name">
                                    <h1>{{ $brand['name'] }}</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </section>
    </div>