
                    <div class="z-sidepanel">
                        <div class="z-sidepanel-wrapper">
                        <a href="{{ url('/') }}" class="z-sidepanel-logo z-sidepanel-list-item">
                        <img src="{{asset('storage/laraassets/images/general/header-ozzigeno-logo.png')}}" alt="Ozzigeno Studio Logo">
                            </a>
                            <div class="z-button">
                                <div id="z-button-wrapper" class="z-button-wrapper">
                                    <svg width="1000px" height="1000px">
                                        <path id="pathA" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                                        <path id="pathB" d="M 300 500 L 700 500"></path>
                                        <path id="pathC" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
                                    </svg>
                                    <button id="z-button-trigger" class="z-button-trigger"></button>
                                </div>
                                <span class="z-button-text z-sidepanel-list-item"></span>
                            </div>
                            <div class="z-sidepanel-footer z-sidepanel-list-item">
                                <ul class="z-sidepanel-footer-assets">
                                    <li><a href="mailto:jobs@ozzigeno.com" class="mail"></a></li>
                                    <li><a href="https://www.facebook.com/ozzigenostudio" target="_blank" class="facebook"></a></li>
                                    <li><a href="https://www.instagram.com/ozzigenostudio" target="_blank" class="instagram"></a></li>
                                </ul>
                                <span class="z-sidepanel-footer-caption">Breathe in Indonesia</span>
                            </div>
                        </div>
                    </div>