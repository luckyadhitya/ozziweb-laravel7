<!DOCTYPE html>
<html>
    @include('layouts.head')

    <body class=" {{ ($haszopen) ? 'has-z-open' : false }} " data-aos-easing="ease" data-aos-duration="1000" data-aos-delay="0">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NXJRL8"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class="oz-wrapper">
            <div class="oz-container">
                <div class="z-wrapper">

                    @include('layouts.sidepanel')

                    <div class="z-container">

                        @include('layouts.menu')

                        @yield('section')

                    </div>
                </div>
            </div>
        </div>

    @include('layouts.footer')
    
    </body>
</html>