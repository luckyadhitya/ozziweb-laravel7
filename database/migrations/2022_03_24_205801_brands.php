<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Brands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('url')->nullable();
            $table->string('logo')->nullable();
            $table->string('cover')->nullable();
            $table->string('background')->nullable();
            $table->string('thumb')->nullable();
            $table->string('item')->nullable();
            $table->string('service')->nullable();
            $table->string('color')->nullable();
            $table->string('nav_color')->nullable();
            $table->integer('flag')->default(1);
            $table->integer('sort')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
