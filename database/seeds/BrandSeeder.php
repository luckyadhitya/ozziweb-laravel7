<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'name' => 'Safi',
                'url'  => 'works/safi-indonesia',
                'slug' => 'safi-indonesia',
                'logo' => '',
                'cover'=> 'safi-indonesia/cover.jpg',
                'background'=> 'safi-indonesia/background.jpg',
                'thumb'=> 'safi-indonesia/thumb.png',
                'item' => 'safi-indonesia/item.png',
                'service' => 'Annual Campaign',
                'color'=> '#F5F5F7',
                'nav_color' => '#333',
                'flag' => 1,
            ],
            [
                'name' => 'Ease',
                'url'  => 'works/ease',
                'slug' => 'ease',
                'logo' => '',
                'cover'=> 'ease/cover.jpg',
                'background'=> 'ease/background.jpg',
                'thumb'=> 'ease/thumb.png',
                'item' => 'ease/item.png',
                'service' => 'E-Commerce',
                'color'=> '#F8CACA',
                'nav_color' => '#333',
                'flag' => 1,
            ],
            [
                'name' => 'Bio Essence',
                'url'  => 'works/bio-essence',
                'slug' => 'bio-essence',
                'logo' => 'bio-essence/logo.png',
                'cover'=> 'bio-essence/cover.jpg',
                'background'=> 'bio-essence/background.jpg',
                'thumb'=> 'bio-essence/thumb.png',
                'item' => 'bio-essence/item.png',
                'service' => 'Digital Strategy',
                'color'=> '#F5F5F7',
                'nav_color' => '#333',
                'flag' => 1,
            ],
            [
                'name' => 'Milkuat',
                'url'  => 'works/milkuat',
                'slug' => 'milkuat',
                'logo' => 'milkuat/logo.png',
                'cover'=> 'milkuat/cover.jpg',
                'background'=> 'milkuat/background.jpg',
                'thumb'=> 'milkuat/thumb.png',
                'item' => 'milkuat/item.png',
                'service' => 'Digital Campaign',
                'color'=> '#FFFFFF',
                'nav_color' => '#333',
                'flag' => 1,
            ],
            [
                'name' => 'BAM!',
                'url'  => 'works/bam',
                'slug' => 'bam',
                'logo' => '',
                'cover'=> 'bam/cover.jpg',
                'background'=> 'bam/background.jpg',
                'thumb'=> 'bam/thumb.png',
                'item' => 'bam/item.png',
                'service' => 'Annual Campaign',
                'color'=> '#383636',
                'nav_color' => '#A3D900',
                'flag' => 1,
            ],
            [
                'name' => 'Vitalis',
                'url'  => 'works/vitalis',
                'slug' => 'vitalis',
                'logo' => 'vitalis/logo.png',
                'cover'=> 'vitalis/cover.jpg',
                'background'=> 'vitalis/background.jpg',
                'thumb'=> 'vitalis/thumb.png',
                'item' => 'vitalis/item.png',
                'service' => 'Annual Campaign',
                'color'=> '#383636',
                'nav_color' => '#A3D900',
                'flag' => 1,
            ],
            [
                'name' => 'Bahana',
                'url'  => null,
                'slug' => 'Bahana',
                'logo' => 'Bahana/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Bebelac',
                'url'  => null,
                'slug' => 'Bebelac',
                'logo' => 'Bebelac/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Danone',
                'url'  => null,
                'slug' => 'Danone',
                'logo' => 'Danone/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Delfi',
                'url'  => null,
                'slug' => 'Delfi',
                'logo' => 'Delfi/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Durex',
                'url'  => null,
                'slug' => 'Durex',
                'logo' => 'Durex/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Morin',
                'url'  => null,
                'slug' => 'Morin',
                'logo' => 'Morin/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Nutricia',
                'url'  => null,
                'slug' => 'Nutricia',
                'logo' => 'Nutricia/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'Sasa',
                'url'  => null,
                'slug' => 'Sasa',
                'logo' => 'Sasa/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
            [
                'name' => 'WWF',
                'url'  => null,
                'slug' => 'WWF',
                'logo' => 'WWF/logo.png',
                'cover'=> null,
                'background'=> null,
                'thumb'=> null,
                'item' => null,
                'service' => '-',
                'color'=> null,
                'nav_color' => null,
                'flag' => 1,
            ],
        ];

        $pages = [
            'safi-indonesia' => [
                [
                    'id_brand' => "",
                    'section_name' => 'banner',
                    'content' => '<section class="banner" style="background-image: url(/storage/laraassets/images/brands/safi-indonesia/asset/background/safi-banner.png);">
                    <article class="work-details-content banner-content">
                        <div class="work-details-desc banner-desc">
                            <div class="banner-logo">
                                <img src="/storage/laraassets/images/brands/safi-indonesia/asset/logo/safi-logo.png" alt="Safi Indonesia Logo" srcset="">
                            </div>
                            <div class="work-details-desc-wrap banner-desc-wrap">
                                <h1 style="color: #001A66;">Project Brief</h1>
                                <p style="color: #333333;">Create BRAND AWARENESS of Safi as new skincare brand made from finest natural ingredients, halal, and tested (proven efficacy) with EMOTIONAL ATTACHMENT that Safi understand their needs and enables them become a better Muslimah every day in clear communication to DIFFERENTIATE Safi from others.</p>
                                <h1 style="color: #001A66;">The Challenge</h1>
                                <p style="color: #333333;">There are already some of Big Halal Skincare brands in Indonesia, then how to differentiate Safi with other brands? How to build awareness and trust thru digital?</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'additional',
                    'content' => '<section class="additional">
                    <article class="work-details-content additional-content image-left" style="background-image: url(/storage/laraassets/images/brands/safi-indonesia/asset/background/hexagon-1.png);">
                        <div class="work-details-thumb start aos-animate aos-init">
                            <img src="/storage/laraassets/images/brands/safi-indonesia/asset/thumb/safi-research-institute.png" alt="Safi Research Institute" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <span class="additional-logo">
                                    <img src="/storage/laraassets/images/brands/safi-indonesia/asset/logo/safi-research-institute.png" alt="Safi Research Institute" srcset="">
                                </span>
                                <p style="color: #333333;">Safi is developed in Safi Research Institute, the World’s First Halal Research Facilities. Scientists in Safi Research Institute interact with Indonesian Muslim women to understand their needs of skincare products that are Halal, Natural, and Tested.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'site',
                    'content' => '<section class="site" style="background-color: #000040;">
                    <article class="work-details-content site-content" style="background-image: url(/storage/laraassets/images/brands/safi-indonesia/asset/background/hexagon-2.png">
                        <div class="site-info">
                            <h1 style="color: #FFFFFF;">Website</h1>
                            <p style="color: #FFFFFF;">One Stop Information Center for All Safi Enthusiasts. Designed to give costumers all the Information they need about Safi. User Friendly.</p>
                            <a class="site-url" href="https://safiindonesia.com" target="_blank" style="background-color: #1D1D4F;">Go to website</a>
                        </div>
                        <div class="site-preview">
                            <img src="/storage/laraassets/images/brands/safi-indonesia/asset/thumb/safi-site.png" alt="Safi Indonesia Official Site" srcset="">
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'app',
                    'content' => '<section class="app">
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb center">
                            <img src="/storage/laraassets/images/brands/safi-indonesia/asset/thumb/safi-social-media.png" alt="Safi Social Media" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #000040;">
                                    <span>Social</span>
                                    <span>Media</span>
                                </h1>
                                <p style="color: #000040;">The face of the brand: Bridging Safi and Costumers. Informative Content to build trust with customers while Highlighting the USP to differentiate Safi with the existing Brands.</p>
                            </div>
                        </div>
                    </article>
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb end">
                            <img src="/storage/laraassets/images/brands/safi-indonesia/asset/thumb/safi-application.gif" alt="Safi Skin Analyze Application" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h2 style="color: #000040;">
                                    <span>Skin</span>
                                    <span>Analyze</span>
                                    <span>Application</span>
                                </h2>
                                <p style="color: #000040;">Highly Technological Apps to help customers to understand their skin in just 1 Selfie. Giving information related to skin problems and how to overcome it.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'kol',
                    'content' => '<section class="kol" style="background-color: #000040;">
                    <article class="work-details-content kol-content image-right" style="background-image: url(storage/laraassets/images/brands/safi-indonesia/asset/background/hexagon-3.png);">
                        <div class="work-details-thumb end">
                            <img src="/storage/laraassets/images/brands/safi-indonesia/asset/thumb/safi-kol.png" alt="Safi KOL" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #FFFFFF;">KOL</h1>
                                <p style="color: #FFFFFF;">The power of Words of Mouth. Focusing on the Efficacy of the products, collaborating with KOL in raising awareness about Safi Products.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'result',
                    'content' => '<section class="result">
                    <article class="work-details-content result-content image-left" style="background-image: url(storage/laraassets/images/brands/safi-indonesia/asset/background/safi-result.jpg);">
                        <div class="work-details-thumb result-thumb center">
                            <img src="/storage/laraassets/images/brands/safi-indonesia/asset/thumb/safi-social-media.png" alt="Safi Result" srcset="">
                        </div>
                        <div class="work-details-desc result-desc">
                            <div class="work-details-desc-wrap result-desc-wrap">
                                <h1 style="color: #FFFFFF">Result</h1>
                                <p style="color: #FFFFFF;">Successfully enter Indonesian Market in a year. Being one of the noisiest brands that are launched in 2018.</p>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h1 style="color: #26FFFF;">75K</h1>
                                                <p style="color: #AAAAAA;">/ 7 Month<br>Followers Instagram</p>
                                                <hr>
                                            </td>
                                            <td>
                                                <h1 style="color: #26FFFF;">40K</h1>
                                                <p style="color: #AAAAAA;">Organic Traffic Monthly</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h1 style="color: #26FFFF;">1.16M</h1>
                                                <p style="color: #AAAAAA;">/ 10 Month<br>Organic Impression</p>
                                            </td>
                                            <td>
                                                <h1 style="color: #26FFFF;">41%</h1>
                                                <p style="color: #AAAAAA;">Awareness<br>The penetration of the awareness of Safi in Indonesia</p>
                                                <hr>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h1 style="color: #26FFFF;">0.4%</h1>  
                                                <p style="color: #AAAAAA;">Difference with main competitor in terms of Sales in GT<br><br>Market Share</p>          
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
            ],
            'ease' => [
                [
                    'id_brand' => "",
                    'section_name' => 'banner',
                    'content' => '<section class="banner" style="background-image: url(/storage/laraassets/images/brands/ease/asset/background/ease-banner.png);">
                    <article class="work-details-content banner-content">
                        <div class="work-details-desc banner-desc">
                            <div class="banner-logo">
                                <img src="/storage/laraassets/images/brands/ease/asset/logo/ease-logo.png" alt="Ease Brand Logo" srcset="">
                            </div>
                            <div class="work-details-desc-wrap banner-desc-wrap">
                                <h1 style="color: #243638;">Project Brief</h1>
                                <p style="color: #243638;">An Ecommerce web that provides a pleasant shopping experience which is designed in a modern manner with a touch of soft colours that spoil the eyes of the consumers.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'site',
                    'content' => '<section class="site">
                    <article class="work-details-content site-content">
                        <div class="site-info">
                            <h1 style="color: #243638;">Website</h1>
                            <p style="color: #333333;">An Ecommerce web that provides a pleasant shopping experience which is designed in a modern manner with a touch of soft colours that spoil the eyes of the consumers.</p>
                            <a class="site-url" href="https://easebrand.com" target="_blank" style="background-color: #243638;">Go to website</a>
                        </div>
                        <div class="site-preview">
                            <img src="/storage/laraassets/images/brands/ease/asset/thumb/ease-site.png" alt="Ease Brand Official Site" srcset="">
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'app',
                    'content' => '<section class="app">
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb center">
                            <img src="/storage/laraassets/images/brands/ease/asset/thumb/ease-bag.png" alt="Ease Brand add to Bag" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #243638;">
                                    <span>To Bag</span>
                                </h1>
                                <p style="color: #333333;">Using a simple layout filled with all the information needed, that makes it easy for consumers to purchase.</p>
                            </div>
                        </div>
                    </article>
                    <article class="work-details-content app-features image-right" style="background-image: url(/storage/laraassets/images/brands/ease/asset/background/ease-cart.jpg);">
                        <div class="work-details-thumb end">
                            <img src="/storage/laraassets/images/brands/ease/asset/thumb/ease-cart.png" alt="Ease Brand add to Cart" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h2 style="color: #243638;">
                                    <span>To Cart</span>
                                </h2>
                                <p style="color: #333333;">The items that you save or want to buy are stored in a shopping basket that is easy to process.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'result',
                    'content' => '<section class="result">
                    <article class="work-details-content result-content image-right">
                        <div class="work-details-thumb result-thumb end">
                            <img src="/storage/laraassets/images/brands/ease/asset/thumb/ease-result.png" alt="Ease Brand Result" srcset="">
                        </div>
                        <div class="work-details-desc result-desc">
                            <div class="work-details-desc-wrap result-desc-wrap">
                                <h1 style="color: #243638;">Result</h1>
                                <p style="color: #333333;">Website adaptation to mobile devices is an essential and inseparable part of a modern website.</p>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h1 style="color: #333333;">2.514</h1>
                                                <p style="color: #5AC4BF;">User</p>
                                                <hr>
                                            </td>
                                            <td>
                                                <h1 style="color: #333333;">5.12</h1>
                                                <p style="color: #5AC4BF;">Page/Session</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h1 style="color: #333333;">21.641</h1>
                                                <p style="color: #5AC4BF;">Page view</p>
                                            </td>
                                            <td>
                                                <h1 style="color: #333333;">38,75%</h1>
                                                <p style="color: #5AC4BF;">Bounce Rate</p>
                                                <hr>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
            ],
            'bio-essence' => [
                [
                    'id_brand' => "",
                    'section_name' => 'banner',
                    'content' => '<section class="banner" style="background-image: url(/storage/laraassets/images/brands/bio-essence/asset/background/bioessence-banner.png);">
                    <article class="work-details-content banner-content">
                        <div class="work-details-desc banner-desc">
                            <div class="banner-logo">
                                <img src="/storage/laraassets/images/brands/bio-essence/asset/logo/bioessence-logo.png" alt="Bioessence Logo" srcset="">
                            </div>
                            <div class="work-details-desc-wrap banner-desc-wrap">
                                <h1 style="color: #3693CF;">Project Brief</h1>
                                <p style="color: #243538;">Create brand awareness of Bio-essence in Indonesia and drive it to sales.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'site',
                    'content' => '<section class="site">
                    <article class="work-details-content site-content">
                        <div class="site-info">
                            <h1 style="color: #2188CA;">Website</h1>
                            <p style="color: #444444;">Bio-essence designed as a responsive website which the customers can browse it anywhere from the desktop to the mobile phone. Not only limited to that, Bio-essence designed as a user friendly website to give a full information for customers.</p>
                            <a class="site-url" href="https://bioessence.id" target="_blank" style="background-color: #2188CA;">Go to website</a>
                        </div>
                        <div class="site-preview">
                            <img src="/storage/laraassets/images/brands/bio-essence/asset/thumb/bioessence-site.png" alt="Bioessence Official Site" srcset="">
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'app',
                    'content' => '<section class="app">
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb center">
                            <img src="/storage/laraassets/images/brands/bio-essence/asset/thumb/product.png" alt="Bioessence Product" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h3 style="color: #1F86CA;">
                                    <span>Showing Off</span>
                                    <span>Product Detail</span>
                                </h3>
                                <p style="color: #444444;">As a skincare who understand the customers needs, here Bio-essence give a product detail and benefit to keep the customers understanding about the product.</p>
                            </div>
                        </div>
                    </article>
                    <article class="work-details-content app-features image-right" style="background-color: #CDE1EF;">
                        <div class="work-details-thumb end">
                            <img src="/storage/laraassets/images/brands/bio-essence/asset/thumb/store-locator.png" alt="Bioessence Store Locator" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #1F86CA;">
                                    <span>Store</span>
                                    <span>Locator</span>
                                </h1>
                                <p style="color: #444444;">There&#39;s still a lot of people out there who have no idea where to buy Bio-essence. This is where the customers can find an information of where is the nearest store to buy Bio-essence.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'kol',
                    'content' => '<section class="kol">
                    <article class="work-details-content kol-content image-right" style="background-image: url(/storage/laraassets/images/brands/bio-essence/asset/background/bioessence-kol.png);">
                        <div class="work-details-thumb center">
                            <img src="/storage/laraassets/images/brands/bio-essence/asset/thumb/bioessence-kol.png" alt="Bioessence KOL" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #2B8BCA;">KOL</h1>
                                <p style="color: #333333;">Use BIG KOL influencer of skin care, the product was sold out in everywhere.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'result',
                    'content' => '<section class="result">
                    <article class="work-details-content result-content image-right">
                        <div class="work-details-thumb result-thumb end">
                            <img src="/storage/laraassets/images/brands/bio-essence/asset/thumb/campaign-result.png" alt="Bioessence Campaign Result" srcset="">
                        </div>
                        <div class="work-details-desc result-desc">
                            <div class="work-details-desc-wrap result-desc-wrap">
                                <h1 style="color: #2B8BCA">
                                    <span>Campaign</span>
                                    <span>&amp; Result</span>
                                </h1>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h1 style="color: #444444;">360</h1>
                                                <p style="color: #444444;">Intergrated digital for Bio-essence Royal Jelly +ATP campaign</p>
                                                <hr>
                                            </td>
                                            <td>
                                                <h1 style="color: #444444;">601</h1>
                                                <p style="color: #444444;">Actual Offline Registrants from 100 targeted Registrants in Microsite</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h1 style="color: #444444;">449</h1>
                                                <p style="color: #444444;">Online Submissions Videos</p>
                                            </td>
                                            <td>
                                                <h1 style="color: #444444;">50+</h1>
                                                <p style="color: #444444;">KOL helps to review trial kit products of Bio-essence Royal Jelly+ATP</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'infographic',
                    'content' => '<section class="infographic" style="background-color: #FFFFFF;">
                    <article class="work-details-content infographic-content" style="background-image: url(/storage/laraassets/images/brands/bio-essence/asset/background/result-infographic.png);">
                        <div class="work-details-thumb infographic-thumb">
                            <img src="/storage/laraassets/images/brands/bio-essence/asset/thumb/infographic.png" alt="Bioessence Campaign Infographic" srcset="">
                        </div>
                    </article>
                </section>'
                ],
            ],
            'milkuat' => [
                [
                    'id_brand' => "",
                    'section_name' => 'banner',
                    'content' => '<section class="banner" style="background-image: url(/storage/laraassets/images/brands/milkuat/asset/background/milkuat-banner.jpg);">
                    <article class="work-details-content banner-content">
                        <div class="work-details-desc banner-desc">
                            <div class="banner-logo">
                                <img src="/storage/laraassets/images/brands/milkuat/asset/logo/milkuat-logo.png" alt="Milkuat Logo" srcset="">
                            </div>
                            <div class="work-details-desc-wrap banner-desc-wrap">
                                <h1 style="color: #444444;">Project Brief</h1>
                                <ul style="color: #AAAAAA;">
                                    <li>Educate Moms to providing good 	nutrition & great taste to kids through right channel, right content & right time</li>
                                    <li>Encourage Kids to engage with Milkuat brand through experiences (games, video, etc) that drives incremental brand choice before purchase.</li>
                                </ul>
                                <h1 style="color: #444444;">Challenge</h1>
                                <p style="color: #AAAAAA;">How to Milkuat become a product choice and known as a milk that have nutrition</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'site',
                    'content' => '<section class="site" style="background-color: #ED1B25;">
                    <article class="work-details-content site-content" style="background-image: url(/storage/laraassets/images/brands/milkuat/asset/background/milkuat-site.png);">
                        <div class="site-info">
                            <h1 style="color: #FFFFFF;">Website</h1>
                            <p style="color: #FFFFFF;">All information about product of Milkuat and parenting tips article and user friendly.</p>
                            <a class="site-url" href="https://milkuat.co.id" target="_blank" style="background-color: #FFC311;">Go to website</a>
                        </div>
                        <div class="site-preview">
                            <img src="/storage/laraassets/images/brands/milkuat/asset/thumb/milkuat-site.png" alt="Milkuat Official Site" srcset="">
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'app',
                    'content' => '<section class="app" style="background-image: url(/storage/laraassets/images/brands/milkuat/asset/background/milkuat-app.png);">
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb end">
                            <img src="/storage/laraassets/images/brands/milkuat/asset/thumb/milkuat-app.png" alt="What We Do with Milkuat" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #444444;">
                                    <span>What</span>
                                    <span>We Do</span>
                                </h1>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src="/storage/laraassets/images/brands/milkuat/asset/thumb/milkuat-do-1.png" alt="Article in website">
                                                <p style="color: #AAAAAA;">Article in website for Moms</p>
                                            </td>
                                            <td>
                                                <img src="/storage/laraassets/images/brands/milkuat/asset/thumb/milkuat-do-2.png" alt="Article in website">
                                                <p style="color: #AAAAAA;">Games for kids</p>
                                            </td>
                                            <td>
                                                <img src="/storage/laraassets/images/brands/milkuat/asset/thumb/milkuat-do-3.png" alt="Article in website">
                                                <p style="color: #AAAAAA;">Mini campaign for kids</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </article>
                    <article class="work-details-content app-features image-right" style="background-image: url(/storage/laraassets/images/brands/milkuat/asset/background/milkuat-app-earth.png);">
                        <div class="work-details-thumb end">
                            <img src="/storage/laraassets/images/brands/milkuat/asset/thumb/milkuat-campaign.png" alt="Milkuat Boboiboy Campaign" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h3 style="color: #444444;">
                                    <span>In 2017 Collaborate</span>
                                    <span>With Boboiboy</span>
                                </h3>
                                <ol style="color: #AAAAAA;">
                                    <li>Learning from Milkuat BBB Promo in semester 2, 2017, the promo was drives  the incrasing sales of Milkuat Bottle by >45%</li>
                                    <li>On the brand imagery, the Milkuat BBB communication drives imagery on “makes me strong and gives me energy”</li>
                                </ol>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'result',
                    'content' => '<section class="result">
                    <article class="work-details-content result-content-noImage" style="background-image: url(/storage/laraassets/images/brands/milkuat/asset/background/milkuat-result.jpg);">
                        <div class="work-details-desc result-desc-noImage">
                            <div class="work-details-desc-wrap result-desc-noImage-wrap">
                                <h1 style="color: #444444;">Result</h1>
                                <div class="result-desc-noImage-list">
                                    <div class="data">
                                        <div class="data-counter">
                                            <div class="data-counter-wrapper">
                                                <span style="color: #ED1B25;">2.895</span>
                                            </div>
                                        </div>
                                        <div class="data-desc">
                                            <p>Total user participant in 3 months of games called Milkupolis</p>
                                        </div>
                                    </div>
                                    <div class="data">
                                        <div class="data-counter">
                                            <div class="data-counter-wrapper">
                                                <span style="color: #ED1B25;">1.026.980</span>
                                            </div>
                                        </div>
                                        <div class="data-desc">
                                            <p>Total fans in facebook since 2012</p>
                                        </div>
                                    </div>
                                    <div class="data">
                                        <div class="data-counter">
                                            <div class="data-counter-wrapper">
                                                <span style="color: #ED1B25;">3.817</span>
                                            </div>
                                        </div>
                                        <div class="data-desc">
                                            <p>Total fans in Instagram (without ads at Instagram) since 2017</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
            ],
            'bam' => [
                [
                    'id_brand' => "",
                    'section_name' => 'banner',
                    'content' => '<section class="banner" style="background-image: url(/storage/laraassets/images/brands/bam/asset/background/bam-banner.png); background-color: #383636;">
                    <article class="work-details-content banner-content">
                        <div class="work-details-desc banner-desc">
                            <div class="banner-logo">
                                <img src="/storage/laraassets/images/brands/bam/asset/logo/bam-logo.png" alt="BAM Logo" srcset="">
                            </div>
                            <div class="work-details-desc-wrap banner-desc-wrap">
                                <h1 style="color: #FFFFFF;">Project Brief</h1>
                                <p style="color: #FFFFFF;">Create brand awareness of BAM. A Restaurant with Spanish food in Indonesia.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'site',
                    'content' => '<section class="site" style="background-color: #383636;">
                    <article class="work-details-content site-content" style="background-image: url(/storage/laraassets/images/brands/bam/asset/background/bam-gallery.png);">
                        <div class="site-info">
                            <h1 style="color: #FFFFFF;">
                                <span>Food</span>
                                <span>Photography</span>
                            </h1>
                            <p style="color: #FFFFFF;">Maintaince for Social media of BAM and 48 Dimsum.<br>Food photography in every post combine with digital imaging.</p>
                        </div>
                        <div class="site-preview">
                            <img src="/storage/laraassets/images/brands/bam/asset/thumb/bam-instagram.png" alt="BAM Instagram Official" srcset="">
                        </div>
                    </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'app',
                    'content' => '<section class="app">
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb center">
                            <img src="/storage/laraassets/images/brands/bam/asset/thumb/bam-project-brief.png" alt="BAM Project Brief" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #000000;">
                                    <span>Project Brief</span>
                                </h1>
                                <p style="color: #000000;">Create brand awareness of 48 Dimsum a Halal Dimsum in Indonesia.</p>
                            </div>
                        </div>
                    </article>
                </section>'
                ],
            ],
            'vitalis' => [
                [
                'id_brand' => "",
                'section_name' => 'banner',
                'content' => '<section class="banner" style="background-image: url(/storage/laraassets/images/brands/vitalis/asset/background/vitalis-banner.png);">
                <article class="work-details-content banner-content">
                    <div class="work-details-desc banner-desc">
                        <div class="banner-logo">
                            <img src="/storage/laraassets/images/brands/vitalis/asset/logo/vitalis-logo.png" alt="Vitalis Logo" srcset="">
                        </div>
                        <div class="work-details-desc-wrap banner-desc-wrap">
                            <h1 style="color: #333333;">Project Brief</h1>
                            <p style="color: #444444;">Create BRAND AWARENESS and STRENGTHENING message Vitalis as market leader in female fragrance. Vitalis want to attract audience to reach their dream and Vitalis will help them to feel more confident.</p>
                            <h1 style="color: #333333;">The Challenge</h1>
                            <p style="color: #444444;">There are already some of fragrance brands in Indonesia, then how to differentiate Vitalis with other brands? How to build awareness and trust thru digital?</p>
                        </div>
                    </div>
                </article>
                </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'site',
                    'content' => '<section class="site" style="background-color: #EEEEEE;">
                    <article class="work-details-content site-content">
                        <div class="site-info">
                            <h1 style="color: #333333;">Website</h1>
                            <p style="color: #444444;">To give information about brand and product knowledge.</p>
                            <a class="site-url" href="https://pesonavitalis.com" target="_blank" style="background-color: #666666;">Go to Website</a>
                        </div>
                        <div class="site-preview">
                            <img src="/storage/laraassets/images/brands/vitalis/asset/thumb/vitalis-site.png" alt="Vitalis Official Site" srcset="">
                        </div>
                    </article>
                    </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'app',
                    'content' => '<section class="app" style="background-color: #EEEEEE;">
                    <article class="work-details-content app-features image-right">
                        <div class="work-details-thumb center">
                            <img src="/storage/laraassets/images/brands/vitalis/asset/thumb/vitalis-social-media.png" alt="Vitalis Social Media" srcset="">
                        </div>
                        <div class="work-details-desc">
                            <div class="work-details-desc-wrap">
                                <h1 style="color: #333333;">
                                    <span>Social</span>
                                    <span>Media</span>
                                </h1>
                                <p style="color: #444444;">As a place for customers to find information updated from Vitalis and also as a place to invite customers to interact with brands.</p>
                            </div>
                        </div>
                    </article>
                    </section>'
                ],
                [
                    'id_brand' => "",
                    'section_name' => 'result',
                    'content' => '<section class="result" style="background-color: #EEEEEE;">
                    <div class="result-header">
                        <h1 style="color: #333333;">Vitalis Empression</h1>
                        <p style="color: #444444;">Awareness and Virality about campaign #VitalisEmpression</p>
                    </div>
                    <article class="work-details-content result-content image-left">
                        <div class="work-details-thumb result-thumb center">
                            <img src="/storage/laraassets/images/brands/vitalis/asset/thumb/vitalis-empression.png" alt="Vitalis Empression Campaign Result">
                        </div>
                        <div class="work-details-desc result-desc">
                            <div class="work-details-desc-wrap result-desc-wrap">
                                <h1 style="color: #333333;">Result</h1>
                                <p style="color: #444444;">Successfully enter Indonesian Market in a year. Being one of the noisiest brands that are launched in 2018.</p>
                                <table width="100%" cellpadding="" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <h1 style="color: #333333;">87.934.805</h1>
                                                <p style="color: #444444;">Impression</p>
                                                <hr>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <h1 style="color: #333333;">877.449</h1>
                                                <p style="color: #444444;">Engagement</p>
                                                <hr>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h1 style="color: #333333;">1%</h1>
                                                <p style="color: #444444;">Engagement Rate</p>
                                            </td>
                                            <td>
                                                <h1 style="color: #333333;">610</h1>
                                                <p style="color: #444444;">Submissions</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </article>
                    <article class="work-details-content result-content image-right">
                        <div class="work-details-thumb result-thumb end">
                            <img src="/storage/laraassets/images/brands/vitalis/asset/thumb/vitalis-my-exotic-vacation.png" alt="Vitalis My Exotic Vacation Campaign Result" srcset="">
                        </div>
                        <div class="work-details-desc result-desc">
                            <div class="work-details-desc-wrap result-desc-wrap">
                                <h1 style="color: #333333;">My Exotic Vacation</h1>
                                <p style="color: #444444;">To increase New Vitalis EBS Awareness which start from traveler blogger and to link our EBS wit a vacation. At the end we want to get virality from bloggers.</p>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h1 style="color: #545454;">343</h1>
                                                <p style="color: #868686;">Total Post</p>
                                                <hr>
                                            </td>
                                            <td>
                                                <h1 style="color: #545454;">2217</h1>
                                                <p style="color: #868686;">Total User</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h1 style="color: #545454;">7411</h1>
                                                <p style="color: #868686;">Total Share</p>
                                            </td>
                                            <td>
                                                <h1 style="color: #545454;">1765</h1>
                                                <p style="color: #868686;">Total Likes</p>
                                                <hr>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </article>
                    </section>'
                ]
            ]
        ];

        $sort = 1;
        foreach ($datas as $data) {
            $data['sort'] = $sort++;
            $id_brand = DB::table('brands')->insertGetId($data);

            if(isset($pages[$data['slug']]))
            {
                foreach ($pages[$data['slug']] as $page) {
                    $page['id_brand'] = $id_brand;
    
                    DB::table('brand_pages')->insert($page);
                }
            }
        }
    }
}
