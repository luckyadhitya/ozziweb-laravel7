<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'photo'=>'Kriston.jpg',
                'fullName'=>'Kriston',
                'callName'=>'Kriston',
                'title'=>'Chief Elaborate Officer',
            ],
            [
                'photo'=>'Ayu.jpg',
                'fullName'=>'Ayu',
                'callName'=>'Ayu',
                'title'=>'Chief Inspiration Officer',
            ],
            [
                'photo'=>'Sylvia.jpg',
                'fullName'=>'Sylvia',
                'callName'=>'Sylvia',
                'title'=>'PRINCESS (Profesional Relation of Client Happiness)',
            ],
            [
                'photo'=>'Chika.jpg',
                'fullName'=>'Chika',
                'callName'=>'Chika',
                'title'=>'Client-Heart Holder',
            ],
            [
                'photo'=>'Dina.jpg',
                'fullName'=>'Dina',
                'callName'=>'Dina',
                'title'=>'AXE (Account Xuper Executive)',
            ],
            [
                'photo'=>'Billy.jpg',
                'fullName'=>'Billy',
                'callName'=>'Billy',
                'title'=>'(M)Ad Men',
            ],
            [
                'photo'=>'Yori.jpg',
                'fullName'=>'Yori',
                'callName'=>'Yori',
                'title'=>'Design Artmirer',
            ],
            [
                'photo'=>'fangga.jpg',
                'fullName'=>'Fangga',
                'callName'=>'Fangga',
                'title'=>'Knight of Design Norms',
            ],
            [
                'photo'=>'Esterina.jpg',
                'fullName'=>'Ester',
                'callName'=>'Ester',
                'title'=>'Bachelor of Visual Magician',
            ],
            [
                'photo'=>'Giovanni.jpg',
                'fullName'=>'Gio',
                'callName'=>'Gio',
                'title'=>'Secret Visual Agent',
            ],
            [
                'photo'=>'Febri.jpg',
                'fullName'=>'Febri',
                'callName'=>'Febri',
                'title'=>'Illustrious Visual Alchemist',
            ],
            [
                'photo'=>'Fina.jpg',
                'fullName'=>'Fina',
                'callName'=>'Fina',
                'title'=>'Brand Internetologist',
            ],
            [
                'photo'=>'Tisha.jpg',
                'fullName'=>'Tisha',
                'callName'=>'Tisha',
                'title'=>'Content Creator Warrior',
            ],
            [
                'photo'=>'Candhy.jpg',
                'fullName'=>'Candhy',
                'callName'=>'Candhy',
                'title'=>'Furiosa (Further Ozzigeno Socmed Assistant)',
            ],
            [
                'photo'=>'Faqih.jpg',
                'fullName'=>'Faqih',
                'callName'=>'Faqih',
                'title'=>'Master of Internet Language & Forecasting',
            ],
            [
                'photo'=>'Agung.jpg',
                'fullName'=>'Agung',
                'callName'=>'Agung',
                'title'=>'Functional & Usable Code Keeper',
            ],
            [
                'photo'=>'Wahyu.jpg',
                'fullName'=>'Wahyu',
                'callName'=>'Wahyu',
                'title'=>'Amazing WYSIWYG Maker',
            ],
            [
                'photo'=>'Eko.jpg',
                'fullName'=>'Eko',
                'callName'=>'Eko',
                'title'=>'Virtuous Code Artisan',
            ]
        ];
        foreach ($datas as $data) {
            DB::table('teams')->insert($data);
        }
    }
}
