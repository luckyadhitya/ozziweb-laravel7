<?php
namespace App\Helper;

class ImageFactory
{
    /** Atribute */
    private static $path = '';
    private static $images = [];

    public static function prepare($path, $request)
    {
        self::$path = $path;
        $requestKey = $request->except('_token', '_method');

        foreach ($requestKey as $key => $value) {
            
            if (preg_match('/new/', $key))
            {
                $arrKey = preg_split('/new/', $key)[1];
                $fieldKey = strtolower($arrKey);

                
                if( isset($value) )
                {
                    // insert request to attributes
                    self::$images[] = [
                        'request' => $request['new'.$arrKey],
                        'image' => $fieldKey . '.' . $request['new'.$arrKey]->extension()
                    ];
                } else {
                    echo "NULL condition is not available";
                    return false;
                }
            }

        }

        return new self;
    }

    public function reupload()
    {
        $path = self::$path;

    }

    public function remove($path = null)
    {
        $existPath = $path ? $path : self::$path;
        if ( file_exist($existPath) )
        {
            chmod($existPath, 0777);

            if( unlink($existPath) )
            {
                if( is_null($path) )
                {
                    return new self;
                } else {
                    return true;
                }
            }

        }

    }

    public function upload()
    {
        $dataImage = [];
        $path = self::$path;
        $images = self::$images;

        foreach ($images as $image) {
            chmod($path, 0777);
            if($image['request']->move( $path, $image['image']))
            {
                $fieldKey = explode('.', $image['image']);
                $dataImage[] = $image['image'];
            }
        }

        return $dataImage;

    }
    
}
