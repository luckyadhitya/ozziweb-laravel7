<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\BrandPages;
use Illuminate\Support\Facades\DB;

class SectionController extends Controller
{
    // private $slug;

    // private $id;

    // public function __construct($slug, $id)
    // {
    //     $this->slug = $slug;
    //     $this->id = $id;
    // }

    public function section($slug, $id)
    {
        $brand = Brand::find($id);
        // $pages = Brand::join('brand_pages', 'id')
        $sections = DB::table('brands')
                    ->join('brand_pages', 'brands.id', 'brand_pages.id_brand')
                    ->where('brands.id', $id)
                    ->get();
        
        $data = [
            'client' => $brand,
            'sections' => $sections
        ];
        
        return view('admin.pages.client-detail', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'id_brand' => 'required',
            'section_name' => 'required'
        ]);

        $brand = new BrandPages;
        $brand->id_brand = $request->id_brand;
        $brand->section_name = $request->section_name;
        $brand->content = null;
        $brand->images = null;

        $brand->save();

        return redirect( route('section.index', ['slug' => 'client', 'id' => $request->id_brand]) )->with('message', ["status"=> true, "message"=> "New section created!"]);
    }

    public function update(Request $request, $slug, $id)
    {
        $section = BrandPages::find($id);

        $section->content = $request->content;
        $section->save();

        return redirect( route('section.index', ['slug' => 'client', 'id' => $section->id_brand]) )->with('message', ["status"=> true, "message"=> "Section updated!"]);
    }

    public function destroy($slug, $id)
    {
        $section = BrandPages::find($id);
        $section->delete();

        return redirect( route('section.index', ['slug' => 'client', 'id' => $section->id_brand]) )->with('message', ["status"=> true, "message"=> "Section has deleted!"]);
    }
}
