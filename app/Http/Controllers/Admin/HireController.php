<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HireController extends Controller
{
    public function index()
    {
        $data = Job::all();
        return view('admin.pages.hire', compact('data'));
    }

    public function store(Request $request)
    {
        $job = new Job;

        $validate = $request->validate([
            'position' => 'required',
            'requirement' => 'required',
            'responsibilities' => 'required',
        ]);

        //slug
        $slug = Str::slug($request->position, '-');

        $job->position = $request->position;
        $job->slug = $slug;
        $job->requirement = $request->requirement;
        $job->responsibilities = $request->responsibilities;
        $job->status = 1; // Auto active after first create

        $job->save();

        return redirect( route('hire.index') )->with('message', ["status"=> true, "message"=> "Jobs hiring opened!"]);
    }

    public function update(Request $request, $id)
    {
        $job = Job::find($id);

        /** Validation */
        $validate = $request->validate([
            'position' => 'required',
            'requirement' => 'required',
            'responsibilities' => 'required',
            'status' => 'required',
        ]);

        $job->position = $request->position;
        $job->requirement = $request->requirement;
        $job->responsibilities = $request->responsibilities;
        $job->status = $request->status; // Auto active after first create

        $job->save();

        return redirect( route('hire.index') )->with('message', ["status"=> true, "message"=> "Job hire updated!"]);
    }

    public function destroy($id)
    {
        $status = null;
        $job = Job::find($id);

        // team deleted
        if( $job->delete() )
        {
            $status = ["status"=> true, "message"=> "Job hire deleted!"];
        } else {
            $status = ["status"=> false, "message"=> "Job hire not found!"];
        }
        
        return redirect( route('hire.index') )->with('message', $status);
    }
}
