<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

// use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    public function index() 
    {
        $data = Team::all();
        return view('admin.pages.team', compact('data'));
    }

    public function create(Request $request)
    {
        /** Validation */
        $validate = $request->validate([
            'fullName' => 'required',
            'callName' => 'required',
            'title' => 'required',
            'newPhoto' => 'required|mimes:jpeg,jpg,png'
        ]);
        $imageName = time() . '-' . $request->callName . '.' . $request->newPhoto->extension();

        $team = new Team;
        
        if($request->newPhoto->move( public_path('storage/laraassets/images/team'), $imageName))
        {
            $team->fullName = $request->fullName;
            $team->callName = $request->callName;
            $team->title = $request->title;
            $team->photo = $imageName;

            $team->save();
        }

        return redirect( route('team.index') )->with('message', ["status"=> true, "message"=> "Data created!"]);
    }

    public function update(Request $request, $id) {

        $team = Team::find($id);

        /** Validation */
        $validate = $request->validate([
            'fullName' => 'required',
            'callName' => 'required',
            'title' => 'required',
            'oldPhoto' => 'required'
        ]);

        if ( isset($request->newPhoto) )
        {
            $path = public_path('storage/laraassets/images/team/' . $request->oldPhoto);

            // check file exist
            if( file_exists($path) )
            {
                // delete file
                if( unlink($path) )
                {
                    $imageName = time() . '-' . $request->callName . '.' . $request->newPhoto->extension();            
                    if($request->newPhoto->move( public_path('storage/laraassets/images/team'), $imageName))
                    {
                        $team->photo = $imageName;
                    }
                }
            }
        }

        $team->fullName = $request->fullName;
        $team->callName = $request->callName;
        $team->title = $request->title;

        $team->save();

        return redirect( route('team.index') )->with('message', ["status"=> true, "message"=> "Data user updated!"]);
    }

    public function destroy($id)
    {
        $status = null;
        $team = Team::find($id);

        // check photo exist
        $path = public_path('storage/laraassets/images/team/' . $team->photo);
        if( File::delete($path) )
        {
            $status = ["status"=> false, "message"=> "Fail to delete!"];
        }

        // team deleted
        if( $team->delete() )
        {
            $status = ["status"=> true, "message"=> "User deleted!"];
        } else {
            $status = ["status"=> false, "message"=> "User not found!"];
        }
        
        return redirect( route('team.index') )->with('message', $status);
    }

    public function sortTable(Request $request)
    {

        foreach ($request->data as $data) {
            $team = Team::find($data['id']);
            $team->sort = $data['sort'];

            $team->save();
        }

    }
}
