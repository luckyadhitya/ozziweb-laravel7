<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\BrandPages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ClientController extends Controller
{
    public function index() 
    {
        $data = Brand::orderBy('sort', 'asc')->get();
        return view('admin.pages.client', compact('data'));
    }

    public function store(Request $request)
    {
        $brand = new Brand;
        $lastSort = Brand::max('sort');

        $validate = $request->validate([
            'brandName' => 'required',
        ]);

        //slug
        $slug = Str::slug($request->brandName, '-');

        $brand->name = $request->brandName;
        $brand->slug = $slug;
        $brand->service = $request->brandService;
        $brand->sort = $lastSort+1;

        $brand->save();

        return redirect( route('client.index') )->with('message', ["status"=> true, "message"=> "Brand created!"]);
    }

    public function destroy($id)
    {
        $brand = Brand::find($id);

        $path = public_path('storage/laraassets/images/brands/' . $brand->slug);
        if( is_dir($path) )
        {
            exec ("find $path -type d -exec chmod 0770 {} +");//for sub directory
            exec ("find $path -type f -exec chmod 0644 {} +");//for files inside directory

            $files = glob($path.'/*'); 
   
            // Deleting all the files in the list
            foreach($files as $file) {
                if(is_file($file))
                    unlink($file); // Delete the given file
            }
            if(rmdir($path))
            {
                $brand->delete();
            }
        } else {
            $brand->delete();
        }


        return redirect( route('client.index') )->with('message', ["status"=> true, "message"=> "Brand deleted!"]);
    }

    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        $dataImage = $this->imageUploader($request, $brand->slug);

        $brand->name = $request->brandName;
        // $brand->slug = $request->brandSlug;
        $brand->logo = $dataImage['logo'];
        $brand->cover = $dataImage['cover'];
        $brand->background = $dataImage['background'];
        $brand->thumb = $dataImage['thumb'];
        $brand->item = $dataImage['item'];

        $brand->save();

        return redirect( route('section.index', ['slug' => 'client', 'id' => $brand->id]) )->with('message', ["status"=> true, "message"=> "Brand modified!"]);
    }

    public function imageUploader($request, $slug, $action = 'create')
    {
        /**
         * check apa ada old photo
         * check jika ada request new photo
         * 
         */
        $path = public_path("storage/laraassets/images/brands/$slug/");
        $requestKey = $request->except('_token', '_method');

        $dataImage = [
            'logo' => $request->oldLogo,
            'cover' => $request->oldCover,
            'background' => $request->oldBackground,
            'thumb' => $request->oldThumb,
            'item' => $request->oldItem
        ];

        foreach ($request->except('_token', '_method') as $key => $value) {
            
            if (preg_match('/new/', $key))
            {
                $arrKey = preg_split('/new/', $key)[1];
                $fieldKey = strtolower($arrKey);

                if( isset($value) )
                {
                    $imageName = $fieldKey . '.' . $request['new'.$arrKey]->extension();

                    switch ($action) {
                        case 'edit':
                            File::delete($path . $request['old'.$arrKey]);

                            if($request['new'.$arrKey]->move( $path, $imageName))
                                $dataImage[$fieldKey] = $slug . '/' . $imageName;

                            break;
                        case 'create':
                        default:
                            if($request['new'.$arrKey]->move( $path, $imageName))
                                $dataImage[$fieldKey] = $slug . '/' . $imageName;
                            break;
                    }
                }
            }
        }

        return $dataImage;
    }

    public function showHide(Request $request, $id)
    {
        $brand = Brand::find($id);
        
        $brand->flag = $request->flag;
        
        if($brand->save())
            return true;
        else
            return false;
    }

    public function sortTable(Request $request)
    {

        foreach ($request->data as $data) {
            $brand = Brand::find($data['id']);
            $brand->sort = $data['sort'];

            $brand->save();
        }

    }

    public function imageDestroy(Request $request, $id)
    {
        $status = null;
        $brand = Brand::find($id);
        $path = public_path('storage/laraassets/images/brands/');

        // multiple case what u want to delete
        switch ($request->image) {
            case 'logo':
                $path = $path . $brand->logo;
                break;
            case 'background':
                $path = $path . $brand->background;
                break;
            case 'cover':
                $path = $path . $brand->cover;
                break;
            case 'thumbnail':
                $path = $path . $brand->thumbnail;
                break;
            case 'item':
                $path = $path . $brand->item;
                break;
        }

        if( File::delete($path) )
        {
            switch ($request->image) {
                case 'logo':
                    $brand->logo = '';
                    break;
                case 'background':
                    $brand->background = '';
                    break;
                case 'cover':
                    $brand->cover = '';
                    break;
                case 'thumbnail':
                    $brand->thumbnail = '';
                    break;
                case 'item':
                    $brand->item = '';
                    break;
            }

            $brand->save();
            $status = ["status"=> true, "message"=> "Image deleted!"];
        } else {
            $status = ["status"=> false, "message"=> "Image not found!"];
        }

        return redirect( route('section.index', ['slug' => 'client', 'id' => $id]) )->with('message', $status);
    }
}
