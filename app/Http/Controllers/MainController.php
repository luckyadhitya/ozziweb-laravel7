<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class MainController extends Controller
{
    public function index() {

        $brands = Brand::where(['flag' => 1])->whereNotNull('item')->orderBy('sort', 'asc')->get();

        $data = [
            'haszopen'      => false,
            'page'          => 'home',
            'brands'         => $brands
        ];

        return view('pages.home', $data);
    }
}
