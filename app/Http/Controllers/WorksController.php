<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\BrandPages;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class WorksController extends Controller
{
    public function index() 
    {
        $brands = Brand::where(['flag' => 1])->whereNotNull(['thumb', 'cover'])->orderBy('sort', 'asc')->get();
        $data = [
            'haszopen'  =>true,
            'title'     => 'Work',
            'page'      => 'work',
            'brands'    => $brands
            // 'brands'    => [
            //     'safi' => [
            //         'url'   => 'works/safi-indonesia',
            //         'cover' => 'storage/laraassets/images/work/cover/safi.jpg',
            //         'name'  => 'Safi Indonesia',
            //         'thumb' => 'storage/laraassets/images/work/thumb/safi.png',
            //         'project-service' => 'Annual Campaign',
            //     ],
            //     'ease' => [
            //         'url'   => 'works/ease',
            //         'cover' => 'storage/laraassets/images/work/cover/ease.jpg',
            //         'name'  => 'Ease Brand',
            //         'thumb' => 'storage/laraassets/images/work/thumb/ease.png',
            //         'project-service' => 'E-Commerce',
            //     ],
            //     'bio-essence' => [
            //         'url'   => 'works/bio-essence',
            //         'cover' => 'storage/laraassets/images/work/cover/bio-essence.jpg',
            //         'name'  => 'Bio-essence',
            //         'thumb' => 'storage/laraassets/images/work/thumb/bio-essence.png',
            //         'project-service' => 'Digital Strategy',
            //     ],
            //     'milkuat' => [
            //         'url'   => 'works/milkuat',
            //         'cover' => 'storage/laraassets/images/work/cover/milkuat.jpg',
            //         'name'  => 'Milkuat',
            //         'thumb' => 'storage/laraassets/images/work/thumb/milkuat.png',
            //         'project-service' => 'Digital Campaign',
            //     ],
            //     'bam' => [
            //         'url'   => 'works/bam',
            //         'cover' => 'storage/laraassets/images/work/cover/bam.jpg',
            //         'name'  => 'BAM!',
            //         'thumb' => 'storage/laraassets/images/work/thumb/bam.png',
            //         'project-service' => 'Annual Campaign',
            //     ],
            //     'vitalis' => [
            //         'url'   => 'works/vitalis',
            //         'cover' => 'storage/laraassets/images/work/cover/vitalis.jpg',
            //         'name'  => 'Vitalis',
            //         'thumb' => 'storage/laraassets/images/work/thumb/vitalis.png',
            //         'project-service' => 'Annual Campaign',
            //     ],
            // ]
        ];

        return view('pages.work', $data);
    }

    public function getSingle( $slug = '' )
    {
        //if ( $slug == 'vitalis' ) return redirect()->route('work');
        // switch ($slug) {
        //     case 'safi-indonesia':
        //         $title  = 'Safi Indonesia';
        //         $bg     = '#F5F5F7';
        //         $bgnavcolor = '#333';
        //         break;
        //     case 'bio-essence':
        //         $title  = 'Bio-Essence';
        //         $bg     = '#F5F5F7';
        //         $bgnavcolor = '#333';
        //         break;
        //     case 'ease':
        //         $title  = 'Ease Brand';
        //         $bg     = '#F8CACA';
        //         $bgnavcolor = '#333';
        //         break;
        //     case 'milkuat':
        //         $title  = 'Milkuat';
        //         $bg     = '#FFFFFF';
        //         $bgnavcolor = '#333';
        //         break;
        //     case 'bam':
        //         $title  = 'BAM!';
        //         $bg     = '#383636';
        //         $bgnavcolor = '#A3D900';
        //         break;
        //     case 'vitalis':
        //         $title  = 'Vitalis';
        //         $bg     = '#383636';
        //         $bgnavcolor = '#A3D900';
        //         break;
        //     default:
        //         $title  = 'Work';
        //         $bg     = '#FFFFFF';
        //         $bgnavcolor = '#333';
        //         break;
        // }

        // $data = [
        //     'haszopen'      => true,
        //     'title'         => $title,
        //     'page'          => 'work',
        //     'workpage'      => $slug,
        //     'bgColorWork'   => $bg,
        //     'bgnavcolor'    => $bgnavcolor,
        //     'brands'    => [
        //         'safi' => [
        //             'url'   => 'works/safi-indonesia',
        //             'slug'  => 'safi-indonesia',
        //             'cover' => url('storage/laraassets/images/work/cover/safi.jpg'),
        //             'name'  => 'Safi',
        //             'thumb' => url('storage/laraassets/images/work/thumb/safi.png'),
        //             'project-service' => 'Annual Campaign',
        //         ],
        //         'ease' => [
        //             'url'   => 'works/ease',
        //             'slug'  => 'ease',
        //             'cover' => url('storage/laraassets/images/work/cover/ease.jpg'),
        //             'name'  => 'Ease',
        //             'thumb' => url('storage/laraassets/images/work/thumb/ease.png'),
        //             'project-service' => 'Digital Strategy',
        //         ],
        //         'bio-essence' => [
        //             'url'   => 'works/bio-essence',
        //             'slug'  => 'bio-essence',
        //             'cover' => url('storage/laraassets/images/work/cover/bio-essence.jpg'),
        //             'name'  => 'Bio-essence',
        //             'thumb' => url('storage/laraassets/images/work/thumb/bio-essence.png'),
        //             'project-service' => 'E-Commerce',
        //         ],
        //         'milkuat' => [
        //             'url'   => 'works/milkuat',
        //             'slug'  => 'milkuat',
        //             'cover' => url('storage/laraassets/images/work/cover/milkuat.jpg'),
        //             'name'  => 'Milkuat',
        //             'thumb' => url('storage/laraassets/images/work/thumb/milkuat.png'),
        //             'project-service' => 'Digital Campaign',
        //         ],
        //         'bam' => [
        //             'url'   => 'works/bam',
        //             'slug'  => 'bam',
        //             'cover' => url('storage/laraassets/images/work/cover/bam.jpg'),
        //             'name'  => 'BAM!',
        //             'thumb' => url('storage/laraassets/images/work/thumb/bam.png'),
        //             'project-service' => 'Annual Campaign',
        //         ],
        //         'vitalis' => [
        //             'url'   => 'works/vitalis',
        //             'slug'  => 'vitalis',
        //             'cover' => url('storage/laraassets/images/work/cover/vitalis.jpg'),
        //             'name'  => 'Vitalis',
        //             'thumb' => url('storage/laraassets/images/work/thumb/vitalis.png'),
        //             'project-service' => 'Annual Campaign',
        //         ],
        //     ]
        // ];

        // return view('pages.works.'.$slug, $data);

        $brand = Brand::where([['slug', '=', $slug], ['flag', '=', 1]])->first();
        $sections = BrandPages::where('id_brand', $brand->id)->get();

        // next project

        $next = Brand::where([
            ['sort', '>', $brand->sort],
            ['flag', '=', 1],
            ['url', !null]
        ])->orderBy('sort', 'asc')->first();

        if(empty($next))
        {
            $next = Brand::where('flag', 1)->orderBy('sort', 'asc')->first();
        }
        $data = [
            'haszopen'      => true,
            'page'          => 'work',
            'brand' => $brand,
            'sections' => $sections,
            'next' => $next
        ];

        return view('pages.work-detail', $data);
    }
}
