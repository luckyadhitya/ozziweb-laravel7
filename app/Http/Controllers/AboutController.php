<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Brand;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index() 
    {
        $team = Team::orderBy('sort', 'asc')->get();
        $brands = Brand::select('name', 'logo')->where(['flag' => 1])->whereNotNull('logo')->orderBy('sort', 'asc')->get();

        $data = [
            'haszopen'  => true,
            'title'     => 'About',
            'page'      => 'about',
            'teams'     => $team,
            'brands'    => $brands
        ];

        return view('pages.about', $data);
    }
}
