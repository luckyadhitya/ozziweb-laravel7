<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index() 
    {
        $jobs = Job::where('status', 1)->get();
        $data = [
            'haszopen'  =>true,
            'title'     => 'Contact',
            'page'      => 'contact',
            'jobs'      => $jobs,
            'count'     => Job::where('status', 1)->count()
        ];

        return view('pages.contact', $data);
    }
}
