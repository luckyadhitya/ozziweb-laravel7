<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\BrandPages;
use App\Models\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class HireController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $job = Job::where('status', 1)->get();
        $data = [
            'haszopen'      => true,
            'page'          => 'Hire',
            'jobs' => $job
        ];

        return view('pages/hire', $data);
    }

    public function details($slug = '')
    {
        $job = Job::where('slug', $slug)->first();
        $data = [
            'haszopen'      => true,
            'page'          => 'Hire',
            'job' => $job
        ];

        return view('pages/hire-detail', $data);
    }
}
