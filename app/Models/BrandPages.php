<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandPages extends Model
{
    protected $table = "brand_pages";
}
