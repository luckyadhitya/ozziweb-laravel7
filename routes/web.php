<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/about', 'AboutController@index');
Route::get('/contact', 'ContactController@index');
Route::get('/works', 'WorksController@index');
Route::get('/works/{slug}', 'WorksController@getSingle');
Route::get('/hire', 'HireController@index');
Route::get('/hire/{slug}', 'HireController@details');

// Dashboard Admin
Route::group(['prefix' => "admin", 'middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@index')->name('admin');

    /**
     * team
     */
    Route::resource('/team', 'Admin\TeamController', ['name' => 'team'])->except([
        'show', 'edit'
    ]);
    Route::post('team/sortable/', ['as' => 'team.sort', 'uses' => 'Admin\TeamController@sortTable']);

    /**
     * Client
     */
    Route::resource('/client', 'Admin\ClientController', ['name' => 'client'])->except([
        'show', 'edit'
    ]);
    Route::delete('client/imageDestroy/{id}', ['as' => 'client.imgdestroy', 'uses' => 'Admin\ClientController@imageDestroy']);
    Route::put('client/toggle/{id}', ['as' => 'client.toggle', 'uses' => 'Admin\ClientController@showHide']);
    Route::post('client/sortable/', ['as' => 'client.sort', 'uses' => 'Admin\ClientController@sortTable']);

    Route::resource('/{slug}/section', 'Admin\SectionController', ['name' => 'section'])->except([
        'index', 'create', 'show', 'edit'
    ]);
    Route::get('/{slug}/section/{id}', ['as' => 'section.index', 'uses' => 'Admin\SectionController@section']);

    /**
     * Job Hire
     */
    Route::resource('/hire', 'Admin\HireController', ['name' => 'hire']);
    Route::get('/hire/{slug}', ['as' => 'hire.detail', 'uses' => 'Admin\HireController@details']);
});
// Dashboard Admin -- END

// Auth route login to dashboard
Auth::routes([
    'register' => false,
    'reset' => false,
    'verivy' => false
]);

Route::get('/home', 'HomeController@index')->name('home');
// Auth route login to dashboard -- END
