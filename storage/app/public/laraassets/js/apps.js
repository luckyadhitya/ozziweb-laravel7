var tl          = new TimelineLite({ onReverseComplete: clearReverseAnimation }),
    $body       = $("body"),
    $toggle     = $(".z-button-wrapper"),
    $sidepanel  = $(".z-sidepanel"),
    $item       = $(".z-sidepanel-list-item"),
    $overlay    = $(".z-overlay"),
    $menu       = $(".z-menu"),
    $mask       = $(".z-menu-mask-group"),
    $menuList   = $(".z-menu-list"),
    $menuAnchor = $(".z-menu-list > li > a"),
    $menuFooter = $(".z-menu-footer"),
    $menuLogo   = $(".z-menu-logo"),
    $content    = $(".z-layout");

function viewBoxArea() {
    svgH    = $mask.height(),
    svgW    = $mask.width();

    var svgMask = document.querySelector(".z-menu-mask-group");
    svgMask.setAttribute("viewBox", `0 0 ${svgW} ${svgH}`);
}

function radius() {
    frameH  = $(".z-menu-nav").height();
    var radius  = $(".z-menu-mask-group circle");

    if(window.matchMedia("(min-device-height: 480px) and (max-device-height: 736px) and (orientation: portrait)").matches) {
        radius.attr('r', frameH / 5);
    } else if(window.matchMedia("(min-device-height: 737px) and (max-device-height: 823px) and (orientation: portrait)").matches) {
        radius.attr('r', frameH / 6);
    } else if(window.matchMedia("(min-device-height: 320px) and (max-device-height: 375px) and (orientation: landscape)").matches) {
        radius.attr('r', frameH / 4);
    } else if(window.matchMedia("(min-device-height: 376px) and (max-device-height: 414px) and (orientation: landscape)").matches) {
        radius.attr('r', frameH / 5);
    } else if(window.matchMedia("(min-device-height: 1440px) and (orientation: portrait)").matches) {
        radius.attr('r', frameH / 8);
    } else (
        radius.attr('r', frameH / 5)
    )
}

tl.to($item, 0.25, {
        scale: 0,
        onComplete: function() {
            $item.css("display", "none");
        }
    })
    .to($menu, 0.25, {
        display: "flex",
        onComplete: function() {
            viewBoxArea();
        }
    })
    .to($sidepanel, 0.25, {
        width: "8%"
    })
    .to($overlay, 0.1, {
        onComplete: function onComplete() {
          $overlay.css("z-index", "1");
        }
    })
    .to($content, 0.25, { filter: "blur(4px)" })
    .to($mask, 0.5, {
        left: "0%",
        delay: -0.25,
        onStart: function() {
            radius();
        }
    })
    .staggerTo($menuAnchor, 0.5, { left: "0%" }, 0.15, "stagger")
    .to($menuFooter, 0.25, { left: "0%" })
    .to($menuLogo, 0.25, { opacity: "1" })
    .to($body, 0.25, { overflow: "hidden" });

tl.pause();

$toggle.on('click', function(clearReverseAnimation) {
    if($toggle.hasClass("expanded")) {
        tl.play();
    } else {
        tl.reverse();
    }
});

function clearReverseAnimation(){
    TimelineLite.exportRoot().clear().set($item, {
        clearProps: "all"
    })
    TimelineLite.exportRoot().clear().set($overlay, {
        clearProps: "all"
    });
}

// Init Isotope
// var $grid = $('#gridBlog').isotope({
//     itemSelector: '.ozg-blog-intro',
//     layoutMode: 'masonry',
//     masonry: {
//         columnWidth: undefined,
//         gutter: 0
//     },
//     resize: true,
//     initLayout: true
// });
// // Bind filter when category is clicked
// $('.filters-list-group').on('click', '.list-button', function(){
//     var filterValue = $(this).attr('data-filter');
    
//     filterValue = filterValue;
//     $grid.isotope({ filter: filterValue });
// });
// Change active class on clicked category
// $('.list-group').each(function( i, listGroup ){
//     var $listGroup = $(listGroup);
//     $listGroup.on('click', '.list-button', function(){
//         $listGroup.find('.active').removeClass('active');
//         $(this).addClass('active');
//     });
// });

$(window).on("resize", function(){
    viewBoxArea();
    radius();
});