/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./storage/app/public/laraassets/js/apps.js":
/*!**************************************************!*\
  !*** ./storage/app/public/laraassets/js/apps.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var tl = new TimelineLite({
  onReverseComplete: clearReverseAnimation
}),
    $body = $("body"),
    $toggle = $(".z-button-wrapper"),
    $sidepanel = $(".z-sidepanel"),
    $item = $(".z-sidepanel-list-item"),
    $overlay = $(".z-overlay"),
    $menu = $(".z-menu"),
    $mask = $(".z-menu-mask-group"),
    $menuList = $(".z-menu-list"),
    $menuAnchor = $(".z-menu-list > li > a"),
    $menuFooter = $(".z-menu-footer"),
    $menuLogo = $(".z-menu-logo"),
    $content = $(".z-layout");

function viewBoxArea() {
  svgH = $mask.height(), svgW = $mask.width();
  var svgMask = document.querySelector(".z-menu-mask-group");
  svgMask.setAttribute("viewBox", "0 0 ".concat(svgW, " ").concat(svgH));
}

function radius() {
  frameH = $(".z-menu-nav").height();
  var radius = $(".z-menu-mask-group circle");

  if (window.matchMedia("(min-device-height: 480px) and (max-device-height: 736px) and (orientation: portrait)").matches) {
    radius.attr('r', frameH / 5);
  } else if (window.matchMedia("(min-device-height: 737px) and (max-device-height: 823px) and (orientation: portrait)").matches) {
    radius.attr('r', frameH / 6);
  } else if (window.matchMedia("(min-device-height: 320px) and (max-device-height: 375px) and (orientation: landscape)").matches) {
    radius.attr('r', frameH / 4);
  } else if (window.matchMedia("(min-device-height: 376px) and (max-device-height: 414px) and (orientation: landscape)").matches) {
    radius.attr('r', frameH / 5);
  } else if (window.matchMedia("(min-device-height: 1440px) and (orientation: portrait)").matches) {
    radius.attr('r', frameH / 8);
  } else radius.attr('r', frameH / 5);
}

tl.to($item, 0.25, {
  scale: 0,
  onComplete: function onComplete() {
    $item.css("display", "none");
  }
}).to($menu, 0.25, {
  display: "flex",
  onComplete: function onComplete() {
    viewBoxArea();
  }
}).to($sidepanel, 0.25, {
  width: "8%"
}).to($overlay, 0.1, {
  onComplete: function onComplete() {
    $overlay.css("z-index", "1");
  }
}).to($content, 0.25, {
  filter: "blur(4px)"
}).to($mask, 0.5, {
  left: "0%",
  delay: -0.25,
  onStart: function onStart() {
    radius();
  }
}).staggerTo($menuAnchor, 0.5, {
  left: "0%"
}, 0.15, "stagger").to($menuFooter, 0.25, {
  left: "0%"
}).to($menuLogo, 0.25, {
  opacity: "1"
}).to($body, 0.25, {
  overflow: "hidden"
});
tl.pause();
$toggle.on('click', function (clearReverseAnimation) {
  if ($toggle.hasClass("expanded")) {
    tl.play();
  } else {
    tl.reverse();
  }
});

function clearReverseAnimation() {
  TimelineLite.exportRoot().clear().set($item, {
    clearProps: "all"
  });
  TimelineLite.exportRoot().clear().set($overlay, {
    clearProps: "all"
  });
} // Init Isotope
// var $grid = $('#gridBlog').isotope({
//     itemSelector: '.ozg-blog-intro',
//     layoutMode: 'masonry',
//     masonry: {
//         columnWidth: undefined,
//         gutter: 0
//     },
//     resize: true,
//     initLayout: true
// });
// // Bind filter when category is clicked
// $('.filters-list-group').on('click', '.list-button', function(){
//     var filterValue = $(this).attr('data-filter');
//     filterValue = filterValue;
//     $grid.isotope({ filter: filterValue });
// });
// Change active class on clicked category
// $('.list-group').each(function( i, listGroup ){
//     var $listGroup = $(listGroup);
//     $listGroup.on('click', '.list-button', function(){
//         $listGroup.find('.active').removeClass('active');
//         $(this).addClass('active');
//     });
// });


$(window).on("resize", function () {
  viewBoxArea();
  radius();
});

/***/ }),

/***/ "./storage/app/public/laraassets/js/owl.carousel.min.js":
/*!**************************************************************!*\
  !*** ./storage/app/public/laraassets/js/owl.carousel.min.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

/**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */
!function (a, b, c, d) {
  function e(b, c) {
    this.settings = null, this.options = a.extend({}, e.Defaults, c), this.$element = a(b), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
      time: null,
      target: null,
      pointer: null,
      stage: {
        start: null,
        current: null
      },
      direction: null
    }, this._states = {
      current: {},
      tags: {
        initializing: ["busy"],
        animating: ["busy"],
        dragging: ["interacting"]
      }
    }, a.each(["onResize", "onThrottledResize"], a.proxy(function (b, c) {
      this._handlers[c] = a.proxy(this[c], this);
    }, this)), a.each(e.Plugins, a.proxy(function (a, b) {
      this._plugins[a.charAt(0).toLowerCase() + a.slice(1)] = new b(this);
    }, this)), a.each(e.Workers, a.proxy(function (b, c) {
      this._pipe.push({
        filter: c.filter,
        run: a.proxy(c.run, this)
      });
    }, this)), this.setup(), this.initialize();
  }

  e.Defaults = {
    items: 3,
    loop: !1,
    center: !1,
    rewind: !1,
    checkVisibility: !0,
    mouseDrag: !0,
    touchDrag: !0,
    pullDrag: !0,
    freeDrag: !1,
    margin: 0,
    stagePadding: 0,
    merge: !1,
    mergeFit: !0,
    autoWidth: !1,
    startPosition: 0,
    rtl: !1,
    smartSpeed: 250,
    fluidSpeed: !1,
    dragEndSpeed: !1,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: b,
    fallbackEasing: "swing",
    slideTransition: "",
    info: !1,
    nestedItemSelector: !1,
    itemElement: "div",
    stageElement: "div",
    refreshClass: "owl-refresh",
    loadedClass: "owl-loaded",
    loadingClass: "owl-loading",
    rtlClass: "owl-rtl",
    responsiveClass: "owl-responsive",
    dragClass: "owl-drag",
    itemClass: "owl-item",
    stageClass: "owl-stage",
    stageOuterClass: "owl-stage-outer",
    grabClass: "owl-grab"
  }, e.Width = {
    Default: "default",
    Inner: "inner",
    Outer: "outer"
  }, e.Type = {
    Event: "event",
    State: "state"
  }, e.Plugins = {}, e.Workers = [{
    filter: ["width", "settings"],
    run: function run() {
      this._width = this.$element.width();
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(a) {
      a.current = this._items && this._items[this.relative(this._current)];
    }
  }, {
    filter: ["items", "settings"],
    run: function run() {
      this.$stage.children(".cloned").remove();
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(a) {
      var b = this.settings.margin || "",
          c = !this.settings.autoWidth,
          d = this.settings.rtl,
          e = {
        width: "auto",
        "margin-left": d ? b : "",
        "margin-right": d ? "" : b
      };
      !c && this.$stage.children().css(e), a.css = e;
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(a) {
      var b = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
          c = null,
          d = this._items.length,
          e = !this.settings.autoWidth,
          f = [];

      for (a.items = {
        merge: !1,
        width: b
      }; d--;) {
        c = this._mergers[d], c = this.settings.mergeFit && Math.min(c, this.settings.items) || c, a.items.merge = c > 1 || a.items.merge, f[d] = e ? b * c : this._items[d].width();
      }

      this._widths = f;
    }
  }, {
    filter: ["items", "settings"],
    run: function run() {
      var b = [],
          c = this._items,
          d = this.settings,
          e = Math.max(2 * d.items, 4),
          f = 2 * Math.ceil(c.length / 2),
          g = d.loop && c.length ? d.rewind ? e : Math.max(e, f) : 0,
          h = "",
          i = "";

      for (g /= 2; g > 0;) {
        b.push(this.normalize(b.length / 2, !0)), h += c[b[b.length - 1]][0].outerHTML, b.push(this.normalize(c.length - 1 - (b.length - 1) / 2, !0)), i = c[b[b.length - 1]][0].outerHTML + i, g -= 1;
      }

      this._clones = b, a(h).addClass("cloned").appendTo(this.$stage), a(i).addClass("cloned").prependTo(this.$stage);
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run() {
      for (var a = this.settings.rtl ? 1 : -1, b = this._clones.length + this._items.length, c = -1, d = 0, e = 0, f = []; ++c < b;) {
        d = f[c - 1] || 0, e = this._widths[this.relative(c)] + this.settings.margin, f.push(d + e * a);
      }

      this._coordinates = f;
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run() {
      var a = this.settings.stagePadding,
          b = this._coordinates,
          c = {
        width: Math.ceil(Math.abs(b[b.length - 1])) + 2 * a,
        "padding-left": a || "",
        "padding-right": a || ""
      };
      this.$stage.css(c);
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(a) {
      var b = this._coordinates.length,
          c = !this.settings.autoWidth,
          d = this.$stage.children();
      if (c && a.items.merge) for (; b--;) {
        a.css.width = this._widths[this.relative(b)], d.eq(b).css(a.css);
      } else c && (a.css.width = a.items.width, d.css(a.css));
    }
  }, {
    filter: ["items"],
    run: function run() {
      this._coordinates.length < 1 && this.$stage.removeAttr("style");
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(a) {
      a.current = a.current ? this.$stage.children().index(a.current) : 0, a.current = Math.max(this.minimum(), Math.min(this.maximum(), a.current)), this.reset(a.current);
    }
  }, {
    filter: ["position"],
    run: function run() {
      this.animate(this.coordinates(this._current));
    }
  }, {
    filter: ["width", "position", "items", "settings"],
    run: function run() {
      var a,
          b,
          c,
          d,
          e = this.settings.rtl ? 1 : -1,
          f = 2 * this.settings.stagePadding,
          g = this.coordinates(this.current()) + f,
          h = g + this.width() * e,
          i = [];

      for (c = 0, d = this._coordinates.length; c < d; c++) {
        a = this._coordinates[c - 1] || 0, b = Math.abs(this._coordinates[c]) + f * e, (this.op(a, "<=", g) && this.op(a, ">", h) || this.op(b, "<", g) && this.op(b, ">", h)) && i.push(c);
      }

      this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + i.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center");
    }
  }], e.prototype.initializeStage = function () {
    this.$stage = this.$element.find("." + this.settings.stageClass), this.$stage.length || (this.$element.addClass(this.options.loadingClass), this.$stage = a("<" + this.settings.stageElement + ">", {
      "class": this.settings.stageClass
    }).wrap(a("<div/>", {
      "class": this.settings.stageOuterClass
    })), this.$element.append(this.$stage.parent()));
  }, e.prototype.initializeItems = function () {
    var b = this.$element.find(".owl-item");
    if (b.length) return this._items = b.get().map(function (b) {
      return a(b);
    }), this._mergers = this._items.map(function () {
      return 1;
    }), void this.refresh();
    this.replace(this.$element.children().not(this.$stage.parent())), this.isVisible() ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass);
  }, e.prototype.initialize = function () {
    if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
      var a, b, c;
      a = this.$element.find("img"), b = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : d, c = this.$element.children(b).width(), a.length && c <= 0 && this.preloadAutoWidthImages(a);
    }

    this.initializeStage(), this.initializeItems(), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized");
  }, e.prototype.isVisible = function () {
    return !this.settings.checkVisibility || this.$element.is(":visible");
  }, e.prototype.setup = function () {
    var b = this.viewport(),
        c = this.options.responsive,
        d = -1,
        e = null;
    c ? (a.each(c, function (a) {
      a <= b && a > d && (d = Number(a));
    }), e = a.extend({}, this.options, c[d]), "function" == typeof e.stagePadding && (e.stagePadding = e.stagePadding()), delete e.responsive, e.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + d))) : e = a.extend({}, this.options), this.trigger("change", {
      property: {
        name: "settings",
        value: e
      }
    }), this._breakpoint = d, this.settings = e, this.invalidate("settings"), this.trigger("changed", {
      property: {
        name: "settings",
        value: this.settings
      }
    });
  }, e.prototype.optionsLogic = function () {
    this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1);
  }, e.prototype.prepare = function (b) {
    var c = this.trigger("prepare", {
      content: b
    });
    return c.data || (c.data = a("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(b)), this.trigger("prepared", {
      content: c.data
    }), c.data;
  }, e.prototype.update = function () {
    for (var b = 0, c = this._pipe.length, d = a.proxy(function (a) {
      return this[a];
    }, this._invalidated), e = {}; b < c;) {
      (this._invalidated.all || a.grep(this._pipe[b].filter, d).length > 0) && this._pipe[b].run(e), b++;
    }

    this._invalidated = {}, !this.is("valid") && this.enter("valid");
  }, e.prototype.width = function (a) {
    switch (a = a || e.Width.Default) {
      case e.Width.Inner:
      case e.Width.Outer:
        return this._width;

      default:
        return this._width - 2 * this.settings.stagePadding + this.settings.margin;
    }
  }, e.prototype.refresh = function () {
    this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed");
  }, e.prototype.onThrottledResize = function () {
    b.clearTimeout(this.resizeTimer), this.resizeTimer = b.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
  }, e.prototype.onResize = function () {
    return !!this._items.length && this._width !== this.$element.width() && !!this.isVisible() && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")));
  }, e.prototype.registerEventHandlers = function () {
    a.support.transition && this.$stage.on(a.support.transition.end + ".owl.core", a.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(b, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
      return !1;
    })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", a.proxy(this.onDragEnd, this)));
  }, e.prototype.onDragStart = function (b) {
    var d = null;
    3 !== b.which && (a.support.transform ? (d = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), d = {
      x: d[16 === d.length ? 12 : 4],
      y: d[16 === d.length ? 13 : 5]
    }) : (d = this.$stage.position(), d = {
      x: this.settings.rtl ? d.left + this.$stage.width() - this.width() + this.settings.margin : d.left,
      y: d.top
    }), this.is("animating") && (a.support.transform ? this.animate(d.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === b.type), this.speed(0), this._drag.time = new Date().getTime(), this._drag.target = a(b.target), this._drag.stage.start = d, this._drag.stage.current = d, this._drag.pointer = this.pointer(b), a(c).on("mouseup.owl.core touchend.owl.core", a.proxy(this.onDragEnd, this)), a(c).one("mousemove.owl.core touchmove.owl.core", a.proxy(function (b) {
      var d = this.difference(this._drag.pointer, this.pointer(b));
      a(c).on("mousemove.owl.core touchmove.owl.core", a.proxy(this.onDragMove, this)), Math.abs(d.x) < Math.abs(d.y) && this.is("valid") || (b.preventDefault(), this.enter("dragging"), this.trigger("drag"));
    }, this)));
  }, e.prototype.onDragMove = function (a) {
    var b = null,
        c = null,
        d = null,
        e = this.difference(this._drag.pointer, this.pointer(a)),
        f = this.difference(this._drag.stage.start, e);
    this.is("dragging") && (a.preventDefault(), this.settings.loop ? (b = this.coordinates(this.minimum()), c = this.coordinates(this.maximum() + 1) - b, f.x = ((f.x - b) % c + c) % c + b) : (b = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), c = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), d = this.settings.pullDrag ? -1 * e.x / 5 : 0, f.x = Math.max(Math.min(f.x, b + d), c + d)), this._drag.stage.current = f, this.animate(f.x));
  }, e.prototype.onDragEnd = function (b) {
    var d = this.difference(this._drag.pointer, this.pointer(b)),
        e = this._drag.stage.current,
        f = d.x > 0 ^ this.settings.rtl ? "left" : "right";
    a(c).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== d.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(e.x, 0 !== d.x ? f : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = f, (Math.abs(d.x) > 3 || new Date().getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
      return !1;
    })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"));
  }, e.prototype.closest = function (b, c) {
    var e = -1,
        f = 30,
        g = this.width(),
        h = this.coordinates();
    return this.settings.freeDrag || a.each(h, a.proxy(function (a, i) {
      return "left" === c && b > i - f && b < i + f ? e = a : "right" === c && b > i - g - f && b < i - g + f ? e = a + 1 : this.op(b, "<", i) && this.op(b, ">", h[a + 1] !== d ? h[a + 1] : i - g) && (e = "left" === c ? a + 1 : a), -1 === e;
    }, this)), this.settings.loop || (this.op(b, ">", h[this.minimum()]) ? e = b = this.minimum() : this.op(b, "<", h[this.maximum()]) && (e = b = this.maximum())), e;
  }, e.prototype.animate = function (b) {
    var c = this.speed() > 0;
    this.is("animating") && this.onTransitionEnd(), c && (this.enter("animating"), this.trigger("translate")), a.support.transform3d && a.support.transition ? this.$stage.css({
      transform: "translate3d(" + b + "px,0px,0px)",
      transition: this.speed() / 1e3 + "s" + (this.settings.slideTransition ? " " + this.settings.slideTransition : "")
    }) : c ? this.$stage.animate({
      left: b + "px"
    }, this.speed(), this.settings.fallbackEasing, a.proxy(this.onTransitionEnd, this)) : this.$stage.css({
      left: b + "px"
    });
  }, e.prototype.is = function (a) {
    return this._states.current[a] && this._states.current[a] > 0;
  }, e.prototype.current = function (a) {
    if (a === d) return this._current;
    if (0 === this._items.length) return d;

    if (a = this.normalize(a), this._current !== a) {
      var b = this.trigger("change", {
        property: {
          name: "position",
          value: a
        }
      });
      b.data !== d && (a = this.normalize(b.data)), this._current = a, this.invalidate("position"), this.trigger("changed", {
        property: {
          name: "position",
          value: this._current
        }
      });
    }

    return this._current;
  }, e.prototype.invalidate = function (b) {
    return "string" === a.type(b) && (this._invalidated[b] = !0, this.is("valid") && this.leave("valid")), a.map(this._invalidated, function (a, b) {
      return b;
    });
  }, e.prototype.reset = function (a) {
    (a = this.normalize(a)) !== d && (this._speed = 0, this._current = a, this.suppress(["translate", "translated"]), this.animate(this.coordinates(a)), this.release(["translate", "translated"]));
  }, e.prototype.normalize = function (a, b) {
    var c = this._items.length,
        e = b ? 0 : this._clones.length;
    return !this.isNumeric(a) || c < 1 ? a = d : (a < 0 || a >= c + e) && (a = ((a - e / 2) % c + c) % c + e / 2), a;
  }, e.prototype.relative = function (a) {
    return a -= this._clones.length / 2, this.normalize(a, !0);
  }, e.prototype.maximum = function (a) {
    var b,
        c,
        d,
        e = this.settings,
        f = this._coordinates.length;
    if (e.loop) f = this._clones.length / 2 + this._items.length - 1;else if (e.autoWidth || e.merge) {
      if (b = this._items.length) for (c = this._items[--b].width(), d = this.$element.width(); b-- && !((c += this._items[b].width() + this.settings.margin) > d);) {
        ;
      }
      f = b + 1;
    } else f = e.center ? this._items.length - 1 : this._items.length - e.items;
    return a && (f -= this._clones.length / 2), Math.max(f, 0);
  }, e.prototype.minimum = function (a) {
    return a ? 0 : this._clones.length / 2;
  }, e.prototype.items = function (a) {
    return a === d ? this._items.slice() : (a = this.normalize(a, !0), this._items[a]);
  }, e.prototype.mergers = function (a) {
    return a === d ? this._mergers.slice() : (a = this.normalize(a, !0), this._mergers[a]);
  }, e.prototype.clones = function (b) {
    var c = this._clones.length / 2,
        e = c + this._items.length,
        f = function f(a) {
      return a % 2 == 0 ? e + a / 2 : c - (a + 1) / 2;
    };

    return b === d ? a.map(this._clones, function (a, b) {
      return f(b);
    }) : a.map(this._clones, function (a, c) {
      return a === b ? f(c) : null;
    });
  }, e.prototype.speed = function (a) {
    return a !== d && (this._speed = a), this._speed;
  }, e.prototype.coordinates = function (b) {
    var c,
        e = 1,
        f = b - 1;
    return b === d ? a.map(this._coordinates, a.proxy(function (a, b) {
      return this.coordinates(b);
    }, this)) : (this.settings.center ? (this.settings.rtl && (e = -1, f = b + 1), c = this._coordinates[b], c += (this.width() - c + (this._coordinates[f] || 0)) / 2 * e) : c = this._coordinates[f] || 0, c = Math.ceil(c));
  }, e.prototype.duration = function (a, b, c) {
    return 0 === c ? 0 : Math.min(Math.max(Math.abs(b - a), 1), 6) * Math.abs(c || this.settings.smartSpeed);
  }, e.prototype.to = function (a, b) {
    var c = this.current(),
        d = null,
        e = a - this.relative(c),
        f = (e > 0) - (e < 0),
        g = this._items.length,
        h = this.minimum(),
        i = this.maximum();
    this.settings.loop ? (!this.settings.rewind && Math.abs(e) > g / 2 && (e += -1 * f * g), a = c + e, (d = ((a - h) % g + g) % g + h) !== a && d - e <= i && d - e > 0 && (c = d - e, a = d, this.reset(c))) : this.settings.rewind ? (i += 1, a = (a % i + i) % i) : a = Math.max(h, Math.min(i, a)), this.speed(this.duration(c, a, b)), this.current(a), this.isVisible() && this.update();
  }, e.prototype.next = function (a) {
    a = a || !1, this.to(this.relative(this.current()) + 1, a);
  }, e.prototype.prev = function (a) {
    a = a || !1, this.to(this.relative(this.current()) - 1, a);
  }, e.prototype.onTransitionEnd = function (a) {
    if (a !== d && (a.stopPropagation(), (a.target || a.srcElement || a.originalTarget) !== this.$stage.get(0))) return !1;
    this.leave("animating"), this.trigger("translated");
  }, e.prototype.viewport = function () {
    var d;
    return this.options.responsiveBaseElement !== b ? d = a(this.options.responsiveBaseElement).width() : b.innerWidth ? d = b.innerWidth : c.documentElement && c.documentElement.clientWidth ? d = c.documentElement.clientWidth : console.warn("Can not detect viewport width."), d;
  }, e.prototype.replace = function (b) {
    this.$stage.empty(), this._items = [], b && (b = b instanceof jQuery ? b : a(b)), this.settings.nestedItemSelector && (b = b.find("." + this.settings.nestedItemSelector)), b.filter(function () {
      return 1 === this.nodeType;
    }).each(a.proxy(function (a, b) {
      b = this.prepare(b), this.$stage.append(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1);
    }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items");
  }, e.prototype.add = function (b, c) {
    var e = this.relative(this._current);
    c = c === d ? this._items.length : this.normalize(c, !0), b = b instanceof jQuery ? b : a(b), this.trigger("add", {
      content: b,
      position: c
    }), b = this.prepare(b), 0 === this._items.length || c === this._items.length ? (0 === this._items.length && this.$stage.append(b), 0 !== this._items.length && this._items[c - 1].after(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[c].before(b), this._items.splice(c, 0, b), this._mergers.splice(c, 0, 1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[e] && this.reset(this._items[e].index()), this.invalidate("items"), this.trigger("added", {
      content: b,
      position: c
    });
  }, e.prototype.remove = function (a) {
    (a = this.normalize(a, !0)) !== d && (this.trigger("remove", {
      content: this._items[a],
      position: a
    }), this._items[a].remove(), this._items.splice(a, 1), this._mergers.splice(a, 1), this.invalidate("items"), this.trigger("removed", {
      content: null,
      position: a
    }));
  }, e.prototype.preloadAutoWidthImages = function (b) {
    b.each(a.proxy(function (b, c) {
      this.enter("pre-loading"), c = a(c), a(new Image()).one("load", a.proxy(function (a) {
        c.attr("src", a.target.src), c.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh();
      }, this)).attr("src", c.attr("src") || c.attr("data-src") || c.attr("data-src-retina"));
    }, this));
  }, e.prototype.destroy = function () {
    this.$element.off(".owl.core"), this.$stage.off(".owl.core"), a(c).off(".owl.core"), !1 !== this.settings.responsive && (b.clearTimeout(this.resizeTimer), this.off(b, "resize", this._handlers.onThrottledResize));

    for (var d in this._plugins) {
      this._plugins[d].destroy();
    }

    this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel");
  }, e.prototype.op = function (a, b, c) {
    var d = this.settings.rtl;

    switch (b) {
      case "<":
        return d ? a > c : a < c;

      case ">":
        return d ? a < c : a > c;

      case ">=":
        return d ? a <= c : a >= c;

      case "<=":
        return d ? a >= c : a <= c;
    }
  }, e.prototype.on = function (a, b, c, d) {
    a.addEventListener ? a.addEventListener(b, c, d) : a.attachEvent && a.attachEvent("on" + b, c);
  }, e.prototype.off = function (a, b, c, d) {
    a.removeEventListener ? a.removeEventListener(b, c, d) : a.detachEvent && a.detachEvent("on" + b, c);
  }, e.prototype.trigger = function (b, c, d, f, g) {
    var h = {
      item: {
        count: this._items.length,
        index: this.current()
      }
    },
        i = a.camelCase(a.grep(["on", b, d], function (a) {
      return a;
    }).join("-").toLowerCase()),
        j = a.Event([b, "owl", d || "carousel"].join(".").toLowerCase(), a.extend({
      relatedTarget: this
    }, h, c));
    return this._supress[b] || (a.each(this._plugins, function (a, b) {
      b.onTrigger && b.onTrigger(j);
    }), this.register({
      type: e.Type.Event,
      name: b
    }), this.$element.trigger(j), this.settings && "function" == typeof this.settings[i] && this.settings[i].call(this, j)), j;
  }, e.prototype.enter = function (b) {
    a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) {
      this._states.current[b] === d && (this._states.current[b] = 0), this._states.current[b]++;
    }, this));
  }, e.prototype.leave = function (b) {
    a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) {
      this._states.current[b]--;
    }, this));
  }, e.prototype.register = function (b) {
    if (b.type === e.Type.Event) {
      if (a.event.special[b.name] || (a.event.special[b.name] = {}), !a.event.special[b.name].owl) {
        var c = a.event.special[b.name]._default;
        a.event.special[b.name]._default = function (a) {
          return !c || !c.apply || a.namespace && -1 !== a.namespace.indexOf("owl") ? a.namespace && a.namespace.indexOf("owl") > -1 : c.apply(this, arguments);
        }, a.event.special[b.name].owl = !0;
      }
    } else b.type === e.Type.State && (this._states.tags[b.name] ? this._states.tags[b.name] = this._states.tags[b.name].concat(b.tags) : this._states.tags[b.name] = b.tags, this._states.tags[b.name] = a.grep(this._states.tags[b.name], a.proxy(function (c, d) {
      return a.inArray(c, this._states.tags[b.name]) === d;
    }, this)));
  }, e.prototype.suppress = function (b) {
    a.each(b, a.proxy(function (a, b) {
      this._supress[b] = !0;
    }, this));
  }, e.prototype.release = function (b) {
    a.each(b, a.proxy(function (a, b) {
      delete this._supress[b];
    }, this));
  }, e.prototype.pointer = function (a) {
    var c = {
      x: null,
      y: null
    };
    return a = a.originalEvent || a || b.event, a = a.touches && a.touches.length ? a.touches[0] : a.changedTouches && a.changedTouches.length ? a.changedTouches[0] : a, a.pageX ? (c.x = a.pageX, c.y = a.pageY) : (c.x = a.clientX, c.y = a.clientY), c;
  }, e.prototype.isNumeric = function (a) {
    return !isNaN(parseFloat(a));
  }, e.prototype.difference = function (a, b) {
    return {
      x: a.x - b.x,
      y: a.y - b.y
    };
  }, a.fn.owlCarousel = function (b) {
    var c = Array.prototype.slice.call(arguments, 1);
    return this.each(function () {
      var d = a(this),
          f = d.data("owl.carousel");
      f || (f = new e(this, "object" == _typeof(b) && b), d.data("owl.carousel", f), a.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (b, c) {
        f.register({
          type: e.Type.Event,
          name: c
        }), f.$element.on(c + ".owl.carousel.core", a.proxy(function (a) {
          a.namespace && a.relatedTarget !== this && (this.suppress([c]), f[c].apply(this, [].slice.call(arguments, 1)), this.release([c]));
        }, f));
      })), "string" == typeof b && "_" !== b.charAt(0) && f[b].apply(f, c);
    });
  }, a.fn.owlCarousel.Constructor = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  var e = function e(b) {
    this._core = b, this._interval = null, this._visible = null, this._handlers = {
      "initialized.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.autoRefresh && this.watch();
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers);
  };

  e.Defaults = {
    autoRefresh: !0,
    autoRefreshInterval: 500
  }, e.prototype.watch = function () {
    this._interval || (this._visible = this._core.isVisible(), this._interval = b.setInterval(a.proxy(this.refresh, this), this._core.settings.autoRefreshInterval));
  }, e.prototype.refresh = function () {
    this._core.isVisible() !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh());
  }, e.prototype.destroy = function () {
    var a, c;
    b.clearInterval(this._interval);

    for (a in this._handlers) {
      this._core.$element.off(a, this._handlers[a]);
    }

    for (c in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[c] && (this[c] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.AutoRefresh = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  var e = function e(b) {
    this._core = b, this._loaded = [], this._handlers = {
      "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(function (b) {
        if (b.namespace && this._core.settings && this._core.settings.lazyLoad && (b.property && "position" == b.property.name || "initialized" == b.type)) {
          var c = this._core.settings,
              e = c.center && Math.ceil(c.items / 2) || c.items,
              f = c.center && -1 * e || 0,
              g = (b.property && b.property.value !== d ? b.property.value : this._core.current()) + f,
              h = this._core.clones().length,
              i = a.proxy(function (a, b) {
            this.load(b);
          }, this);

          for (c.lazyLoadEager > 0 && (e += c.lazyLoadEager, c.loop && (g -= c.lazyLoadEager, e++)); f++ < e;) {
            this.load(h / 2 + this._core.relative(g)), h && a.each(this._core.clones(this._core.relative(g)), i), g++;
          }
        }
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers);
  };

  e.Defaults = {
    lazyLoad: !1,
    lazyLoadEager: 0
  }, e.prototype.load = function (c) {
    var d = this._core.$stage.children().eq(c),
        e = d && d.find(".owl-lazy");

    !e || a.inArray(d.get(0), this._loaded) > -1 || (e.each(a.proxy(function (c, d) {
      var e,
          f = a(d),
          g = b.devicePixelRatio > 1 && f.attr("data-src-retina") || f.attr("data-src") || f.attr("data-srcset");
      this._core.trigger("load", {
        element: f,
        url: g
      }, "lazy"), f.is("img") ? f.one("load.owl.lazy", a.proxy(function () {
        f.css("opacity", 1), this._core.trigger("loaded", {
          element: f,
          url: g
        }, "lazy");
      }, this)).attr("src", g) : f.is("source") ? f.one("load.owl.lazy", a.proxy(function () {
        this._core.trigger("loaded", {
          element: f,
          url: g
        }, "lazy");
      }, this)).attr("srcset", g) : (e = new Image(), e.onload = a.proxy(function () {
        f.css({
          "background-image": 'url("' + g + '")',
          opacity: "1"
        }), this._core.trigger("loaded", {
          element: f,
          url: g
        }, "lazy");
      }, this), e.src = g);
    }, this)), this._loaded.push(d.get(0)));
  }, e.prototype.destroy = function () {
    var a, b;

    for (a in this.handlers) {
      this._core.$element.off(a, this.handlers[a]);
    }

    for (b in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[b] && (this[b] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.Lazy = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  var e = function e(c) {
    this._core = c, this._previousHeight = null, this._handlers = {
      "initialized.owl.carousel refreshed.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.autoHeight && this.update();
      }, this),
      "changed.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.autoHeight && "position" === a.property.name && this.update();
      }, this),
      "loaded.owl.lazy": a.proxy(function (a) {
        a.namespace && this._core.settings.autoHeight && a.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update();
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._intervalId = null;
    var d = this;
    a(b).on("load", function () {
      d._core.settings.autoHeight && d.update();
    }), a(b).resize(function () {
      d._core.settings.autoHeight && (null != d._intervalId && clearTimeout(d._intervalId), d._intervalId = setTimeout(function () {
        d.update();
      }, 250));
    });
  };

  e.Defaults = {
    autoHeight: !1,
    autoHeightClass: "owl-height"
  }, e.prototype.update = function () {
    var b = this._core._current,
        c = b + this._core.settings.items,
        d = this._core.settings.lazyLoad,
        e = this._core.$stage.children().toArray().slice(b, c),
        f = [],
        g = 0;

    a.each(e, function (b, c) {
      f.push(a(c).height());
    }), g = Math.max.apply(null, f), g <= 1 && d && this._previousHeight && (g = this._previousHeight), this._previousHeight = g, this._core.$stage.parent().height(g).addClass(this._core.settings.autoHeightClass);
  }, e.prototype.destroy = function () {
    var a, b;

    for (a in this._handlers) {
      this._core.$element.off(a, this._handlers[a]);
    }

    for (b in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[b] && (this[b] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.AutoHeight = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  var e = function e(b) {
    this._core = b, this._videos = {}, this._playing = null, this._handlers = {
      "initialized.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.register({
          type: "state",
          name: "playing",
          tags: ["interacting"]
        });
      }, this),
      "resize.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.video && this.isInFullScreen() && a.preventDefault();
      }, this),
      "refreshed.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove();
      }, this),
      "changed.owl.carousel": a.proxy(function (a) {
        a.namespace && "position" === a.property.name && this._playing && this.stop();
      }, this),
      "prepared.owl.carousel": a.proxy(function (b) {
        if (b.namespace) {
          var c = a(b.content).find(".owl-video");
          c.length && (c.css("display", "none"), this.fetch(c, a(b.content)));
        }
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", a.proxy(function (a) {
      this.play(a);
    }, this));
  };

  e.Defaults = {
    video: !1,
    videoHeight: !1,
    videoWidth: !1
  }, e.prototype.fetch = function (a, b) {
    var c = function () {
      return a.attr("data-vimeo-id") ? "vimeo" : a.attr("data-vzaar-id") ? "vzaar" : "youtube";
    }(),
        d = a.attr("data-vimeo-id") || a.attr("data-youtube-id") || a.attr("data-vzaar-id"),
        e = a.attr("data-width") || this._core.settings.videoWidth,
        f = a.attr("data-height") || this._core.settings.videoHeight,
        g = a.attr("href");

    if (!g) throw new Error("Missing video URL.");
    if (d = g.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), d[3].indexOf("youtu") > -1) c = "youtube";else if (d[3].indexOf("vimeo") > -1) c = "vimeo";else {
      if (!(d[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
      c = "vzaar";
    }
    d = d[6], this._videos[g] = {
      type: c,
      id: d,
      width: e,
      height: f
    }, b.attr("data-video", g), this.thumbnail(a, this._videos[g]);
  }, e.prototype.thumbnail = function (b, c) {
    var d,
        e,
        f,
        g = c.width && c.height ? "width:" + c.width + "px;height:" + c.height + "px;" : "",
        h = b.find("img"),
        i = "src",
        j = "",
        k = this._core.settings,
        l = function l(c) {
      e = '<div class="owl-video-play-icon"></div>', d = k.lazyLoad ? a("<div/>", {
        "class": "owl-video-tn " + j,
        srcType: c
      }) : a("<div/>", {
        "class": "owl-video-tn",
        style: "opacity:1;background-image:url(" + c + ")"
      }), b.after(d), b.after(e);
    };

    if (b.wrap(a("<div/>", {
      "class": "owl-video-wrapper",
      style: g
    })), this._core.settings.lazyLoad && (i = "data-src", j = "owl-lazy"), h.length) return l(h.attr(i)), h.remove(), !1;
    "youtube" === c.type ? (f = "//img.youtube.com/vi/" + c.id + "/hqdefault.jpg", l(f)) : "vimeo" === c.type ? a.ajax({
      type: "GET",
      url: "//vimeo.com/api/v2/video/" + c.id + ".json",
      jsonp: "callback",
      dataType: "jsonp",
      success: function success(a) {
        f = a[0].thumbnail_large, l(f);
      }
    }) : "vzaar" === c.type && a.ajax({
      type: "GET",
      url: "//vzaar.com/api/videos/" + c.id + ".json",
      jsonp: "callback",
      dataType: "jsonp",
      success: function success(a) {
        f = a.framegrab_url, l(f);
      }
    });
  }, e.prototype.stop = function () {
    this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video");
  }, e.prototype.play = function (b) {
    var c,
        d = a(b.target),
        e = d.closest("." + this._core.settings.itemClass),
        f = this._videos[e.attr("data-video")],
        g = f.width || "100%",
        h = f.height || this._core.$stage.height();

    this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), e = this._core.items(this._core.relative(e.index())), this._core.reset(e.index()), c = a('<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'), c.attr("height", h), c.attr("width", g), "youtube" === f.type ? c.attr("src", "//www.youtube.com/embed/" + f.id + "?autoplay=1&rel=0&v=" + f.id) : "vimeo" === f.type ? c.attr("src", "//player.vimeo.com/video/" + f.id + "?autoplay=1") : "vzaar" === f.type && c.attr("src", "//view.vzaar.com/" + f.id + "/player?autoplay=true"), a(c).wrap('<div class="owl-video-frame" />').insertAfter(e.find(".owl-video")), this._playing = e.addClass("owl-video-playing"));
  }, e.prototype.isInFullScreen = function () {
    var b = c.fullscreenElement || c.mozFullScreenElement || c.webkitFullscreenElement;
    return b && a(b).parent().hasClass("owl-video-frame");
  }, e.prototype.destroy = function () {
    var a, b;

    this._core.$element.off("click.owl.video");

    for (a in this._handlers) {
      this._core.$element.off(a, this._handlers[a]);
    }

    for (b in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[b] && (this[b] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.Video = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  var e = function e(b) {
    this.core = b, this.core.options = a.extend({}, e.Defaults, this.core.options), this.swapping = !0, this.previous = d, this.next = d, this.handlers = {
      "change.owl.carousel": a.proxy(function (a) {
        a.namespace && "position" == a.property.name && (this.previous = this.core.current(), this.next = a.property.value);
      }, this),
      "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": a.proxy(function (a) {
        a.namespace && (this.swapping = "translated" == a.type);
      }, this),
      "translate.owl.carousel": a.proxy(function (a) {
        a.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap();
      }, this)
    }, this.core.$element.on(this.handlers);
  };

  e.Defaults = {
    animateOut: !1,
    animateIn: !1
  }, e.prototype.swap = function () {
    if (1 === this.core.settings.items && a.support.animation && a.support.transition) {
      this.core.speed(0);
      var b,
          c = a.proxy(this.clear, this),
          d = this.core.$stage.children().eq(this.previous),
          e = this.core.$stage.children().eq(this.next),
          f = this.core.settings.animateIn,
          g = this.core.settings.animateOut;
      this.core.current() !== this.previous && (g && (b = this.core.coordinates(this.previous) - this.core.coordinates(this.next), d.one(a.support.animation.end, c).css({
        left: b + "px"
      }).addClass("animated owl-animated-out").addClass(g)), f && e.one(a.support.animation.end, c).addClass("animated owl-animated-in").addClass(f));
    }
  }, e.prototype.clear = function (b) {
    a(b.target).css({
      left: ""
    }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd();
  }, e.prototype.destroy = function () {
    var a, b;

    for (a in this.handlers) {
      this.core.$element.off(a, this.handlers[a]);
    }

    for (b in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[b] && (this[b] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.Animate = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  var e = function e(b) {
    this._core = b, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = {
      "changed.owl.carousel": a.proxy(function (a) {
        a.namespace && "settings" === a.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : a.namespace && "position" === a.property.name && this._paused && (this._time = 0);
      }, this),
      "initialized.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.autoplay && this.play();
      }, this),
      "play.owl.autoplay": a.proxy(function (a, b, c) {
        a.namespace && this.play(b, c);
      }, this),
      "stop.owl.autoplay": a.proxy(function (a) {
        a.namespace && this.stop();
      }, this),
      "mouseover.owl.autoplay": a.proxy(function () {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
      }, this),
      "mouseleave.owl.autoplay": a.proxy(function () {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play();
      }, this),
      "touchstart.owl.core": a.proxy(function () {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
      }, this),
      "touchend.owl.core": a.proxy(function () {
        this._core.settings.autoplayHoverPause && this.play();
      }, this)
    }, this._core.$element.on(this._handlers), this._core.options = a.extend({}, e.Defaults, this._core.options);
  };

  e.Defaults = {
    autoplay: !1,
    autoplayTimeout: 5e3,
    autoplayHoverPause: !1,
    autoplaySpeed: !1
  }, e.prototype._next = function (d) {
    this._call = b.setTimeout(a.proxy(this._next, this, d), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("interacting") || c.hidden || this._core.next(d || this._core.settings.autoplaySpeed);
  }, e.prototype.read = function () {
    return new Date().getTime() - this._time;
  }, e.prototype.play = function (c, d) {
    var e;
    this._core.is("rotating") || this._core.enter("rotating"), c = c || this._core.settings.autoplayTimeout, e = Math.min(this._time % (this._timeout || c), c), this._paused ? (this._time = this.read(), this._paused = !1) : b.clearTimeout(this._call), this._time += this.read() % c - e, this._timeout = c, this._call = b.setTimeout(a.proxy(this._next, this, d), c - e);
  }, e.prototype.stop = function () {
    this._core.is("rotating") && (this._time = 0, this._paused = !0, b.clearTimeout(this._call), this._core.leave("rotating"));
  }, e.prototype.pause = function () {
    this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, b.clearTimeout(this._call));
  }, e.prototype.destroy = function () {
    var a, b;
    this.stop();

    for (a in this._handlers) {
      this._core.$element.off(a, this._handlers[a]);
    }

    for (b in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[b] && (this[b] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.autoplay = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  "use strict";

  var e = function e(b) {
    this._core = b, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
      next: this._core.next,
      prev: this._core.prev,
      to: this._core.to
    }, this._handlers = {
      "prepared.owl.carousel": a.proxy(function (b) {
        b.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + a(b.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>");
      }, this),
      "added.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 0, this._templates.pop());
      }, this),
      "remove.owl.carousel": a.proxy(function (a) {
        a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 1);
      }, this),
      "changed.owl.carousel": a.proxy(function (a) {
        a.namespace && "position" == a.property.name && this.draw();
      }, this),
      "initialized.owl.carousel": a.proxy(function (a) {
        a.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"));
      }, this),
      "refreshed.owl.carousel": a.proxy(function (a) {
        a.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"));
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers);
  };

  e.Defaults = {
    nav: !1,
    navText: ['<span aria-label="Previous">&#x2039;</span>', '<span aria-label="Next">&#x203a;</span>'],
    navSpeed: !1,
    navElement: 'button type="button" role="presentation"',
    navContainer: !1,
    navContainerClass: "owl-nav",
    navClass: ["owl-prev", "owl-next"],
    slideBy: 1,
    dotClass: "owl-dot",
    dotsClass: "owl-dots",
    dots: !0,
    dotsEach: !1,
    dotsData: !1,
    dotsSpeed: !1,
    dotsContainer: !1
  }, e.prototype.initialize = function () {
    var b,
        c = this._core.settings;
    this._controls.$relative = (c.navContainer ? a(c.navContainer) : a("<div>").addClass(c.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = a("<" + c.navElement + ">").addClass(c.navClass[0]).html(c.navText[0]).prependTo(this._controls.$relative).on("click", a.proxy(function (a) {
      this.prev(c.navSpeed);
    }, this)), this._controls.$next = a("<" + c.navElement + ">").addClass(c.navClass[1]).html(c.navText[1]).appendTo(this._controls.$relative).on("click", a.proxy(function (a) {
      this.next(c.navSpeed);
    }, this)), c.dotsData || (this._templates = [a('<button role="button">').addClass(c.dotClass).append(a("<span>")).prop("outerHTML")]), this._controls.$absolute = (c.dotsContainer ? a(c.dotsContainer) : a("<div>").addClass(c.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", a.proxy(function (b) {
      var d = a(b.target).parent().is(this._controls.$absolute) ? a(b.target).index() : a(b.target).parent().index();
      b.preventDefault(), this.to(d, c.dotsSpeed);
    }, this));

    for (b in this._overrides) {
      this._core[b] = a.proxy(this[b], this);
    }
  }, e.prototype.destroy = function () {
    var a, b, c, d, e;
    e = this._core.settings;

    for (a in this._handlers) {
      this.$element.off(a, this._handlers[a]);
    }

    for (b in this._controls) {
      "$relative" === b && e.navContainer ? this._controls[b].html("") : this._controls[b].remove();
    }

    for (d in this.overides) {
      this._core[d] = this._overrides[d];
    }

    for (c in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[c] && (this[c] = null);
    }
  }, e.prototype.update = function () {
    var a,
        b,
        c,
        d = this._core.clones().length / 2,
        e = d + this._core.items().length,
        f = this._core.maximum(!0),
        g = this._core.settings,
        h = g.center || g.autoWidth || g.dotsData ? 1 : g.dotsEach || g.items;

    if ("page" !== g.slideBy && (g.slideBy = Math.min(g.slideBy, g.items)), g.dots || "page" == g.slideBy) for (this._pages = [], a = d, b = 0, c = 0; a < e; a++) {
      if (b >= h || 0 === b) {
        if (this._pages.push({
          start: Math.min(f, a - d),
          end: a - d + h - 1
        }), Math.min(f, a - d) === f) break;
        b = 0, ++c;
      }

      b += this._core.mergers(this._core.relative(a));
    }
  }, e.prototype.draw = function () {
    var b,
        c = this._core.settings,
        d = this._core.items().length <= c.items,
        e = this._core.relative(this._core.current()),
        f = c.loop || c.rewind;

    this._controls.$relative.toggleClass("disabled", !c.nav || d), c.nav && (this._controls.$previous.toggleClass("disabled", !f && e <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !f && e >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !c.dots || d), c.dots && (b = this._pages.length - this._controls.$absolute.children().length, c.dotsData && 0 !== b ? this._controls.$absolute.html(this._templates.join("")) : b > 0 ? this._controls.$absolute.append(new Array(b + 1).join(this._templates[0])) : b < 0 && this._controls.$absolute.children().slice(b).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(a.inArray(this.current(), this._pages)).addClass("active"));
  }, e.prototype.onTrigger = function (b) {
    var c = this._core.settings;
    b.page = {
      index: a.inArray(this.current(), this._pages),
      count: this._pages.length,
      size: c && (c.center || c.autoWidth || c.dotsData ? 1 : c.dotsEach || c.items)
    };
  }, e.prototype.current = function () {
    var b = this._core.relative(this._core.current());

    return a.grep(this._pages, a.proxy(function (a, c) {
      return a.start <= b && a.end >= b;
    }, this)).pop();
  }, e.prototype.getPosition = function (b) {
    var c,
        d,
        e = this._core.settings;
    return "page" == e.slideBy ? (c = a.inArray(this.current(), this._pages), d = this._pages.length, b ? ++c : --c, c = this._pages[(c % d + d) % d].start) : (c = this._core.relative(this._core.current()), d = this._core.items().length, b ? c += e.slideBy : c -= e.slideBy), c;
  }, e.prototype.next = function (b) {
    a.proxy(this._overrides.to, this._core)(this.getPosition(!0), b);
  }, e.prototype.prev = function (b) {
    a.proxy(this._overrides.to, this._core)(this.getPosition(!1), b);
  }, e.prototype.to = function (b, c, d) {
    var e;
    !d && this._pages.length ? (e = this._pages.length, a.proxy(this._overrides.to, this._core)(this._pages[(b % e + e) % e].start, c)) : a.proxy(this._overrides.to, this._core)(b, c);
  }, a.fn.owlCarousel.Constructor.Plugins.Navigation = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  "use strict";

  var e = function e(c) {
    this._core = c, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
      "initialized.owl.carousel": a.proxy(function (c) {
        c.namespace && "URLHash" === this._core.settings.startPosition && a(b).trigger("hashchange.owl.navigation");
      }, this),
      "prepared.owl.carousel": a.proxy(function (b) {
        if (b.namespace) {
          var c = a(b.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
          if (!c) return;
          this._hashes[c] = b.content;
        }
      }, this),
      "changed.owl.carousel": a.proxy(function (c) {
        if (c.namespace && "position" === c.property.name) {
          var d = this._core.items(this._core.relative(this._core.current())),
              e = a.map(this._hashes, function (a, b) {
            return a === d ? b : null;
          }).join();

          if (!e || b.location.hash.slice(1) === e) return;
          b.location.hash = e;
        }
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers), a(b).on("hashchange.owl.navigation", a.proxy(function (a) {
      var c = b.location.hash.substring(1),
          e = this._core.$stage.children(),
          f = this._hashes[c] && e.index(this._hashes[c]);

      f !== d && f !== this._core.current() && this._core.to(this._core.relative(f), !1, !0);
    }, this));
  };

  e.Defaults = {
    URLhashListener: !1
  }, e.prototype.destroy = function () {
    var c, d;
    a(b).off("hashchange.owl.navigation");

    for (c in this._handlers) {
      this._core.$element.off(c, this._handlers[c]);
    }

    for (d in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[d] && (this[d] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.Hash = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
  function e(b, c) {
    var e = !1,
        f = b.charAt(0).toUpperCase() + b.slice(1);
    return a.each((b + " " + h.join(f + " ") + f).split(" "), function (a, b) {
      if (g[b] !== d) return e = !c || b, !1;
    }), e;
  }

  function f(a) {
    return e(a, !0);
  }

  var g = a("<support>").get(0).style,
      h = "Webkit Moz O ms".split(" "),
      i = {
    transition: {
      end: {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        transition: "transitionend"
      }
    },
    animation: {
      end: {
        WebkitAnimation: "webkitAnimationEnd",
        MozAnimation: "animationend",
        OAnimation: "oAnimationEnd",
        animation: "animationend"
      }
    }
  },
      j = {
    csstransforms: function csstransforms() {
      return !!e("transform");
    },
    csstransforms3d: function csstransforms3d() {
      return !!e("perspective");
    },
    csstransitions: function csstransitions() {
      return !!e("transition");
    },
    cssanimations: function cssanimations() {
      return !!e("animation");
    }
  };
  j.csstransitions() && (a.support.transition = new String(f("transition")), a.support.transition.end = i.transition.end[a.support.transition]), j.cssanimations() && (a.support.animation = new String(f("animation")), a.support.animation.end = i.animation.end[a.support.animation]), j.csstransforms() && (a.support.transform = new String(f("transform")), a.support.transform3d = j.csstransforms3d());
}(window.Zepto || window.jQuery, window, document);

/***/ }),

/***/ "./storage/app/public/laraassets/js/script.js":
/*!****************************************************!*\
  !*** ./storage/app/public/laraassets/js/script.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  // Open Menu
  var beginAC = 80,
      endAC = 320,
      beginB = 80,
      endB = 320;

  function inAC(s) {
    s.draw('80% - 240', '80%', 0.3, {
      delay: 0.1,
      callback: function callback() {
        inAC2(s);
      }
    });
  }

  function inAC2(s) {
    s.draw('100% - 545', '100% - 305', 0.6, {
      easing: ease.ease('elastic-out', 1, 0.3)
    });
  }

  function inB(s) {
    s.draw(beginB - 60, endB + 60, 0.1, {
      callback: function callback() {
        inB2(s);
      }
    });
  }

  function inB2(s) {
    s.draw(beginB + 120, endB - 120, 0.3, {
      easing: ease.ease('bounce-out', 1, 0.3)
    });
  }
  /* Out animations (to burger icon) */


  function outAC(s) {
    s.draw('90% - 240', '90%', 0.1, {
      easing: ease.ease('elastic-in', 1, 0.3),
      callback: function callback() {
        outAC2(s);
      }
    });
  }

  function outAC2(s) {
    s.draw('20% - 240', '20%', 0.3, {
      callback: function callback() {
        outAC3(s);
      }
    });
  }

  function outAC3(s) {
    s.draw(beginAC, endAC, 0.7, {
      easing: ease.ease('elastic-out', 1, 0.3)
    });
  }

  function outB(s) {
    s.draw(beginB, endB, 0.7, {
      delay: 0.1,
      easing: ease.ease('elastic-out', 2, 0.4)
    });
  }
  /* Awesome burger default */


  var pathA = document.getElementById('pathA'),
      pathB = document.getElementById('pathB'),
      pathC = document.getElementById('pathC'),
      segmentA = new Segment(pathA, beginAC, endAC),
      segmentB = new Segment(pathB, beginB, endB),
      segmentC = new Segment(pathC, beginAC, endAC),
      trigger = document.getElementById('z-button-trigger'),
      toCloseIcon = true,
      wrapper = document.getElementById('z-button-wrapper');

  trigger.onclick = function () {
    isAboutPage = document.getElementById("fp-nav") ? document.getElementById("fp-nav") : undefined;

    if (toCloseIcon) {
      inAC(segmentA);
      inB(segmentB);
      inAC(segmentC);
      wrapper.className = 'z-button-wrapper expanded';

      if (isAboutPage) {
        isAboutPage.style.cssText = "opacity: 0; transition: 1s; visibility: hidden;";
      }
    } else {
      outAC(segmentA);
      outB(segmentB);
      outAC(segmentC);
      wrapper.className = 'z-button-wrapper';

      if (isAboutPage) {
        isAboutPage.style.cssText = "opacity: 100; tansition-property: opacity; transition-duration: 1s; transition-delay: 2s; visibility: visible;";
      }
    }

    toCloseIcon = !toCloseIcon;
  };

  $(function () {
    setTimeout(function () {
      $(".z-button").css('opacity', 1);
    }, 1000);
  }); // Fullpage JS for Home Page

  $('[data-aos]').each(function () {
    $(this).addClass("aos-init");
  });
  $('#fullpage').fullpage({
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
    onLeave: function onLeave(origin, destination, direction) {
      $('.section [data-aos]').each(function () {
        $(this).removeClass("aos-animate");
      });

      if (origin != null) {
        if (origin.index == 0 && direction == 'down') {
          $('body').addClass("has-z-open");
        } else if (origin.index == 1 && direction == 'up') {
          $('body').removeClass("has-z-open");
        }
      }
    },
    afterLoad: function afterLoad() {
      $('.section.active [data-aos]').each(function () {
        $(this).addClass("aos-animate");
      });
    }
  }); // Home Slideshow on hover

  var anchor = $('.slideshow-anchor > .link');
  piece = $('.slideshow-item .piece');
  anchor.hover(function () {
    piece.parent().addClass("on-hover");
  }, function () {
    piece.parent().removeClass("on-hover");
  }); // Fullpage JS for About Page

  if ($(window).width() > 800) {
    $('#aboutPage').fullpage({
      licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
      navigation: true,
      navigationPosition: 'left',
      scrollingSpeed: 600,
      keyboardScrolling: true,
      scrollOverflow: true,
      afterLoad: function afterLoad(origin, destination, direction) {
        $('.section.active [data-aos]').each(function () {
          $(this).addClass("aos-animate");
        });

        if (origin != null) {
          if (origin.index == 1) {
            // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
            var getTransform = function getTransform(el) {
              var results = $('#aboutPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
              if (!results) return [0, 0, 0];
              if (results[1] == '3d') return results.slice(2, 5);
              results.push(0);
              return results.slice(6, 7); // this gets me just the top offset from the array...  
            };

            $('.ozg-client-teaser').find('.aos-init').addClass('aos-animate');
          } else if (origin.index == 2) {
            // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
            var _getTransform = function _getTransform(el) {
              var results = $('#aboutPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
              if (!results) return [0, 0, 0];
              if (results[1] == '3d') return results.slice(2, 5);
              results.push(0);
              return results.slice(6, 7); // this gets me just the top offset from the array...  
            };

            $('.ozg-team-teaser').find('.aos-init').addClass('aos-animate');
          }
        }
      },
      onLeave: function onLeave(origin, destination, direction) {
        $('.section [data-aos]').each(function () {
          $(this).removeClass("aos-animate");
        });

        if (origin != null) {
          if (origin.index == 2 && direction == 'down') {
            $('.ozg-client-teaser').find('.aos-init').removeClass('aos-animate');
          } else if (origin.index == 3 && direction == 'up') {
            $('.ozg-team-teaser').find('.aos-init').removeClass('aos-animate');
            $('.ozg-client-teaser').find('.aos-init').addClass('aos-animate');
          }
        }
      }
    });
    $('#workPage').fullpage({
      licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
      navigation: false,
      scrollOverflow: true,
      afterRender: function afterRender() {
        $('.ozg-work-teaser').find('.aos-init').addClass('aos-animate'); // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td

        function getTransform(el) {
          var results = $('#workPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
          if (!results) return [0, 0, 0];
          if (results[1] == '3d') return results.slice(2, 5);
          results.push(0);
          return results.slice(6, 7); // this gets me just the top offset from the array...  
        }
      },
      afterLoad: function afterLoad(origin, destination, direction) {
        $('.section.active [data-aos]').each(function () {
          $(this).addClass("aos-animate");
        });
      }
    });
    $('#blogPage').fullpage({
      licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
      navigation: false,
      scrollOverflow: true,
      afterRender: function afterRender() {
        $('.ozg-blog-teaser').find('.aos-init').addClass('aos-animate');
      },
      afterLoad: function afterLoad(origin, destination, direction) {
        $('.section.active [data-aos]').each(function () {
          $(this).addClass("aos-animate");
        });
      }
    });
  } else {
    var index = $('.ozg-team-list .point');
    setInterval(function () {
      if (index.hasClass('aos-animate')) {
        if (!$('.ozg-team-list .item:last-child').hasClass('aos-animate')) {
          $('.ozg-team-list .item:last-child').addClass('aos-animate');
        }
      }
    });
    $('.ozg-work-list .item').addClass('aos-animate');
    var delayAos = 0;
    delayInc = 1;
    $('[data-aos]').each(function (index) {
      $(this).removeAttr('data-aos-delay');
      delayAos = delayInc * 100;
      $(this).attr('data-aos-delay', delayAos);
      delayInc++;
    });
    AOS.init({
      duration: 1000,
      once: true,
      startEvent: 'DOMContentLoaded'
    });
  }

  function triggerLandscape() {
    var teaser = $('.ozg-client-teaser'),
        teaserPlace = $('.z-layout');

    if (window.matchMedia("(min-device-width: 800px) and (max-device-width: 812px) and (orientation: landscape)").matches) {
      teaser.detach();
      teaserPlace.append(teaser);
      teaser.addClass('isLandscape');
    }
  }

  triggerLandscape(); // Contact Page

  $('.ozg-contact [data-aos]').each(function () {
    $(this).addClass('aos-animate');
  }); // Slick JS

  $('.slick-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slick-nav',
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev">',
    nextArrow: '<button type="button" class="slick-next">',
    draggable: false,
    autoplay: true
  });
  $('.slick-nav [data-aos]').each(function () {
    var slickNav = $(this);
    slickNav.removeClass("aos-animate");
  });
  $('.slick-nav').on('init', function (init) {
    $('.slick-nav [data-aos]').each(function () {
      var slickNav = $(this);
      slickNav.addClass("aos-animate");
    });
  });
  $('.slick-nav').slick({
    slidesToShow: 3,
    asNavFor: '.slick-carousel',
    centerMode: false,
    dots: false,
    draggable: false,
    focusOnSelect: true,
    useTransform: false
  }); // $(".owl-carousel").owlCarousel({
  //     items: 1,
  //     dots: false,
  //     nav: true,
  //     navText: [],
  //     loop: true,
  //     URLhashListener: false
  // });
  // Anime JS for Contact's heading
  // $('.ozg-contact-heading').each(function(){
  //     $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
  // });
  // anime.timeline({
  //     loop: false
  // })
  // .add({
  //     targets: '.ozg-contact-heading .letter',
  //     translateX: [40,0],
  //     translateZ: 0,
  //     opacity: [0,1],
  //     easing: "easeOutExpo",
  //     duration: 1200,
  //     delay: function(el, i) {
  //         return 500 + 30 * i;
  //     }
  // });
  // Blog Page

  function blogOverlay() {
    var imgHeight = $('.ozg-blog-image-wrapper > img').height(),
        overlay = $('.ozg-blog-image-overlay');
    overlay.css('height', imgHeight);
  }

  blogOverlay();
  $(window).resize(function () {
    triggerLandscape();
    blogOverlay();
  });
});

/***/ }),

/***/ "./storage/app/public/laraassets/js/scrolloverflow.min.js":
/*!****************************************************************!*\
  !*** ./storage/app/public/laraassets/js/scrolloverflow.min.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

/**
* Customized version of iScroll.js 0.0.9
* It fixes bugs affecting its integration with fullpage.js
* @license
*/
!function (t, i, s) {
  var e = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || t.msRequestAnimationFrame || function (i) {
    t.setTimeout(i, 1e3 / 60);
  },
      o = function () {
    var e = {},
        o = i.createElement("div").style,
        n = function () {
      for (var t = ["t", "webkitT", "MozT", "msT", "OT"], i = 0, s = t.length; i < s; i++) {
        if (t[i] + "ransform" in o) return t[i].substr(0, t[i].length - 1);
      }

      return !1;
    }();

    function r(t) {
      return !1 !== n && ("" === n ? t : n + t.charAt(0).toUpperCase() + t.substr(1));
    }

    e.getTime = Date.now || function () {
      return new Date().getTime();
    }, e.extend = function (t, i) {
      for (var s in i) {
        t[s] = i[s];
      }
    }, e.addEvent = function (t, i, s, e) {
      t.addEventListener(i, s, !!e);
    }, e.removeEvent = function (t, i, s, e) {
      t.removeEventListener(i, s, !!e);
    }, e.prefixPointerEvent = function (i) {
      return t.MSPointerEvent ? "MSPointer" + i.charAt(7).toUpperCase() + i.substr(8) : i;
    }, e.momentum = function (t, i, e, o, n, r) {
      var h,
          a,
          l = t - i,
          c = s.abs(l) / e;
      return a = c / (r = void 0 === r ? 6e-4 : r), (h = t + c * c / (2 * r) * (l < 0 ? -1 : 1)) < o ? (h = n ? o - n / 2.5 * (c / 8) : o, a = (l = s.abs(h - t)) / c) : h > 0 && (h = n ? n / 2.5 * (c / 8) : 0, a = (l = s.abs(t) + h) / c), {
        destination: s.round(h),
        duration: a
      };
    };
    var h = r("transform");
    return e.extend(e, {
      hasTransform: !1 !== h,
      hasPerspective: r("perspective") in o,
      hasTouch: "ontouchstart" in t,
      hasPointer: !(!t.PointerEvent && !t.MSPointerEvent),
      hasTransition: r("transition") in o
    }), e.isBadAndroid = function () {
      var i = t.navigator.appVersion;

      if (/Android/.test(i) && !/Chrome\/\d/.test(i)) {
        var s = i.match(/Safari\/(\d+.\d)/);
        return !(s && "object" == _typeof(s) && s.length >= 2) || parseFloat(s[1]) < 535.19;
      }

      return !1;
    }(), e.extend(e.style = {}, {
      transform: h,
      transitionTimingFunction: r("transitionTimingFunction"),
      transitionDuration: r("transitionDuration"),
      transitionDelay: r("transitionDelay"),
      transformOrigin: r("transformOrigin")
    }), e.hasClass = function (t, i) {
      return new RegExp("(^|\\s)" + i + "(\\s|$)").test(t.className);
    }, e.addClass = function (t, i) {
      if (!e.hasClass(t, i)) {
        var s = t.className.split(" ");
        s.push(i), t.className = s.join(" ");
      }
    }, e.removeClass = function (t, i) {
      if (e.hasClass(t, i)) {
        var s = new RegExp("(^|\\s)" + i + "(\\s|$)", "g");
        t.className = t.className.replace(s, " ");
      }
    }, e.offset = function (t) {
      for (var i = -t.offsetLeft, s = -t.offsetTop; t = t.offsetParent;) {
        i -= t.offsetLeft, s -= t.offsetTop;
      }

      return {
        left: i,
        top: s
      };
    }, e.preventDefaultException = function (t, i) {
      for (var s in i) {
        if (i[s].test(t[s])) return !0;
      }

      return !1;
    }, e.extend(e.eventType = {}, {
      touchstart: 1,
      touchmove: 1,
      touchend: 1,
      mousedown: 2,
      mousemove: 2,
      mouseup: 2,
      pointerdown: 3,
      pointermove: 3,
      pointerup: 3,
      MSPointerDown: 3,
      MSPointerMove: 3,
      MSPointerUp: 3
    }), e.extend(e.ease = {}, {
      quadratic: {
        style: "cubic-bezier(0.25, 0.46, 0.45, 0.94)",
        fn: function fn(t) {
          return t * (2 - t);
        }
      },
      circular: {
        style: "cubic-bezier(0.1, 0.57, 0.1, 1)",
        fn: function fn(t) {
          return s.sqrt(1 - --t * t);
        }
      },
      back: {
        style: "cubic-bezier(0.175, 0.885, 0.32, 1.275)",
        fn: function fn(t) {
          return (t -= 1) * t * (5 * t + 4) + 1;
        }
      },
      bounce: {
        style: "",
        fn: function fn(t) {
          return (t /= 1) < 1 / 2.75 ? 7.5625 * t * t : t < 2 / 2.75 ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : t < 2.5 / 2.75 ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375;
        }
      },
      elastic: {
        style: "",
        fn: function fn(t) {
          return 0 === t ? 0 : 1 == t ? 1 : .4 * s.pow(2, -10 * t) * s.sin((t - .055) * (2 * s.PI) / .22) + 1;
        }
      }
    }), e.tap = function (t, s) {
      var e = i.createEvent("Event");
      e.initEvent(s, !0, !0), e.pageX = t.pageX, e.pageY = t.pageY, t.target.dispatchEvent(e);
    }, e.click = function (s) {
      var e,
          o = s.target;
      /(SELECT|INPUT|TEXTAREA)/i.test(o.tagName) || ((e = i.createEvent(t.MouseEvent ? "MouseEvents" : "Event")).initEvent("click", !0, !0), e.view = s.view || t, e.detail = 1, e.screenX = o.screenX || 0, e.screenY = o.screenY || 0, e.clientX = o.clientX || 0, e.clientY = o.clientY || 0, e.ctrlKey = !!s.ctrlKey, e.altKey = !!s.altKey, e.shiftKey = !!s.shiftKey, e.metaKey = !!s.metaKey, e.button = 0, e.relatedTarget = null, e._constructed = !0, o.dispatchEvent(e));
    }, e;
  }();

  function n(s, e) {
    for (var n in this.wrapper = "string" == typeof s ? i.querySelector(s) : s, this.scroller = this.wrapper.children[0], this.scrollerStyle = this.scroller.style, this.options = {
      resizeScrollbars: !0,
      mouseWheelSpeed: 20,
      snapThreshold: .334,
      disablePointer: !o.hasPointer,
      disableTouch: o.hasPointer || !o.hasTouch,
      disableMouse: o.hasPointer || o.hasTouch,
      startX: 0,
      startY: 0,
      scrollY: !0,
      directionLockThreshold: 5,
      momentum: !0,
      bounce: !0,
      bounceTime: 600,
      bounceEasing: "",
      preventDefault: !0,
      preventDefaultException: {
        tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT|LABEL)$/
      },
      HWCompositing: !0,
      useTransition: !0,
      useTransform: !0,
      bindToWrapper: void 0 === t.onmousedown
    }, e) {
      this.options[n] = e[n];
    }

    this.translateZ = this.options.HWCompositing && o.hasPerspective ? " translateZ(0)" : "", this.options.useTransition = o.hasTransition && this.options.useTransition, this.options.useTransform = o.hasTransform && this.options.useTransform, this.options.eventPassthrough = !0 === this.options.eventPassthrough ? "vertical" : this.options.eventPassthrough, this.options.preventDefault = !this.options.eventPassthrough && this.options.preventDefault, this.options.scrollY = "vertical" != this.options.eventPassthrough && this.options.scrollY, this.options.scrollX = "horizontal" != this.options.eventPassthrough && this.options.scrollX, this.options.freeScroll = this.options.freeScroll && !this.options.eventPassthrough, this.options.directionLockThreshold = this.options.eventPassthrough ? 0 : this.options.directionLockThreshold, this.options.bounceEasing = "string" == typeof this.options.bounceEasing ? o.ease[this.options.bounceEasing] || o.ease.circular : this.options.bounceEasing, this.options.resizePolling = void 0 === this.options.resizePolling ? 60 : this.options.resizePolling, !0 === this.options.tap && (this.options.tap = "tap"), this.options.useTransition || this.options.useTransform || /relative|absolute/i.test(this.scrollerStyle.position) || (this.scrollerStyle.position = "relative"), "scale" == this.options.shrinkScrollbars && (this.options.useTransition = !1), this.options.invertWheelDirection = this.options.invertWheelDirection ? -1 : 1, this.x = 0, this.y = 0, this.directionX = 0, this.directionY = 0, this._events = {}, this._init(), this.refresh(), this.scrollTo(this.options.startX, this.options.startY), this.enable();
  }

  function r(t, s, e) {
    var o = i.createElement("div"),
        n = i.createElement("div");
    return !0 === e && (o.style.cssText = "position:absolute;z-index:9999", n.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px"), n.className = "iScrollIndicator", "h" == t ? (!0 === e && (o.style.cssText += ";height:7px;left:2px;right:2px;bottom:0", n.style.height = "100%"), o.className = "iScrollHorizontalScrollbar") : (!0 === e && (o.style.cssText += ";width:7px;bottom:2px;top:2px;right:1px", n.style.width = "100%"), o.className = "iScrollVerticalScrollbar"), o.style.cssText += ";overflow:hidden", s || (o.style.pointerEvents = "none"), o.appendChild(n), o;
  }

  function h(s, n) {
    for (var r in this.wrapper = "string" == typeof n.el ? i.querySelector(n.el) : n.el, this.wrapperStyle = this.wrapper.style, this.indicator = this.wrapper.children[0], this.indicatorStyle = this.indicator.style, this.scroller = s, this.options = {
      listenX: !0,
      listenY: !0,
      interactive: !1,
      resize: !0,
      defaultScrollbars: !1,
      shrink: !1,
      fade: !1,
      speedRatioX: 0,
      speedRatioY: 0
    }, n) {
      this.options[r] = n[r];
    }

    if (this.sizeRatioX = 1, this.sizeRatioY = 1, this.maxPosX = 0, this.maxPosY = 0, this.options.interactive && (this.options.disableTouch || (o.addEvent(this.indicator, "touchstart", this), o.addEvent(t, "touchend", this)), this.options.disablePointer || (o.addEvent(this.indicator, o.prefixPointerEvent("pointerdown"), this), o.addEvent(t, o.prefixPointerEvent("pointerup"), this)), this.options.disableMouse || (o.addEvent(this.indicator, "mousedown", this), o.addEvent(t, "mouseup", this))), this.options.fade) {
      this.wrapperStyle[o.style.transform] = this.scroller.translateZ;
      var h = o.style.transitionDuration;
      if (!h) return;
      this.wrapperStyle[h] = o.isBadAndroid ? "0.0001ms" : "0ms";
      var a = this;
      o.isBadAndroid && e(function () {
        "0.0001ms" === a.wrapperStyle[h] && (a.wrapperStyle[h] = "0s");
      }), this.wrapperStyle.opacity = "0";
    }
  }

  n.prototype = {
    version: "5.2.0",
    _init: function _init() {
      this._initEvents(), (this.options.scrollbars || this.options.indicators) && this._initIndicators(), this.options.mouseWheel && this._initWheel(), this.options.snap && this._initSnap(), this.options.keyBindings && this._initKeys();
    },
    destroy: function destroy() {
      this._initEvents(!0), clearTimeout(this.resizeTimeout), this.resizeTimeout = null, this._execEvent("destroy");
    },
    _transitionEnd: function _transitionEnd(t) {
      t.target == this.scroller && this.isInTransition && (this._transitionTime(), this.resetPosition(this.options.bounceTime) || (this.isInTransition = !1, this._execEvent("scrollEnd")));
    },
    _start: function _start(t) {
      if (1 != o.eventType[t.type] && 0 !== (t.which ? t.button : t.button < 2 ? 0 : 4 == t.button ? 1 : 2)) return;

      if (this.enabled && (!this.initiated || o.eventType[t.type] === this.initiated)) {
        !this.options.preventDefault || o.isBadAndroid || o.preventDefaultException(t.target, this.options.preventDefaultException) || t.preventDefault();
        var i,
            e = t.touches ? t.touches[0] : t;
        this.initiated = o.eventType[t.type], this.moved = !1, this.distX = 0, this.distY = 0, this.directionX = 0, this.directionY = 0, this.directionLocked = 0, this.startTime = o.getTime(), this.options.useTransition && this.isInTransition ? (this._transitionTime(), this.isInTransition = !1, i = this.getComputedPosition(), this._translate(s.round(i.x), s.round(i.y)), this._execEvent("scrollEnd")) : !this.options.useTransition && this.isAnimating && (this.isAnimating = !1, this._execEvent("scrollEnd")), this.startX = this.x, this.startY = this.y, this.absStartX = this.x, this.absStartY = this.y, this.pointX = e.pageX, this.pointY = e.pageY, this._execEvent("beforeScrollStart");
      }
    },
    _move: function _move(t) {
      if (this.enabled && o.eventType[t.type] === this.initiated) {
        this.options.preventDefault && t.preventDefault();
        var i,
            e,
            n,
            r,
            h = t.touches ? t.touches[0] : t,
            a = h.pageX - this.pointX,
            l = h.pageY - this.pointY,
            c = o.getTime();

        if (this.pointX = h.pageX, this.pointY = h.pageY, this.distX += a, this.distY += l, n = s.abs(this.distX), r = s.abs(this.distY), !(c - this.endTime > 300 && n < 10 && r < 10)) {
          if (this.directionLocked || this.options.freeScroll || (n > r + this.options.directionLockThreshold ? this.directionLocked = "h" : r >= n + this.options.directionLockThreshold ? this.directionLocked = "v" : this.directionLocked = "n"), "h" == this.directionLocked) {
            if ("vertical" == this.options.eventPassthrough) t.preventDefault();else if ("horizontal" == this.options.eventPassthrough) return void (this.initiated = !1);
            l = 0;
          } else if ("v" == this.directionLocked) {
            if ("horizontal" == this.options.eventPassthrough) t.preventDefault();else if ("vertical" == this.options.eventPassthrough) return void (this.initiated = !1);
            a = 0;
          }

          a = this.hasHorizontalScroll ? a : 0, l = this.hasVerticalScroll ? l : 0, i = this.x + a, e = this.y + l, (i > 0 || i < this.maxScrollX) && (i = this.options.bounce ? this.x + a / 3 : i > 0 ? 0 : this.maxScrollX), (e > 0 || e < this.maxScrollY) && (e = this.options.bounce ? this.y + l / 3 : e > 0 ? 0 : this.maxScrollY), this.directionX = a > 0 ? -1 : a < 0 ? 1 : 0, this.directionY = l > 0 ? -1 : l < 0 ? 1 : 0, this.moved || this._execEvent("scrollStart"), this.moved = !0, this._translate(i, e), c - this.startTime > 300 && (this.startTime = c, this.startX = this.x, this.startY = this.y);
        }
      }
    },
    _end: function _end(t) {
      if (this.enabled && o.eventType[t.type] === this.initiated) {
        this.options.preventDefault && !o.preventDefaultException(t.target, this.options.preventDefaultException) && t.preventDefault();
        t.changedTouches && t.changedTouches[0];
        var i,
            e,
            n = o.getTime() - this.startTime,
            r = s.round(this.x),
            h = s.round(this.y),
            a = s.abs(r - this.startX),
            l = s.abs(h - this.startY),
            c = 0,
            p = "";

        if (this.isInTransition = 0, this.initiated = 0, this.endTime = o.getTime(), !this.resetPosition(this.options.bounceTime)) {
          if (this.scrollTo(r, h), !this.moved) return this.options.tap && o.tap(t, this.options.tap), this.options.click && o.click(t), void this._execEvent("scrollCancel");
          if (this._events.flick && n < 200 && a < 100 && l < 100) this._execEvent("flick");else {
            if (this.options.momentum && n < 300 && (i = this.hasHorizontalScroll ? o.momentum(this.x, this.startX, n, this.maxScrollX, this.options.bounce ? this.wrapperWidth : 0, this.options.deceleration) : {
              destination: r,
              duration: 0
            }, e = this.hasVerticalScroll ? o.momentum(this.y, this.startY, n, this.maxScrollY, this.options.bounce ? this.wrapperHeight : 0, this.options.deceleration) : {
              destination: h,
              duration: 0
            }, r = i.destination, h = e.destination, c = s.max(i.duration, e.duration), this.isInTransition = 1), this.options.snap) {
              var d = this._nearestSnap(r, h);

              this.currentPage = d, c = this.options.snapSpeed || s.max(s.max(s.min(s.abs(r - d.x), 1e3), s.min(s.abs(h - d.y), 1e3)), 300), r = d.x, h = d.y, this.directionX = 0, this.directionY = 0, p = this.options.bounceEasing;
            }

            if (r != this.x || h != this.y) return (r > 0 || r < this.maxScrollX || h > 0 || h < this.maxScrollY) && (p = o.ease.quadratic), void this.scrollTo(r, h, c, p);

            this._execEvent("scrollEnd");
          }
        }
      }
    },
    _resize: function _resize() {
      var t = this;
      clearTimeout(this.resizeTimeout), this.resizeTimeout = setTimeout(function () {
        t.refresh();
      }, this.options.resizePolling);
    },
    resetPosition: function resetPosition(t) {
      var i = this.x,
          s = this.y;
      return t = t || 0, !this.hasHorizontalScroll || this.x > 0 ? i = 0 : this.x < this.maxScrollX && (i = this.maxScrollX), !this.hasVerticalScroll || this.y > 0 ? s = 0 : this.y < this.maxScrollY && (s = this.maxScrollY), (i != this.x || s != this.y) && (this.scrollTo(i, s, t, this.options.bounceEasing), !0);
    },
    disable: function disable() {
      this.enabled = !1;
    },
    enable: function enable() {
      this.enabled = !0;
    },
    refresh: function refresh() {
      this.wrapper.offsetHeight;
      this.wrapperWidth = this.wrapper.clientWidth, this.wrapperHeight = this.wrapper.clientHeight, this.scrollerWidth = this.scroller.offsetWidth, this.scrollerHeight = this.scroller.offsetHeight, this.maxScrollX = this.wrapperWidth - this.scrollerWidth, this.maxScrollY = this.wrapperHeight - this.scrollerHeight, this.hasHorizontalScroll = this.options.scrollX && this.maxScrollX < 0, this.hasVerticalScroll = this.options.scrollY && this.maxScrollY < 0, this.hasHorizontalScroll || (this.maxScrollX = 0, this.scrollerWidth = this.wrapperWidth), this.hasVerticalScroll || (this.maxScrollY = 0, this.scrollerHeight = this.wrapperHeight), this.endTime = 0, this.directionX = 0, this.directionY = 0, this.wrapperOffset = o.offset(this.wrapper), this._execEvent("refresh"), this.resetPosition();
    },
    on: function on(t, i) {
      this._events[t] || (this._events[t] = []), this._events[t].push(i);
    },
    off: function off(t, i) {
      if (this._events[t]) {
        var s = this._events[t].indexOf(i);

        s > -1 && this._events[t].splice(s, 1);
      }
    },
    _execEvent: function _execEvent(t) {
      if (this._events[t]) {
        var i = 0,
            s = this._events[t].length;
        if (s) for (; i < s; i++) {
          this._events[t][i].apply(this, [].slice.call(arguments, 1));
        }
      }
    },
    scrollBy: function scrollBy(t, i, s, e) {
      t = this.x + t, i = this.y + i, s = s || 0, this.scrollTo(t, i, s, e);
    },
    scrollTo: function scrollTo(t, i, s, e) {
      e = e || o.ease.circular, this.isInTransition = this.options.useTransition && s > 0;
      var n = this.options.useTransition && e.style;
      !s || n ? (n && (this._transitionTimingFunction(e.style), this._transitionTime(s)), this._translate(t, i)) : this._animate(t, i, s, e.fn);
    },
    scrollToElement: function scrollToElement(t, i, e, n, r) {
      if (t = t.nodeType ? t : this.scroller.querySelector(t)) {
        var h = o.offset(t);
        h.left -= this.wrapperOffset.left, h.top -= this.wrapperOffset.top, !0 === e && (e = s.round(t.offsetWidth / 2 - this.wrapper.offsetWidth / 2)), !0 === n && (n = s.round(t.offsetHeight / 2 - this.wrapper.offsetHeight / 2)), h.left -= e || 0, h.top -= n || 0, h.left = h.left > 0 ? 0 : h.left < this.maxScrollX ? this.maxScrollX : h.left, h.top = h.top > 0 ? 0 : h.top < this.maxScrollY ? this.maxScrollY : h.top, i = null == i || "auto" === i ? s.max(s.abs(this.x - h.left), s.abs(this.y - h.top)) : i, this.scrollTo(h.left, h.top, i, r);
      }
    },
    _transitionTime: function _transitionTime(t) {
      if (this.options.useTransition) {
        t = t || 0;
        var i = o.style.transitionDuration;

        if (i) {
          if (this.scrollerStyle[i] = t + "ms", !t && o.isBadAndroid) {
            this.scrollerStyle[i] = "0.0001ms";
            var s = this;
            e(function () {
              "0.0001ms" === s.scrollerStyle[i] && (s.scrollerStyle[i] = "0s");
            });
          }

          if (this.indicators) for (var n = this.indicators.length; n--;) {
            this.indicators[n].transitionTime(t);
          }
        }
      }
    },
    _transitionTimingFunction: function _transitionTimingFunction(t) {
      if (this.scrollerStyle[o.style.transitionTimingFunction] = t, this.indicators) for (var i = this.indicators.length; i--;) {
        this.indicators[i].transitionTimingFunction(t);
      }
    },
    _translate: function _translate(t, i) {
      if (this.options.useTransform ? this.scrollerStyle[o.style.transform] = "translate(" + t + "px," + i + "px)" + this.translateZ : (t = s.round(t), i = s.round(i), this.scrollerStyle.left = t + "px", this.scrollerStyle.top = i + "px"), this.x = t, this.y = i, this.indicators) for (var e = this.indicators.length; e--;) {
        this.indicators[e].updatePosition();
      }
    },
    _initEvents: function _initEvents(i) {
      var s = i ? o.removeEvent : o.addEvent,
          e = this.options.bindToWrapper ? this.wrapper : t;
      s(t, "orientationchange", this), s(t, "resize", this), this.options.click && s(this.wrapper, "click", this, !0), this.options.disableMouse || (s(this.wrapper, "mousedown", this), s(e, "mousemove", this), s(e, "mousecancel", this), s(e, "mouseup", this)), o.hasPointer && !this.options.disablePointer && (s(this.wrapper, o.prefixPointerEvent("pointerdown"), this), s(e, o.prefixPointerEvent("pointermove"), this), s(e, o.prefixPointerEvent("pointercancel"), this), s(e, o.prefixPointerEvent("pointerup"), this)), o.hasTouch && !this.options.disableTouch && (s(this.wrapper, "touchstart", this), s(e, "touchmove", this), s(e, "touchcancel", this), s(e, "touchend", this)), s(this.scroller, "transitionend", this), s(this.scroller, "webkitTransitionEnd", this), s(this.scroller, "oTransitionEnd", this), s(this.scroller, "MSTransitionEnd", this);
    },
    getComputedPosition: function getComputedPosition() {
      var i,
          s,
          e = t.getComputedStyle(this.scroller, null);
      return this.options.useTransform ? (i = +((e = e[o.style.transform].split(")")[0].split(", "))[12] || e[4]), s = +(e[13] || e[5])) : (i = +e.left.replace(/[^-\d.]/g, ""), s = +e.top.replace(/[^-\d.]/g, "")), {
        x: i,
        y: s
      };
    },
    _initIndicators: function _initIndicators() {
      var t,
          i = this.options.interactiveScrollbars,
          s = "string" != typeof this.options.scrollbars,
          e = [],
          o = this;
      this.indicators = [], this.options.scrollbars && (this.options.scrollY && (t = {
        el: r("v", i, this.options.scrollbars),
        interactive: i,
        defaultScrollbars: !0,
        customStyle: s,
        resize: this.options.resizeScrollbars,
        shrink: this.options.shrinkScrollbars,
        fade: this.options.fadeScrollbars,
        listenX: !1
      }, this.wrapper.appendChild(t.el), e.push(t)), this.options.scrollX && (t = {
        el: r("h", i, this.options.scrollbars),
        interactive: i,
        defaultScrollbars: !0,
        customStyle: s,
        resize: this.options.resizeScrollbars,
        shrink: this.options.shrinkScrollbars,
        fade: this.options.fadeScrollbars,
        listenY: !1
      }, this.wrapper.appendChild(t.el), e.push(t))), this.options.indicators && (e = e.concat(this.options.indicators));

      for (var n = e.length; n--;) {
        this.indicators.push(new h(this, e[n]));
      }

      function a(t) {
        if (o.indicators) for (var i = o.indicators.length; i--;) {
          t.call(o.indicators[i]);
        }
      }

      this.options.fadeScrollbars && (this.on("scrollEnd", function () {
        a(function () {
          this.fade();
        });
      }), this.on("scrollCancel", function () {
        a(function () {
          this.fade();
        });
      }), this.on("scrollStart", function () {
        a(function () {
          this.fade(1);
        });
      }), this.on("beforeScrollStart", function () {
        a(function () {
          this.fade(1, !0);
        });
      })), this.on("refresh", function () {
        a(function () {
          this.refresh();
        });
      }), this.on("destroy", function () {
        a(function () {
          this.destroy();
        }), delete this.indicators;
      });
    },
    _initWheel: function _initWheel() {
      o.addEvent(this.wrapper, "wheel", this), o.addEvent(this.wrapper, "mousewheel", this), o.addEvent(this.wrapper, "DOMMouseScroll", this), this.on("destroy", function () {
        clearTimeout(this.wheelTimeout), this.wheelTimeout = null, o.removeEvent(this.wrapper, "wheel", this), o.removeEvent(this.wrapper, "mousewheel", this), o.removeEvent(this.wrapper, "DOMMouseScroll", this);
      });
    },
    _wheel: function _wheel(t) {
      if (this.enabled) {
        var i,
            e,
            o,
            n,
            r = this;
        if (void 0 === this.wheelTimeout && r._execEvent("scrollStart"), clearTimeout(this.wheelTimeout), this.wheelTimeout = setTimeout(function () {
          r.options.snap || r._execEvent("scrollEnd"), r.wheelTimeout = void 0;
        }, 400), "deltaX" in t) 1 === t.deltaMode ? (i = -t.deltaX * this.options.mouseWheelSpeed, e = -t.deltaY * this.options.mouseWheelSpeed) : (i = -t.deltaX, e = -t.deltaY);else if ("wheelDeltaX" in t) i = t.wheelDeltaX / 120 * this.options.mouseWheelSpeed, e = t.wheelDeltaY / 120 * this.options.mouseWheelSpeed;else if ("wheelDelta" in t) i = e = t.wheelDelta / 120 * this.options.mouseWheelSpeed;else {
          if (!("detail" in t)) return;
          i = e = -t.detail / 3 * this.options.mouseWheelSpeed;
        }
        if (i *= this.options.invertWheelDirection, e *= this.options.invertWheelDirection, this.hasVerticalScroll || (i = e, e = 0), this.options.snap) return o = this.currentPage.pageX, n = this.currentPage.pageY, i > 0 ? o-- : i < 0 && o++, e > 0 ? n-- : e < 0 && n++, void this.goToPage(o, n);
        o = this.x + s.round(this.hasHorizontalScroll ? i : 0), n = this.y + s.round(this.hasVerticalScroll ? e : 0), this.directionX = i > 0 ? -1 : i < 0 ? 1 : 0, this.directionY = e > 0 ? -1 : e < 0 ? 1 : 0, o > 0 ? o = 0 : o < this.maxScrollX && (o = this.maxScrollX), n > 0 ? n = 0 : n < this.maxScrollY && (n = this.maxScrollY), this.scrollTo(o, n, 0);
      }
    },
    _initSnap: function _initSnap() {
      this.currentPage = {}, "string" == typeof this.options.snap && (this.options.snap = this.scroller.querySelectorAll(this.options.snap)), this.on("refresh", function () {
        var t,
            i,
            e,
            o,
            n,
            r,
            h = 0,
            a = 0,
            l = 0,
            c = this.options.snapStepX || this.wrapperWidth,
            p = this.options.snapStepY || this.wrapperHeight;

        if (this.pages = [], this.wrapperWidth && this.wrapperHeight && this.scrollerWidth && this.scrollerHeight) {
          if (!0 === this.options.snap) for (e = s.round(c / 2), o = s.round(p / 2); l > -this.scrollerWidth;) {
            for (this.pages[h] = [], t = 0, n = 0; n > -this.scrollerHeight;) {
              this.pages[h][t] = {
                x: s.max(l, this.maxScrollX),
                y: s.max(n, this.maxScrollY),
                width: c,
                height: p,
                cx: l - e,
                cy: n - o
              }, n -= p, t++;
            }

            l -= c, h++;
          } else for (t = (r = this.options.snap).length, i = -1; h < t; h++) {
            (0 === h || r[h].offsetLeft <= r[h - 1].offsetLeft) && (a = 0, i++), this.pages[a] || (this.pages[a] = []), l = s.max(-r[h].offsetLeft, this.maxScrollX), n = s.max(-r[h].offsetTop, this.maxScrollY), e = l - s.round(r[h].offsetWidth / 2), o = n - s.round(r[h].offsetHeight / 2), this.pages[a][i] = {
              x: l,
              y: n,
              width: r[h].offsetWidth,
              height: r[h].offsetHeight,
              cx: e,
              cy: o
            }, l > this.maxScrollX && a++;
          }
          this.goToPage(this.currentPage.pageX || 0, this.currentPage.pageY || 0, 0), this.options.snapThreshold % 1 == 0 ? (this.snapThresholdX = this.options.snapThreshold, this.snapThresholdY = this.options.snapThreshold) : (this.snapThresholdX = s.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width * this.options.snapThreshold), this.snapThresholdY = s.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height * this.options.snapThreshold));
        }
      }), this.on("flick", function () {
        var t = this.options.snapSpeed || s.max(s.max(s.min(s.abs(this.x - this.startX), 1e3), s.min(s.abs(this.y - this.startY), 1e3)), 300);
        this.goToPage(this.currentPage.pageX + this.directionX, this.currentPage.pageY + this.directionY, t);
      });
    },
    _nearestSnap: function _nearestSnap(t, i) {
      if (!this.pages.length) return {
        x: 0,
        y: 0,
        pageX: 0,
        pageY: 0
      };
      var e = 0,
          o = this.pages.length,
          n = 0;
      if (s.abs(t - this.absStartX) < this.snapThresholdX && s.abs(i - this.absStartY) < this.snapThresholdY) return this.currentPage;

      for (t > 0 ? t = 0 : t < this.maxScrollX && (t = this.maxScrollX), i > 0 ? i = 0 : i < this.maxScrollY && (i = this.maxScrollY); e < o; e++) {
        if (t >= this.pages[e][0].cx) {
          t = this.pages[e][0].x;
          break;
        }
      }

      for (o = this.pages[e].length; n < o; n++) {
        if (i >= this.pages[0][n].cy) {
          i = this.pages[0][n].y;
          break;
        }
      }

      return e == this.currentPage.pageX && ((e += this.directionX) < 0 ? e = 0 : e >= this.pages.length && (e = this.pages.length - 1), t = this.pages[e][0].x), n == this.currentPage.pageY && ((n += this.directionY) < 0 ? n = 0 : n >= this.pages[0].length && (n = this.pages[0].length - 1), i = this.pages[0][n].y), {
        x: t,
        y: i,
        pageX: e,
        pageY: n
      };
    },
    goToPage: function goToPage(t, i, e, o) {
      o = o || this.options.bounceEasing, t >= this.pages.length ? t = this.pages.length - 1 : t < 0 && (t = 0), i >= this.pages[t].length ? i = this.pages[t].length - 1 : i < 0 && (i = 0);
      var n = this.pages[t][i].x,
          r = this.pages[t][i].y;
      e = void 0 === e ? this.options.snapSpeed || s.max(s.max(s.min(s.abs(n - this.x), 1e3), s.min(s.abs(r - this.y), 1e3)), 300) : e, this.currentPage = {
        x: n,
        y: r,
        pageX: t,
        pageY: i
      }, this.scrollTo(n, r, e, o);
    },
    next: function next(t, i) {
      var s = this.currentPage.pageX,
          e = this.currentPage.pageY;
      ++s >= this.pages.length && this.hasVerticalScroll && (s = 0, e++), this.goToPage(s, e, t, i);
    },
    prev: function prev(t, i) {
      var s = this.currentPage.pageX,
          e = this.currentPage.pageY;
      --s < 0 && this.hasVerticalScroll && (s = 0, e--), this.goToPage(s, e, t, i);
    },
    _initKeys: function _initKeys(i) {
      var s,
          e = {
        pageUp: 33,
        pageDown: 34,
        end: 35,
        home: 36,
        left: 37,
        up: 38,
        right: 39,
        down: 40
      };
      if ("object" == _typeof(this.options.keyBindings)) for (s in this.options.keyBindings) {
        "string" == typeof this.options.keyBindings[s] && (this.options.keyBindings[s] = this.options.keyBindings[s].toUpperCase().charCodeAt(0));
      } else this.options.keyBindings = {};

      for (s in e) {
        this.options.keyBindings[s] = this.options.keyBindings[s] || e[s];
      }

      o.addEvent(t, "keydown", this), this.on("destroy", function () {
        o.removeEvent(t, "keydown", this);
      });
    },
    _key: function _key(t) {
      if (this.enabled) {
        var i,
            e = this.options.snap,
            n = e ? this.currentPage.pageX : this.x,
            r = e ? this.currentPage.pageY : this.y,
            h = o.getTime(),
            a = this.keyTime || 0;

        switch (this.options.useTransition && this.isInTransition && (i = this.getComputedPosition(), this._translate(s.round(i.x), s.round(i.y)), this.isInTransition = !1), this.keyAcceleration = h - a < 200 ? s.min(this.keyAcceleration + .25, 50) : 0, t.keyCode) {
          case this.options.keyBindings.pageUp:
            this.hasHorizontalScroll && !this.hasVerticalScroll ? n += e ? 1 : this.wrapperWidth : r += e ? 1 : this.wrapperHeight;
            break;

          case this.options.keyBindings.pageDown:
            this.hasHorizontalScroll && !this.hasVerticalScroll ? n -= e ? 1 : this.wrapperWidth : r -= e ? 1 : this.wrapperHeight;
            break;

          case this.options.keyBindings.end:
            n = e ? this.pages.length - 1 : this.maxScrollX, r = e ? this.pages[0].length - 1 : this.maxScrollY;
            break;

          case this.options.keyBindings.home:
            n = 0, r = 0;
            break;

          case this.options.keyBindings.left:
            n += e ? -1 : 5 + this.keyAcceleration >> 0;
            break;

          case this.options.keyBindings.up:
            r += e ? 1 : 5 + this.keyAcceleration >> 0;
            break;

          case this.options.keyBindings.right:
            n -= e ? -1 : 5 + this.keyAcceleration >> 0;
            break;

          case this.options.keyBindings.down:
            r -= e ? 1 : 5 + this.keyAcceleration >> 0;
            break;

          default:
            return;
        }

        e ? this.goToPage(n, r) : (n > 0 ? (n = 0, this.keyAcceleration = 0) : n < this.maxScrollX && (n = this.maxScrollX, this.keyAcceleration = 0), r > 0 ? (r = 0, this.keyAcceleration = 0) : r < this.maxScrollY && (r = this.maxScrollY, this.keyAcceleration = 0), this.scrollTo(n, r, 0), this.keyTime = h);
      }
    },
    _animate: function _animate(t, i, s, n) {
      var r = this,
          h = this.x,
          a = this.y,
          l = o.getTime(),
          c = l + s;
      this.isAnimating = !0, function p() {
        var d,
            u,
            f,
            m = o.getTime();
        if (m >= c) return r.isAnimating = !1, r._translate(t, i), void (r.resetPosition(r.options.bounceTime) || r._execEvent("scrollEnd"));
        f = n(m = (m - l) / s), d = (t - h) * f + h, u = (i - a) * f + a, r._translate(d, u), r.isAnimating && e(p);
      }();
    },
    handleEvent: function handleEvent(t) {
      switch (t.type) {
        case "touchstart":
        case "pointerdown":
        case "MSPointerDown":
        case "mousedown":
          this._start(t);

          break;

        case "touchmove":
        case "pointermove":
        case "MSPointerMove":
        case "mousemove":
          this._move(t);

          break;

        case "touchend":
        case "pointerup":
        case "MSPointerUp":
        case "mouseup":
        case "touchcancel":
        case "pointercancel":
        case "MSPointerCancel":
        case "mousecancel":
          this._end(t);

          break;

        case "orientationchange":
        case "resize":
          this._resize();

          break;

        case "transitionend":
        case "webkitTransitionEnd":
        case "oTransitionEnd":
        case "MSTransitionEnd":
          this._transitionEnd(t);

          break;

        case "wheel":
        case "DOMMouseScroll":
        case "mousewheel":
          this._wheel(t);

          break;

        case "keydown":
          this._key(t);

          break;

        case "click":
          this.enabled && !t._constructed && (t.preventDefault(), t.stopPropagation());
      }
    }
  }, h.prototype = {
    handleEvent: function handleEvent(t) {
      switch (t.type) {
        case "touchstart":
        case "pointerdown":
        case "MSPointerDown":
        case "mousedown":
          this._start(t);

          break;

        case "touchmove":
        case "pointermove":
        case "MSPointerMove":
        case "mousemove":
          this._move(t);

          break;

        case "touchend":
        case "pointerup":
        case "MSPointerUp":
        case "mouseup":
        case "touchcancel":
        case "pointercancel":
        case "MSPointerCancel":
        case "mousecancel":
          this._end(t);

      }
    },
    destroy: function destroy() {
      this.options.fadeScrollbars && (clearTimeout(this.fadeTimeout), this.fadeTimeout = null), this.options.interactive && (o.removeEvent(this.indicator, "touchstart", this), o.removeEvent(this.indicator, o.prefixPointerEvent("pointerdown"), this), o.removeEvent(this.indicator, "mousedown", this), o.removeEvent(t, "touchmove", this), o.removeEvent(t, o.prefixPointerEvent("pointermove"), this), o.removeEvent(t, "mousemove", this), o.removeEvent(t, "touchend", this), o.removeEvent(t, o.prefixPointerEvent("pointerup"), this), o.removeEvent(t, "mouseup", this)), this.options.defaultScrollbars && this.wrapper.parentNode.removeChild(this.wrapper);
    },
    _start: function _start(i) {
      var s = i.touches ? i.touches[0] : i;
      i.preventDefault(), i.stopPropagation(), this.transitionTime(), this.initiated = !0, this.moved = !1, this.lastPointX = s.pageX, this.lastPointY = s.pageY, this.startTime = o.getTime(), this.options.disableTouch || o.addEvent(t, "touchmove", this), this.options.disablePointer || o.addEvent(t, o.prefixPointerEvent("pointermove"), this), this.options.disableMouse || o.addEvent(t, "mousemove", this), this.scroller._execEvent("beforeScrollStart");
    },
    _move: function _move(t) {
      var i,
          s,
          e,
          n,
          r = t.touches ? t.touches[0] : t;
      o.getTime();
      this.moved || this.scroller._execEvent("scrollStart"), this.moved = !0, i = r.pageX - this.lastPointX, this.lastPointX = r.pageX, s = r.pageY - this.lastPointY, this.lastPointY = r.pageY, e = this.x + i, n = this.y + s, this._pos(e, n), t.preventDefault(), t.stopPropagation();
    },
    _end: function _end(i) {
      if (this.initiated) {
        if (this.initiated = !1, i.preventDefault(), i.stopPropagation(), o.removeEvent(t, "touchmove", this), o.removeEvent(t, o.prefixPointerEvent("pointermove"), this), o.removeEvent(t, "mousemove", this), this.scroller.options.snap) {
          var e = this.scroller._nearestSnap(this.scroller.x, this.scroller.y),
              n = this.options.snapSpeed || s.max(s.max(s.min(s.abs(this.scroller.x - e.x), 1e3), s.min(s.abs(this.scroller.y - e.y), 1e3)), 300);

          this.scroller.x == e.x && this.scroller.y == e.y || (this.scroller.directionX = 0, this.scroller.directionY = 0, this.scroller.currentPage = e, this.scroller.scrollTo(e.x, e.y, n, this.scroller.options.bounceEasing));
        }

        this.moved && this.scroller._execEvent("scrollEnd");
      }
    },
    transitionTime: function transitionTime(t) {
      t = t || 0;
      var i = o.style.transitionDuration;

      if (i && (this.indicatorStyle[i] = t + "ms", !t && o.isBadAndroid)) {
        this.indicatorStyle[i] = "0.0001ms";
        var s = this;
        e(function () {
          "0.0001ms" === s.indicatorStyle[i] && (s.indicatorStyle[i] = "0s");
        });
      }
    },
    transitionTimingFunction: function transitionTimingFunction(t) {
      this.indicatorStyle[o.style.transitionTimingFunction] = t;
    },
    refresh: function refresh() {
      this.transitionTime(), this.options.listenX && !this.options.listenY ? this.indicatorStyle.display = this.scroller.hasHorizontalScroll ? "block" : "none" : this.options.listenY && !this.options.listenX ? this.indicatorStyle.display = this.scroller.hasVerticalScroll ? "block" : "none" : this.indicatorStyle.display = this.scroller.hasHorizontalScroll || this.scroller.hasVerticalScroll ? "block" : "none", this.scroller.hasHorizontalScroll && this.scroller.hasVerticalScroll ? (o.addClass(this.wrapper, "iScrollBothScrollbars"), o.removeClass(this.wrapper, "iScrollLoneScrollbar"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = "8px" : this.wrapper.style.bottom = "8px")) : (o.removeClass(this.wrapper, "iScrollBothScrollbars"), o.addClass(this.wrapper, "iScrollLoneScrollbar"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = "2px" : this.wrapper.style.bottom = "2px"));
      this.wrapper.offsetHeight;
      this.options.listenX && (this.wrapperWidth = this.wrapper.clientWidth, this.options.resize ? (this.indicatorWidth = s.max(s.round(this.wrapperWidth * this.wrapperWidth / (this.scroller.scrollerWidth || this.wrapperWidth || 1)), 8), this.indicatorStyle.width = this.indicatorWidth + "px") : this.indicatorWidth = this.indicator.clientWidth, this.maxPosX = this.wrapperWidth - this.indicatorWidth, "clip" == this.options.shrink ? (this.minBoundaryX = 8 - this.indicatorWidth, this.maxBoundaryX = this.wrapperWidth - 8) : (this.minBoundaryX = 0, this.maxBoundaryX = this.maxPosX), this.sizeRatioX = this.options.speedRatioX || this.scroller.maxScrollX && this.maxPosX / this.scroller.maxScrollX), this.options.listenY && (this.wrapperHeight = this.wrapper.clientHeight, this.options.resize ? (this.indicatorHeight = s.max(s.round(this.wrapperHeight * this.wrapperHeight / (this.scroller.scrollerHeight || this.wrapperHeight || 1)), 8), this.indicatorStyle.height = this.indicatorHeight + "px") : this.indicatorHeight = this.indicator.clientHeight, this.maxPosY = this.wrapperHeight - this.indicatorHeight, "clip" == this.options.shrink ? (this.minBoundaryY = 8 - this.indicatorHeight, this.maxBoundaryY = this.wrapperHeight - 8) : (this.minBoundaryY = 0, this.maxBoundaryY = this.maxPosY), this.maxPosY = this.wrapperHeight - this.indicatorHeight, this.sizeRatioY = this.options.speedRatioY || this.scroller.maxScrollY && this.maxPosY / this.scroller.maxScrollY), this.updatePosition();
    },
    updatePosition: function updatePosition() {
      var t = this.options.listenX && s.round(this.sizeRatioX * this.scroller.x) || 0,
          i = this.options.listenY && s.round(this.sizeRatioY * this.scroller.y) || 0;
      this.options.ignoreBoundaries || (t < this.minBoundaryX ? ("scale" == this.options.shrink && (this.width = s.max(this.indicatorWidth + t, 8), this.indicatorStyle.width = this.width + "px"), t = this.minBoundaryX) : t > this.maxBoundaryX ? "scale" == this.options.shrink ? (this.width = s.max(this.indicatorWidth - (t - this.maxPosX), 8), this.indicatorStyle.width = this.width + "px", t = this.maxPosX + this.indicatorWidth - this.width) : t = this.maxBoundaryX : "scale" == this.options.shrink && this.width != this.indicatorWidth && (this.width = this.indicatorWidth, this.indicatorStyle.width = this.width + "px"), i < this.minBoundaryY ? ("scale" == this.options.shrink && (this.height = s.max(this.indicatorHeight + 3 * i, 8), this.indicatorStyle.height = this.height + "px"), i = this.minBoundaryY) : i > this.maxBoundaryY ? "scale" == this.options.shrink ? (this.height = s.max(this.indicatorHeight - 3 * (i - this.maxPosY), 8), this.indicatorStyle.height = this.height + "px", i = this.maxPosY + this.indicatorHeight - this.height) : i = this.maxBoundaryY : "scale" == this.options.shrink && this.height != this.indicatorHeight && (this.height = this.indicatorHeight, this.indicatorStyle.height = this.height + "px")), this.x = t, this.y = i, this.scroller.options.useTransform ? this.indicatorStyle[o.style.transform] = "translate(" + t + "px," + i + "px)" + this.scroller.translateZ : (this.indicatorStyle.left = t + "px", this.indicatorStyle.top = i + "px");
    },
    _pos: function _pos(t, i) {
      t < 0 ? t = 0 : t > this.maxPosX && (t = this.maxPosX), i < 0 ? i = 0 : i > this.maxPosY && (i = this.maxPosY), t = this.options.listenX ? s.round(t / this.sizeRatioX) : this.scroller.x, i = this.options.listenY ? s.round(i / this.sizeRatioY) : this.scroller.y, this.scroller.scrollTo(t, i);
    },
    fade: function fade(t, i) {
      if (!i || this.visible) {
        clearTimeout(this.fadeTimeout), this.fadeTimeout = null;
        var s = t ? 250 : 500,
            e = t ? 0 : 300;
        t = t ? "1" : "0", this.wrapperStyle[o.style.transitionDuration] = s + "ms", this.fadeTimeout = setTimeout(function (t) {
          this.wrapperStyle.opacity = t, this.visible = +t;
        }.bind(this, t), e);
      }
    }
  }, n.utils = o,  true && module.exports ? module.exports = n :  true ? !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
    return n;
  }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(window, document, Math),
/*!
* Scrolloverflow 2.0.0 module for fullPage.js >= 3
* https://github.com/alvarotrigo/fullPage.js
* @license MIT licensed
*
* Copyright (C) 2015 alvarotrigo.com - A project by Alvaro Trigo
*/
function (t, i) {
  t.fp_scrolloverflow = function () {
    t.IScroll || (IScroll = module.exports);
    var s = "fp-scrollable",
        e = "." + s,
        o = ".active",
        n = ".fp-section",
        r = n + o,
        h = ".fp-slide",
        a = h + o,
        l = ".fp-tableCell",
        c = "fp-responsive",
        p = "fp-auto-height-responsive";

    function d(t) {
      var i = fp_utils.closest(t, n);
      return null != i ? parseInt(getComputedStyle(i)["padding-bottom"]) + parseInt(getComputedStyle(i)["padding-top"]) : 0;
    }

    function u() {
      var s = this;

      function e() {
        var t;
        fp_utils.hasClass(i.body, c) ? (t = s.options.scrollOverflowHandler, r(function (i) {
          fp_utils.hasClass(fp_utils.closest(i, n), p) && t.remove(i);
        })) : r(o);
      }

      function o(t) {
        if (!fp_utils.hasClass(t, "fp-noscroll")) {
          fp_utils.css(t, {
            overflow: "hidden"
          });
          var i,
              e = s.options.scrollOverflowHandler,
              o = e.wrapContent(),
              r = fp_utils.closest(t, n),
              h = e.scrollable(t),
              a = d(r);
          null != h ? i = e.scrollHeight(t) : (i = t.scrollHeight - a, s.options.verticalCentered && (i = f(l, t)[0].scrollHeight - a));
          var c = fp_utils.getWindowHeight() - a;
          i > c ? null != h ? e.update(t, c) : (s.options.verticalCentered ? (fp_utils.wrapInner(f(l, t)[0], o.scroller), fp_utils.wrapInner(f(l, t)[0], o.scrollable)) : (fp_utils.wrapInner(t, o.scroller), fp_utils.wrapInner(t, o.scrollable)), e.create(t, c, s.iscrollOptions)) : e.remove(t), fp_utils.css(t, {
            overflow: ""
          });
        }
      }

      function r(t) {
        f(n).forEach(function (i) {
          var s = f(h, i);
          s.length ? s.forEach(function (i) {
            t(i);
          }) : t(i);
        });
      }

      s.options = null, s.init = function (o, n) {
        return s.options = o, s.iscrollOptions = n, "complete" === i.readyState && (e(), fullpage_api.shared.afterRenderActions()), t.addEventListener("load", function () {
          e(), fullpage_api.shared.afterRenderActions();
        }), s;
      }, s.createScrollBarForAll = e;
    }

    IScroll.prototype.wheelOn = function () {
      this.wrapper.addEventListener("wheel", this), this.wrapper.addEventListener("mousewheel", this), this.wrapper.addEventListener("DOMMouseScroll", this);
    }, IScroll.prototype.wheelOff = function () {
      this.wrapper.removeEventListener("wheel", this), this.wrapper.removeEventListener("mousewheel", this), this.wrapper.removeEventListener("DOMMouseScroll", this);
    };
    var f = null,
        m = {
      refreshId: null,
      iScrollInstances: [],
      iscrollOptions: {
        scrollbars: !0,
        mouseWheel: !0,
        hideScrollbars: !1,
        fadeScrollbars: !1,
        disableMouse: !0,
        interactiveScrollbars: !0
      },
      init: function init(i) {
        f = fp_utils.$;
        var s = "ontouchstart" in t || navigator.msMaxTouchPoints > 0 || navigator.maxTouchPoints;
        return m.iscrollOptions.click = s, m.iscrollOptions = fp_utils.deepExtend(m.iscrollOptions, i.scrollOverflowOptions), new u().init(i, m.iscrollOptions);
      },
      toggleWheel: function toggleWheel(t) {
        f(e, f(r)[0]).forEach(function (i) {
          var s = i.fp_iscrollInstance;
          null != s && (t ? s.wheelOn() : s.wheelOff());
        });
      },
      onLeave: function onLeave() {
        m.toggleWheel(!1);
      },
      beforeLeave: function beforeLeave() {
        m.onLeave();
      },
      afterLoad: function afterLoad() {
        m.toggleWheel(!0);
      },
      create: function create(t, i, s) {
        f(e, t).forEach(function (e) {
          fp_utils.css(e, {
            height: i + "px"
          });
          var o = e.fp_iscrollInstance;
          null != o && m.iScrollInstances.forEach(function (t) {
            t.destroy();
          }), o = new IScroll(e, s), m.iScrollInstances.push(o), fp_utils.hasClass(fp_utils.closest(t, n), "active") || o.wheelOff(), e.fp_iscrollInstance = o;
        });
      },
      isScrolled: function isScrolled(t, i) {
        var s = i.fp_iscrollInstance;
        return !s || ("top" === t ? s.y >= 0 && !fp_utils.getScrollTop(i) : "bottom" === t ? 0 - s.y + fp_utils.getScrollTop(i) + 1 + i.offsetHeight >= i.scrollHeight : void 0);
      },
      scrollable: function scrollable(t) {
        return f(".fp-slides", t).length ? f(e, f(a, t)[0])[0] : f(e, t)[0];
      },
      scrollHeight: function scrollHeight(t) {
        return f(".fp-scroller", f(e, t)[0])[0].scrollHeight;
      },
      remove: function remove(t) {
        if (null != t) {
          var i = f(e, t)[0];

          if (null != i) {
            var s = i.fp_iscrollInstance;
            null != s && s.destroy(), i.fp_iscrollInstance = null, f(".fp-scroller", t)[0].outerHTML = f(".fp-scroller", t)[0].innerHTML, f(e, t)[0].outerHTML = f(e, t)[0].innerHTML;
          }
        }
      },
      update: function update(t, i) {
        clearTimeout(m.refreshId), m.refreshId = setTimeout(function () {
          m.iScrollInstances.forEach(function (t) {
            t.refresh(), fullpage_api.silentMoveTo(fp_utils.index(f(r)[0]) + 1);
          });
        }, 150), fp_utils.css(f(e, t)[0], {
          height: i + "px"
        }), fp_utils.css(f(e, t)[0].parentNode, {
          height: i + d(t) + "px"
        });
      },
      wrapContent: function wrapContent() {
        var t = i.createElement("div");
        t.className = s;
        var e = i.createElement("div");
        return e.className = "fp-scroller", {
          scrollable: t,
          scroller: e
        };
      }
    };
    return {
      iscrollHandler: m
    };
  }();
}(window, document);

/***/ }),

/***/ 0:
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** multi ./public/storage/laraassets/js/scrolloverflow.min.js ./public/storage/laraassets/js/owl.carousel.min.js ./public/storage/laraassets/js/apps.js ./public/storage/laraassets/js/script.js ***!
  \*****************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Lucver\Project\root\Work\Ozzigeno web\ozziApp-laravel7\public\storage\laraassets\js\scrolloverflow.min.js */"./storage/app/public/laraassets/js/scrolloverflow.min.js");
__webpack_require__(/*! D:\Lucver\Project\root\Work\Ozzigeno web\ozziApp-laravel7\public\storage\laraassets\js\owl.carousel.min.js */"./storage/app/public/laraassets/js/owl.carousel.min.js");
__webpack_require__(/*! D:\Lucver\Project\root\Work\Ozzigeno web\ozziApp-laravel7\public\storage\laraassets\js\apps.js */"./storage/app/public/laraassets/js/apps.js");
module.exports = __webpack_require__(/*! D:\Lucver\Project\root\Work\Ozzigeno web\ozziApp-laravel7\public\storage\laraassets\js\script.js */"./storage/app/public/laraassets/js/script.js");


/***/ })

/******/ });