$(document).ready(function() {
    // Open Menu
    var beginAC = 80,
        endAC   = 320,
        beginB  = 80,
        endB    = 320;

    function inAC(s) {
        s.draw('80% - 240', '80%', 0.3, {
            delay: 0.1,
            callback: function() {
                inAC2(s)
            }
        });
    }
    function inAC2(s) {
        s.draw('100% - 545', '100% - 305', 0.6, {
            easing: ease.ease('elastic-out', 1, 0.3)
        });
    }
    function inB(s) {
        s.draw(beginB - 60, endB + 60, 0.1, {
            callback: function() {
                inB2(s)
            }
        });
    }
    function inB2(s) {
        s.draw(beginB + 120, endB - 120, 0.3, {
            easing: ease.ease('bounce-out', 1, 0.3)
        });
    }

    /* Out animations (to burger icon) */
    function outAC(s) {
        s.draw('90% - 240', '90%', 0.1, {
            easing: ease.ease('elastic-in', 1, 0.3),
            callback: function() {
                outAC2(s)
            }
        });
    }
    function outAC2(s) {
        s.draw('20% - 240', '20%', 0.3, {
            callback: function() {
                outAC3(s)
            }
        });
    }
    function outAC3(s) {
        s.draw(beginAC, endAC, 0.7, {
            easing: ease.ease('elastic-out', 1, 0.3)
        });
    }
    function outB(s) {
        s.draw(beginB, endB, 0.7, {
            delay: 0.1,
            easing: ease.ease('elastic-out', 2, 0.4)
        });
    }

    /* Awesome burger default */
    var pathA       = document.getElementById('pathA'),
        pathB       = document.getElementById('pathB'),
        pathC       = document.getElementById('pathC'),
        segmentA    = new Segment(pathA, beginAC, endAC),
        segmentB    = new Segment(pathB, beginB, endB),
        segmentC    = new Segment(pathC, beginAC, endAC),
        trigger     = document.getElementById('z-button-trigger'),
        toCloseIcon = true,
        wrapper     = document.getElementById('z-button-wrapper');

    trigger.onclick = function() {
        isAboutPage = document.getElementById("fp-nav") ? document.getElementById("fp-nav") : undefined;
        if (toCloseIcon) {
            inAC(segmentA);
            inB(segmentB);
            inAC(segmentC);

            wrapper.className = 'z-button-wrapper expanded';

            if(isAboutPage)
            {
                isAboutPage
                .style.cssText = `opacity: 0; transition: 1s; visibility: hidden;`
            }
        } else {
            outAC(segmentA);
            outB(segmentB);
            outAC(segmentC);

            wrapper.className = 'z-button-wrapper';

            if(isAboutPage)
            {
                isAboutPage
                .style.cssText = `opacity: 100; tansition-property: opacity; transition-duration: 1s; transition-delay: 2s; visibility: visible;`
            }
        }
        toCloseIcon = !toCloseIcon;
    };

    $(function(){
        setTimeout(function(){
            $(".z-button").css('opacity', 1);
        }, 1000)
    });

    // Fullpage JS for Home Page
    $('[data-aos]').each(function(){
        $(this).addClass("aos-init");
    });

    $('#fullpage').fullpage({
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        onLeave: function(origin, destination, direction) {
            $('.section [data-aos]').each(function(){
                $(this).removeClass("aos-animate");
            });

            if (origin != null) {
                if(origin.index == 0 && direction == 'down') {
                    $('body').addClass("has-z-open");
                } else if(origin.index == 1 && direction == 'up') {
                    $('body').removeClass("has-z-open");
                }
            }
        },
        afterLoad: function() {
            $('.section.active [data-aos]').each(function(){
                $(this).addClass("aos-animate");
            });
        }
    });

    // Home Slideshow on hover
    var anchor  = $('.slideshow-anchor > .link');
        piece   = $('.slideshow-item .piece');

    anchor.hover(function(){
        piece.parent().addClass("on-hover");
    }, function(){
        piece.parent().removeClass("on-hover");
    });

    // Fullpage JS for About Page
    if ($(window).width() > 800) {
        $('#aboutPage').fullpage({
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            navigation: true,
            navigationPosition: 'left',
            scrollingSpeed: 600,
            keyboardScrolling: true,
            scrollOverflow: true,
            afterLoad: function(origin, destination, direction) {
                $('.section.active [data-aos]').each(function(){
                    $(this).addClass("aos-animate");
                });

                if (origin != null) {
                    if(origin.index == 1) {
                        $('.ozg-client-teaser').find('.aos-init').addClass('aos-animate');

                        // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
                        function getTransform(el) {
                            var results = $('#aboutPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/)
                        if(!results) return [0, 0, 0];
                            if(results[1] == '3d') return results.slice(2,5);
                        results.push(0);
                            return results.slice(6, 7); // this gets me just the top offset from the array...  
                        }
                    } else if(origin.index == 2) {
                        $('.ozg-team-teaser').find('.aos-init').addClass('aos-animate');

                        // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
                        function getTransform(el) {
                            var results = $('#aboutPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/)
                        if(!results) return [0, 0, 0];
                            if(results[1] == '3d') return results.slice(2,5);
                        results.push(0);
                            return results.slice(6, 7); // this gets me just the top offset from the array...  
                        }
                    }
                }
            },
            onLeave: function(origin, destination, direction) {
                $('.section [data-aos]').each(function(){
                    $(this).removeClass("aos-animate");
                });

                if (origin != null) {
                    if(origin.index == 2 && direction == 'down') {
                        $('.ozg-client-teaser').find('.aos-init').removeClass('aos-animate');
                    } else if(origin.index == 3 && direction == 'up') {
                        $('.ozg-team-teaser').find('.aos-init').removeClass('aos-animate');     
                        $('.ozg-client-teaser').find('.aos-init').addClass('aos-animate');               
                    }
                }
            }
        });

        $('#workPage').fullpage({
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            navigation: false,
            scrollOverflow: true,
            afterRender: function() {
                $('.ozg-work-teaser').find('.aos-init').addClass('aos-animate');

                // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
                function getTransform(el) {
                    var results = $('#workPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/)
                if(!results) return [0, 0, 0];
                    if(results[1] == '3d') return results.slice(2,5);
                results.push(0);
                    return results.slice(6, 7); // this gets me just the top offset from the array...  
                }
            },
            afterLoad: function(origin, destination, direction) {            
                $('.section.active [data-aos]').each(function(){
                    $(this).addClass("aos-animate");
                });
            }
        });

        $('#blogPage').fullpage({
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            navigation: false,
            scrollOverflow: true,
            afterRender: function() {
                $('.ozg-blog-teaser').find('.aos-init').addClass('aos-animate');
            },
            afterLoad: function(origin, destination, direction) {
                $('.section.active [data-aos]').each(function(){
                    $(this).addClass("aos-animate");
                });
            }
        });
    } else {
        var index = $('.ozg-team-list .point');
        setInterval(function(){
            if(index.hasClass('aos-animate')){
                if(!$('.ozg-team-list .item:last-child').hasClass('aos-animate')){
                    $('.ozg-team-list .item:last-child').addClass('aos-animate');
                }
            }
        })

        $('.ozg-work-list .item').addClass('aos-animate');

        var delayAos = 0;
            delayInc = 1;

        $('[data-aos]').each(function(index){
            $(this).removeAttr('data-aos-delay');
            delayAos = delayInc * 100;
            $(this).attr('data-aos-delay', delayAos);
            delayInc++;
        });

        AOS.init({
            duration: 1000,
            once: true,
            startEvent: 'DOMContentLoaded'
        });
    }

    function triggerLandscape() {
        var teaser      = $('.ozg-client-teaser'),
            teaserPlace = $('.z-layout');
        
        if (window.matchMedia("(min-device-width: 800px) and (max-device-width: 812px) and (orientation: landscape)").matches) {
            teaser.detach();
            teaserPlace.append(teaser);
            teaser.addClass('isLandscape');
        }
    }
    
    triggerLandscape();

    // Contact Page
    $('.ozg-contact [data-aos]').each(function() {
        $(this).addClass('aos-animate');
    });

    // Slick JS
    $('.slick-carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slick-nav',
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev">',
        nextArrow: '<button type="button" class="slick-next">',
        draggable: false,
        autoplay: true,
    });

    $('.slick-nav [data-aos]').each(function(){
        let slickNav = $(this);
        slickNav.removeClass("aos-animate");
    });

    $('.slick-nav').on('init', function(init) {
        $('.slick-nav [data-aos]').each(function(){
            let slickNav = $(this);

            slickNav.addClass("aos-animate");
        });
    });

    $('.slick-nav').slick({
        slidesToShow: 3,
        asNavFor: '.slick-carousel',
        centerMode: false,
        dots: false,
        draggable: false,
        focusOnSelect: true,
        useTransform: false
    })

    // $(".owl-carousel").owlCarousel({
    //     items: 1,
    //     dots: false,
    //     nav: true,
    //     navText: [],
    //     loop: true,
    //     URLhashListener: false
    // });

    // Anime JS for Contact's heading
    // $('.ozg-contact-heading').each(function(){
    //     $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
    // });

    // anime.timeline({
    //     loop: false
    // })
    // .add({
    //     targets: '.ozg-contact-heading .letter',
    //     translateX: [40,0],
    //     translateZ: 0,
    //     opacity: [0,1],
    //     easing: "easeOutExpo",
    //     duration: 1200,
    //     delay: function(el, i) {
    //         return 500 + 30 * i;
    //     }
    // });

    // Blog Page
    function blogOverlay() {
        var imgHeight   = $('.ozg-blog-image-wrapper > img').height(),
        overlay     = $('.ozg-blog-image-overlay');
        
        overlay.css('height', imgHeight);
    }

    blogOverlay();

    $(window).resize(function() {
        triggerLandscape();
        blogOverlay();
    });
});