
//  bootstrap switch hooking | on change event
$(function() {
    $('.ozshowhide').on('switchChange.bootstrapSwitch', function(event, state) {
        $.ajax({
            url : this.dataset.url,
            type: 'post',
            data: {
                _method: 'PUT',
                _token: this.dataset.token,
                flag: state ? 1 : 0 // state is true|false
            }
        })
    })
})

function sortablePlugin({table, ajaxURL})
{
    elTable = document.querySelector(`${table} tbody`)
    const sortable = new Sortable(elTable, {
        animation: 100,

    onSort: (e) => {
        // get attr id form table row and update every row on field 'sort'

        let datas = [];
        let sort = 1;
        for (const key in e.to.children) {
            if (e.to.children.hasOwnProperty.call(e.to.children, key)) {
                const el = e.to.children[key];
                const numb = sort++
                
                const row = {
                    id: el.dataset.id,
                    sort: numb
                }
                datas.push(row)
                
                el.children[0].innerHTML = numb
            }
        }

        if ( datas.length > 0 ) {

            $.ajax({
                url: ajaxURL,
                type: 'post',
                data: {
                    _method: 'post',
                    _token: e.to.dataset.token,
                    data: datas
                }
            }).done(() => {
                toastr.success("Data Updated")
            })

        }
    }
    })
}

// Get page now
const page = location.pathname.split('/')
const rn = page.at(-1)

// Client page | Admin
if( rn.toLocaleLowerCase() == 'client' )
{
    sortablePlugin({
        table :'.sortable-client',
        ajaxURL : '/admin/client/sortable'
    })
}
if( rn.toLocaleLowerCase() == 'team' )
{
    sortablePlugin({
        table : '.sortable-team',
        ajaxURL : '/admin/team/sortable'
    })
}

// Auto slug
$("#InputNamaClient").on('keyup', function() {

    let slug = this.value.split(' ').join('-')

    document.querySelector('#InputSlug').value = slug
    document.querySelector('#tagSlug').innerText = slug
})

// // Sortable plugin
// const elTable = document.querySelector('.sortable-trigger tbody')
// const sortable = new Sortable(elTable, {
//     animation: 100,

//     onSort: (e) => {
//         // get attr id form table row and update every row on field 'sort'

//         let datas = [];
//         let sort = 1;
//         for (const key in e.to.children) {
//             if (e.to.children.hasOwnProperty.call(e.to.children, key)) {
//                 const el = e.to.children[key];
//                 const numb = sort++
                
//                 const row = {
//                     id: el.dataset.id,
//                     sort: numb
//                 }
//                 datas.push(row)
                
//                 el.children[0].innerHTML = numb
//             }
//         }

//         if ( datas.length > 0 ) {
//             $.ajax({
//                 url: '/admin/work/sortable',
//                 type: 'post',
//                 data: {
//                     _method: 'post',
//                     _token: e.to.dataset.token,
//                     data: datas
//                 }
//             })
//             .done(() => {
//                 toastr.success("Data Updated")
//             })
//         }
//     }
// })