/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../public/laraassets/js/apps.js":
/*!***************************************!*\
  !*** ../public/laraassets/js/apps.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var tl = new TimelineLite({
  onReverseComplete: clearReverseAnimation
}),
    $body = $("body"),
    $toggle = $(".z-button-wrapper"),
    $sidepanel = $(".z-sidepanel"),
    $item = $(".z-sidepanel-list-item"),
    $overlay = $(".z-overlay"),
    $menu = $(".z-menu"),
    $mask = $(".z-menu-mask-group"),
    $menuList = $(".z-menu-list"),
    $menuAnchor = $(".z-menu-list > li > a"),
    $menuFooter = $(".z-menu-footer"),
    $menuLogo = $(".z-menu-logo"),
    $content = $(".z-layout");

function viewBoxArea() {
  svgH = $mask.height(), svgW = $mask.width();
  var svgMask = document.querySelector(".z-menu-mask-group");
  svgMask.setAttribute("viewBox", "0 0 ".concat(svgW, " ").concat(svgH));
}

function radius() {
  frameH = $(".z-menu-nav").height();
  var radius = $(".z-menu-mask-group circle");

  if (window.matchMedia("(min-device-height: 480px) and (max-device-height: 736px) and (orientation: portrait)").matches) {
    radius.attr('r', frameH / 5);
  } else if (window.matchMedia("(min-device-height: 737px) and (max-device-height: 823px) and (orientation: portrait)").matches) {
    radius.attr('r', frameH / 6);
  } else if (window.matchMedia("(min-device-height: 320px) and (max-device-height: 375px) and (orientation: landscape)").matches) {
    radius.attr('r', frameH / 4);
  } else if (window.matchMedia("(min-device-height: 376px) and (max-device-height: 414px) and (orientation: landscape)").matches) {
    radius.attr('r', frameH / 5);
  } else if (window.matchMedia("(min-device-height: 1440px) and (orientation: portrait)").matches) {
    radius.attr('r', frameH / 8);
  } else radius.attr('r', frameH / 5);
}

tl.to($item, 0.1, {
  scale: 0,
  onComplete: function onComplete() {
    $item.css("display", "none");
  }
}).to($menu, 0.1, {
  display: "flex",
  onComplete: function onComplete() {
    viewBoxArea();
  }
}).to($sidepanel, 0.1, {
  width: "8%"
}).to($overlay, 0.1, {
  onComplete: function onComplete() {
    $overlay.css("z-index", "1");
  }
}).to($content, 0.1, {
  filter: "blur(4px)"
}).to($mask, 0.5, {
  left: "0%",
  delay: -0.1,
  onStart: function onStart() {
    radius();
  }
}).staggerTo($menuAnchor, 0.5, {
  left: "0%"
}, 0.15, "stagger").to($menuFooter, 0.1, {
  left: "0%"
}).to($menuLogo, 0.1, {
  opacity: "1"
}).to($body, 0.1, {
  overflow: "hidden"
});
tl.pause();
$toggle.on('click', function () {
  if ($toggle.hasClass("expanded")) {
    tl.play();
  } else {
    tl.reverse();
  }
});
$('body .z-menu').on('click', function (e) {
  var Otrg = $('.z-menu-nav')[0];
  var Itrg = e.target;

  if (Otrg != Itrg) {
    tl.reverse();
  }
});

function clearReverseAnimation() {
  TimelineLite.exportRoot().clear().set($item, {
    clearProps: "all"
  });
  TimelineLite.exportRoot().clear().set($overlay, {
    clearProps: "all"
  });
}

try {
  // Init Isotope
  var $grid = $('#gridBlog').isotope({
    itemSelector: '.ozg-blog-intro',
    layoutMode: 'masonry',
    masonry: {
      columnWidth: undefined,
      gutter: 0
    },
    resize: true,
    initLayout: true
  }); // Bind filter when category is clicked

  $('.filters-list-group').on('click', '.list-button', function () {
    var filterValue = $(this).attr('data-filter');
    filterValue = filterValue;
    $grid.isotope({
      filter: filterValue
    });
  }); // Change active class on clicked category

  $('.list-group').each(function (i, listGroup) {
    var $listGroup = $(listGroup);
    $listGroup.on('click', '.list-button', function () {
      $listGroup.find('.active').removeClass('active');
      $(this).addClass('active');
    });
  });
} catch (error) {}

$(window).on("resize", function () {
  viewBoxArea();
  radius();
});

/***/ }),

/***/ "../public/laraassets/js/script.js":
/*!*****************************************!*\
  !*** ../public/laraassets/js/script.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  // Open Menu
  var beginAC = 80,
      endAC = 320,
      beginB = 80,
      endB = 320;

  function inAC(s) {
    s.draw('80% - 240', '80%', 0.3, {
      delay: 0.1,
      callback: function callback() {
        inAC2(s);
      }
    });
  }

  function inAC2(s) {
    s.draw('100% - 545', '100% - 305', 0.6, {
      easing: ease.ease('elastic-out', 1, 0.3)
    });
  }

  function inB(s) {
    s.draw(beginB - 60, endB + 60, 0.1, {
      callback: function callback() {
        inB2(s);
      }
    });
  }

  function inB2(s) {
    s.draw(beginB + 120, endB - 120, 0.3, {
      easing: ease.ease('bounce-out', 1, 0.3)
    });
  }
  /* Out animations (to burger icon) */


  function outAC(s) {
    s.draw('90% - 240', '90%', 0.1, {
      easing: ease.ease('elastic-in', 1, 0.3),
      callback: function callback() {
        outAC2(s);
      }
    });
  }

  function outAC2(s) {
    s.draw('20% - 240', '20%', 0.3, {
      callback: function callback() {
        outAC3(s);
      }
    });
  }

  function outAC3(s) {
    s.draw(beginAC, endAC, 0.7, {
      easing: ease.ease('elastic-out', 1, 0.3)
    });
  }

  function outB(s) {
    s.draw(beginB, endB, 0.7, {
      delay: 0.1,
      easing: ease.ease('elastic-out', 2, 0.4)
    });
  }
  /* Awesome burger default */


  var pathA = document.getElementById('pathA'),
      pathB = document.getElementById('pathB'),
      pathC = document.getElementById('pathC'),
      segmentA = new Segment(pathA, beginAC, endAC),
      segmentB = new Segment(pathB, beginB, endB),
      segmentC = new Segment(pathC, beginAC, endAC),
      trigger = document.getElementById('z-button-trigger'),
      toCloseIcon = true,
      wrapper = document.getElementById('z-button-wrapper');

  trigger.onclick = function () {
    if (toCloseIcon) {
      inAC(segmentA);
      inB(segmentB);
      inAC(segmentC);

      wrapper.className = 'z-button-wrapper expanded';
    } else {
      outAC(segmentA);
      outB(segmentB);
      outAC(segmentC);

      wrapper.className = 'z-button-wrapper';
    }

    toCloseIcon = !toCloseIcon;
  };

  $(function () {
    setTimeout(function () {
      $(".z-button").css('opacity', 1);
    }, 1);
  }); // Fullpage JS for Home Page

  $('[data-aos]').each(function () {
    $(this).addClass("aos-init");
  });

  try {
    $('#fullpage').fullpage({
      licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
      onLeave: function onLeave(origin, destination, direction) {
        $('.section [data-aos]').each(function () {
          $(this).removeClass("aos-animate");
        });

        if (origin != null) {
          if (origin.index == 0 && direction == 'down') {
            $('body').addClass("has-z-open");
          } else if (origin.index == 1 && direction == 'up') {
            $('body').removeClass("has-z-open");
          }
        }
      },
      afterLoad: function afterLoad() {
        $('.section.active [data-aos]').each(function () {
          $(this).addClass("aos-animate");
        });
      }
    });
  } catch (error) {}

  $('.rslides').responsiveSlides({
    timeout: 5000
  }); // Home Slideshow on hover

  var anchor = $('.slideshow-anchor > .link');
  piece = $('.slideshow-item .piece');
  anchor.hover(function () {
    piece.parent().addClass("on-hover");
  }, function () {
    piece.parent().removeClass("on-hover");
  }); // Fullpage JS for About Page

  if ($(window).width() > 800) {
    try {
      $('#aboutPage').fullpage({
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        navigation: true,
        navigationPosition: 'left',
        scrollingSpeed: 600,
        keyboardScrolling: true,
        scrollOverflow: true,
        afterLoad: function afterLoad(origin, destination, direction) {
          $('.section.active [data-aos]').each(function () {
            $(this).addClass("aos-animate");
          });

          if (origin != null) {
            if (origin.index == 1) {
              // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
              var getTransform = function getTransform(el) {
                var results = $('#aboutPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
                if (!results) return [0, 0, 0];
                if (results[1] == '3d') return results.slice(2, 5);
                results.push(0);
                return results.slice(6, 7); // this gets me just the top offset from the array...  
              };

              $('.ozg-client-teaser').find('.aos-init').addClass('aos-animate');
            } else if (origin.index == 2) {
              // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td
              var _getTransform = function _getTransform(el) {
                var results = $('#aboutPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
                if (!results) return [0, 0, 0];
                if (results[1] == '3d') return results.slice(2, 5);
                results.push(0);
                return results.slice(6, 7); // this gets me just the top offset from the array...  
              };

              $('.ozg-team-teaser').find('.aos-init').addClass('aos-animate');
            }
          }
        },
        onLeave: function onLeave(origin, destination, direction) {
          $('.section [data-aos]').each(function () {
            $(this).removeClass("aos-animate");
          });

          if (origin != null) {
            if (origin.index == 2 && direction == 'down') {
              $('.ozg-client-teaser').find('.aos-init').removeClass('aos-animate');
            } else if (origin.index == 3 && direction == 'up') {
              $('.ozg-team-teaser').find('.aos-init').removeClass('aos-animate');
              $('.ozg-client-teaser').find('.aos-init').addClass('aos-animate');
            }
          }
        }
      });
      $('#workPage').fullpage({
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        navigation: false,
        scrollOverflow: true,
        afterRender: function afterRender() {
          $('.ozg-work-teaser').find('.aos-init').addClass('aos-animate'); // function to get the 3d transform offset on a slide so I can position the timeline arrows. - td

          function getTransform(el) {
            var results = $('#workPage').css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
            if (!results) return [0, 0, 0];
            if (results[1] == '3d') return results.slice(2, 5);
            results.push(0);
            return results.slice(6, 7); // this gets me just the top offset from the array...  
          }
        },
        afterLoad: function afterLoad(origin, destination, direction) {
          $('.section.active [data-aos]').each(function () {
            $(this).addClass("aos-animate");
          });
        }
      });
      $('#blogPage').fullpage({
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        navigation: false,
        scrollOverflow: true,
        afterRender: function afterRender() {
          $('.ozg-blog-teaser').find('.aos-init').addClass('aos-animate');
        },
        afterLoad: function afterLoad(origin, destination, direction) {
          $('.section.active [data-aos]').each(function () {
            $(this).addClass("aos-animate");
          });
        }
      });
    } catch (error) {
      $('.ozg-blog-teaser, .ozg-work, .ozg-work-teaser').find('.aos-init').addClass('aos-animate');
      $('.ozg-blog-content, .ozg-work-content').addClass('aos-animate');
      $('.section.active [data-aos]').each(function () {
        $(this).addClass("aos-animate");
      });
    }
  } else {
    var index = $('.ozg-team-list .point');
    setInterval(function () {
      if (index.hasClass('aos-animate')) {
        if (!$('.ozg-team-list .item:last-child').hasClass('aos-animate')) {
          $('.ozg-team-list .item:last-child').addClass('aos-animate');
        }
      }
    });
    $('.ozg-work-list .item').addClass('aos-animate');
    var delayAos = 0;
    delayInc = 1;
    $('[data-aos]').each(function (index) {
      $(this).removeAttr('data-aos-delay');
      delayAos = delayInc * 100;
      $(this).attr('data-aos-delay', delayAos);
      delayInc++;
    });
    AOS.init({
      duration: 1000,
      once: true,
      startEvent: 'DOMContentLoaded'
    });
  }

  function triggerLandscape() {
    var teaser = $('.ozg-client-teaser'),
        teaserPlace = $('.z-layout');

    if (window.matchMedia("(min-device-width: 800px) and (max-device-width: 812px) and (orientation: landscape)").matches) {
      teaser.detach();
      teaserPlace.append(teaser);
      teaser.addClass('isLandscape');
    }
  }

  triggerLandscape(); // Contact Page

  $('.ozg-contact [data-aos]').each(function () {
    $(this).addClass('aos-animate');
  }); // Slick JS

  $('.slick-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slick-nav',
    arrows: false,
    prevArrow: '<button type="button" class="slick-prev">',
    nextArrow: '<button type="button" class="slick-next">',
    draggable: false
  });
  $('.slick-nav [data-aos]').each(function () {
    var slickNav = $(this);
    slickNav.removeClass("aos-animate");
  });
  $('.slick-nav').on('init', function (init) {
    $('.slick-nav [data-aos]').each(function () {
      var slickNav = $(this);
      slickNav.addClass("aos-animate");
    });
  });
  $('.slick-nav').slick({
    slidesToShow: 3,
    asNavFor: '.slick-carousel',
    centerMode: false,
    dots: false,
    draggable: false,
    focusOnSelect: true,
    useTransform: false
  });
  $('.item-link-inner').on('click', function (e) {
    e.preventDefault();
    var attr = $(this).attr('data-href');
    $('.ozg-contact-to').attr('href', attr);
  });
  $('.slick-carousel').on('swipe', function (e, s, direction) {
    e.preventDefault();
    var slick_c = $(this).find('.ozg-contact-heading.slick-current');
    var slick_href = slick_c.attr('data-href');
    $('.ozg-contact-to').attr('href', slick_href);
  });
  $('body .z-menu').on('click', function (e) {
    var Otrg = $('.z-menu-nav')[0];
    var Itrg = e.target;

    if (Otrg != Itrg) {
      outAC(segmentA);
      outB(segmentB);
      outAC(segmentC);
      $('#z-button-wrapper').removeClass('expanded');
      toCloseIcon = !toCloseIcon;
    }
  }); // Blog Page

  function blogOverlay() {
    var imgHeight = $('.ozg-blog-image-wrapper > img').height(),
        overlay = $('.ozg-blog-image-overlay');
    overlay.css('height', imgHeight);
  }

  blogOverlay(); // Back to Top button

  var btn = $("#back-to-top");
  var height = $(window).height();
  $(window).scroll(function () {
    if ($(window).scrollTop() > height) {
      btn.addClass('show');
    } else {
      btn.removeClass('show');
    }
  });
  btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 300);
  }); // Work Detail Navigation

  $(window).scroll(function () {
    if ($(window).width() >= 1024) {
      if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        $(".next-work-cover-image").css('transform', 'scale(1.2)');
      }
    }
  });
  $(window).resize(function () {
    triggerLandscape();
    blogOverlay();
  });
});

/***/ }),

/***/ 0:
/*!*******************************************************************************!*\
  !*** multi ../public/laraassets/js/apps.js ../public/laraassets/js/script.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! E:\programs\xampp\htdocs\laravel\projects\oz2\public\laraassets\js\apps.js */"../public/laraassets/js/apps.js");
module.exports = __webpack_require__(/*! E:\programs\xampp\htdocs\laravel\projects\oz2\public\laraassets\js\script.js */"../public/laraassets/js/script.js");


/***/ })

/******/ });